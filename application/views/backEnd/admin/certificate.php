<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-teal">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('certificate'); ?> </h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <?php if(isset($edit_info)){ ?>
                    <div class="row" style="box-shadow: 1px 1px 15px 5px teal;margin: 10px 30px 40px 25px;padding: 30px 0px 30px 0px;">
                        <form action="<?php echo base_url('admin/certificate/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <?php echo $this->lang->line('name'); ?> *</label>
                                            <input name="name" class="form-control" placeholder="<?php echo $this->lang->line('name'); ?>" required="" type="text" value="<?php echo $edit_info->name; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for=""><?php echo $this->lang->line('year'); ?> *</label>
                                            <select name="year" class="form-control select2" style="width: 100%;" required="">
                                                <option value="<?php echo date('Y', strtotime('-10 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-10 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-10 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-9 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-9 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-9 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-8 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-8 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-8 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-7 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-7 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-7 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-6 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-6 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-6 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-5 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-5 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-5 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-4 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-4 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-4 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-3 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-3 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-3 years')) ?></option>
                                                    <option value="<?php echo date('Y', strtotime('-2 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-2 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-2 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-1 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('-1 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('-1 years')) ?></option>
                                                <option value="<?php echo date('Y') ?>"
                                                        <?php if ($edit_info->year == date('Y')) echo 'selected'; ?>
                                                    ><?php echo date('Y') ?></option>
                                                <option value="<?php echo date('Y', strtotime('+1 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('+1 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('+1 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('+2 years')) ?>"
                                                        <?php if ($edit_info->year == date('Y', strtotime('+2 years'))) echo 'selected'; ?>
                                                    ><?php echo date('Y', strtotime('+2 years')) ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label><?php echo $this->lang->line('photo'); ?></label>
                                            <input name="photo" type="file" onchange="readpicture(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <br> </label>
                                            <button type="submit" class="form-control btn bg-teal"> <?php echo $this->lang->line('save'); ?> </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>	
                                            <img id="certificate_photo_change" class="img-responsive" src="<?php echo base_url($edit_info->photo); ?>" alt="Certificate Picture" style="width: 200px; height: 285px;">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php }else {?>
                    <div class="row" style="box-shadow: 1px 1px 15px 5px teal;margin: 10px 30px 40px 25px;padding: 30px 0px 30px 0px;">
                        <form action="<?php echo base_url('admin/certificate/add/') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <?php echo $this->lang->line('name'); ?> *</label>
                                            <input name="name" class="form-control" placeholder="<?php echo $this->lang->line('name'); ?>" required="" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for=""><?php echo $this->lang->line('year'); ?> *</label>
                                            <select name="year" class="form-control select2" style="width: 100%;" required>
                                                <option value="<?php echo date('Y', strtotime('-10 years')) ?>"><?php echo date('Y', strtotime('-10 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-9 years')) ?>"><?php echo date('Y', strtotime('-9 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-8 years')) ?>"><?php echo date('Y', strtotime('-8 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-7 years')) ?>"><?php echo date('Y', strtotime('-7 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-6 years')) ?>"><?php echo date('Y', strtotime('-6 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-5 years')) ?>"><?php echo date('Y', strtotime('-5 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-4 years')) ?>"><?php echo date('Y', strtotime('-4 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-3 years')) ?>"><?php echo date('Y', strtotime('-3 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-2 years')) ?>"><?php echo date('Y', strtotime('-2 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('-1 years')) ?>"><?php echo date('Y', strtotime('-1 years')) ?></option>
                                                <option value="<?php echo date('Y') ?>" selected><?php echo date('Y') ?></option>
                                                <option value="<?php echo date('Y', strtotime('+1 years')) ?>"><?php echo date('Y', strtotime('+1 years')) ?></option>
                                                <option value="<?php echo date('Y', strtotime('+2 years')) ?>"><?php echo date('Y', strtotime('+2 years')) ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label><?php echo $this->lang->line('photo'); ?> *</label>
                                            <input name="photo" type="file" required onchange="readpicture(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <br> </label>
                                            <button type="submit" class="form-control btn bg-teal"> <?php echo $this->lang->line('save'); ?> </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>    
                                            <img id="certificate_photo_change" class="img-responsive" src="//placehold.it/200x285" alt="Certificate Picture">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="userListTable" class="table table-bordered table-striped table_th_teal">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('sl'); ?></th>
                                        <th><?php echo $this->lang->line('name'); ?></th>
                                        <th><?php echo $this->lang->line('year'); ?></th>
                                        <th><?php echo $this->lang->line('photo'); ?></th>
                                        <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($certificates as $key => $certificate_value) {
                                        	?>
                                    <tr>
                                        <td> <?= ++$key; ?> </td>
                                        <td> <?= $certificate_value->name; ?> </td>
                                        <td> <?= $certificate_value->year; ?> </td>
                                        <td> <img src="<?= base_url($certificate_value->photo); ?>" class="img img-responsive" style="width: 50px; height: 50px;"> </td>
                                        <td>
                                            <a href="<?= base_url('admin/certificate/edit/'.$certificate_value->id); ?>" class="btn btn-sm bg-teal"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/certificate/delete/'.$certificate_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-red"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class=" box-footer">
                </div>
                <!-- /.box-footer --> 
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#certificate_photo_change')
            .attr('src', e.target.result)
            .width(200)
            .height(285);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>