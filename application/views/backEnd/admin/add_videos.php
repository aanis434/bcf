<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> Add Videos </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url('admin/videos/list'); ?>" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Videos List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/videos/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Video Title *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="video_title" class="form-control" placeholder="Video Title" required>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Video URL *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="video_link" class="form-control" placeholder="Video URL" required>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Video Description</label>
                                        <div class="col-md-10">
                                            <textarea name="video_description" id="video_description" rows="1" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-solid box-primary">
                                        <div class="box-header"> <label>Video Thumb *</label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="thumb_photo_change" class="img-responsive" src="//placehold.it/400x400" alt="News Photo">
                                                <br>
                                                <input name="photo" type="file" onchange="readpicture(this)">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Video</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#thumb_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('video_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>