

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-purple box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('update_contact_manage'); ?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url() ?>admin/contactmanage/list" type="submit" class="btn bg-orange btn-sm" style="color: white;"> <i class="fa fa-list"></i> <?php echo $this->lang->line('contact_manage_list'); ?> </a>
                    </div>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('admin/contactmanage/edit/'.$contact_manage_edit->id) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="title_two" class="control-label"><?php echo $this->lang->line('name'); ?> *</label>
                                            <input name="name" class="form-control inner_shadow_purple" type="text" value="<?php echo $contact_manage_edit->name; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="title_two" class="control-label"><?php echo $this->lang->line('email'); ?> *</label>
                                            <input name="email" class="form-control inner_shadow_purple" type="email" value="<?php echo $contact_manage_edit->email; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="title_two" class="control-label"><?php echo $this->lang->line('subject'); ?> *</label>
                                            <input name="subject" class="form-control inner_shadow_purple" type="text" value="<?php echo $contact_manage_edit->subject; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="body" class="control-label" > <?php echo $this->lang->line('message_body'); ?> *</label>
                                            <textarea id="editor1" name="message_body" rows="5"  required><?php echo $contact_manage_edit->message_body; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-12">
                            <center>
                                <button type="reset" class="btn bg-aqua"><?php echo $this->lang->line('cancel'); ?></button>
                                <button type="submit" class="btn btn-danger"><?php echo $this->lang->line('update'); ?></button>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>

