<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('contact_add'); ?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url() ?>admin/contactpage/list" class="btn bg-green btn-sm" style="color: white;"> <i class="fa fa-list"></i> <?php echo $this->lang->line('contact_list'); ?> </a>
                    </div>
                </div>
                <div class="box-body">
                    <form action="<?php echo base_url('admin/contactpage/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="">Footer Description *</label>
                                            <textarea name="footer_description" rows="3" placeholder="Footer description" class="form-control inner_shadow_red"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for=""><?php echo $this->lang->line('address'); ?> *</label>
                                            <textarea name="address" rows="3" class="form-control inner_shadow_red" placeholder="Address" required></textarea>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="">Google Map</label>
                                            <input type="text" name="g_map" class="form-control inner_shadow_red" placeholder="Google map">
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for=""><?php echo $this->lang->line('phone'); ?> *</label>
                                            <input type="text" name="phone" class="form-control inner_shadow_red" placeholder="<?php echo $this->lang->line('phone'); ?>" required pattern="[0]{1}[1]{1}[3|4|5|6|7|8|9]{1}[0-9]{8}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for=""><?php echo $this->lang->line('email'); ?> *</label>
                                            <input type="email" name="email" class="form-control inner_shadow_red" placeholder="<?php echo $this->lang->line('email'); ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="">Telephone</label>
                                            <input type="text" name="tel" class="form-control inner_shadow_red" placeholder="TelePhone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="">Website URL</label>
                                            <input type="text" name="w_url" class="form-control inner_shadow_red" placeholder="Website URL">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <center>
                                <button type="reset" class="btn btn-sm bg-red"><?php echo $this->lang->line('reset'); ?></button>
                                <button type="submit" class="btn btn-sm bg-green"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $this->lang->line('save'); ?></button>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>