<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i> Follow List</h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/follow_us/add" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Follow</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userListTable" class="table table-bordered table-striped table_th_primary">
                        <thead>
                            <tr>
                                <th width="10%">SL</th>
                                <th width="10%">Facebook URl</th>
                                <th width="10%">Twitter URl</th>
                                <th width="10%">Linkedn URl</th>
                                <th width="10%">Youtube URl</th>
                                <th width="10%">Pinterest URl</th>
                                <th width="10%">Instagram URl</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($all_social_url as $key => $all_social_url_value) { ?>
                                 <tr>
                                <td><?php echo ++$key; ?></td>
                                  
                                <td><?php echo $all_social_url_value->fb_url; ?></td>
                                <td><?php echo $all_social_url_value->tw_url; ?></td>
                                <td><?php echo $all_social_url_value->ln_url; ?></td>
                                <td><?php echo $all_social_url_value->yt_url; ?></td>
                                <td><?php echo $all_social_url_value->pi_url; ?></td>
                                <td><?php echo $all_social_url_value->ins_url; ?></td>
                                
                                <td> 
                                    <a href="<?php echo base_url('admin/follow_us/edit/'.$all_social_url_value->id) ;?>" class="btn btn-sm bg-purple"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url('admin/follow_us/delete/'.$all_social_url_value->id) ;?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                           <?php } ?>
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
</script>

