<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> News Update </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url('admin/news/list'); ?>" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> News List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/news/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">News Title *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="news_title" class="form-control" value="<?php echo $edit_info->news_title; ?>" placeholder="Title" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">News Source *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="news_source" class="form-control" value="<?=$edit_info->news_source?>" placeholder="News Source" required>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">News Description</label>
                                        <div class="col-md-10">
                                            <textarea name="news_description" id="news_description" rows="1" class="form-control"><?php echo $edit_info->news_description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-solid box-primary">
                                        <div class="box-header"> <label> News Photo *</label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="news_photo_change" class="img-responsive" src="<?php echo base_url($edit_info->photo); ?>" alt="News Photo">
                                                <br>
                                                <input name="photo" type="file" onchange="readpicture(this)">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#news_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('news_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>