<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> Add Partner </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url('admin/partner/list'); ?>" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Partner List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/partner/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="box box-solid box-primary">
                                        <div class="box-header"> <label>News Photo *</label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="news_photo_change" class="img-responsive" src="//placehold.it/400x400" alt="News Photo">
                                                <br>
                                                <input name="photo" type="file" onchange="readpicture(this)">
                                            </center>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Partner</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#news_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>