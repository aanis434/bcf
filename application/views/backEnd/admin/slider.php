

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('slider'); ?> </h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <?php if(isset($slider_info)){ ?>
                    <div class="row" style="box-shadow: 1px 1px 15px 5px #3c8dbc;margin: 10px 30px 40px 25px;padding: 30px 0px 30px 0px;">
                        <form action="<?php echo base_url('admin/slider/edit/'.$slider_info_id) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                                <center>
                                    <h3 style="color: #3c8dbc;"> <b> <?php echo $this->lang->line('update'); ?> </b> </h3>
                                </center>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label><?php echo $this->lang->line('slider_photo'); ?></label>
                                            <input name="photo" type="file" onchange="readpicture(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <?php echo $this->lang->line('priority'); ?></label>
                                            <input name="priority" class="form-control inner_shadow_primary" placeholder="<?php echo $this->lang->line('priority'); ?>" required="" type="number" value="<?php echo $slider_info->priority; ?>" min="1" max="1000">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <br> </label>
                                            <button type="submit" class="form-control btn btn-primary"> <?php echo $this->lang->line('save'); ?> </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>	
                                            <img id="slider_photo_change" class="img-responsive" src="<?php echo base_url($slider_info->photo); ?>" alt="Slider Picture">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php }else {?>
                    <div class="row" style="box-shadow: 1px 1px 15px 5px #3c8dbc;margin: 10px 30px 40px 25px;padding: 30px 0px 30px 0px;">
                        <form action="<?php echo base_url('admin/slider/add/') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label><?php echo $this->lang->line('slider_photo'); ?></label>
                                            <input name="photo" required="" type="file" onchange="readpicture(this)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <?php echo $this->lang->line('priority'); ?></label>
                                            <input name="priority" class="form-control inner_shadow_primary" placeholder="<?php echo $this->lang->line('priority'); ?>" required="" type="number" min="1" max="1000">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <br> </label>
                                            <button type="submit" class="form-control btn btn-primary"> <?php echo $this->lang->line('add'); ?> </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>	
                                            <img id="slider_photo_change" class="img-responsive" src="//placehold.it/600x300" alt="Slider Picture">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="userListTable" class="table table-bordered table-striped table_th_primary">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('sl'); ?></th>
                                        <th><?php echo $this->lang->line('priority'); ?></th>
                                        <th><?php echo $this->lang->line('photo'); ?></th>
                                        <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($all_slider as $key => $slider_value) {
                                        	?>
                                    <tr>
                                        <td> <?= $key+1; ?> </td>
                                        <td> <?= $slider_value->priority; ?> </td>
                                        <td> <img src="<?= base_url($slider_value->photo); ?>" class="img img-responsive" style="width: 100px;"> </td>
                                        <td>
                                            <a href="<?= base_url('admin/slider/edit/'.$slider_value->id); ?>" class="btn btn-sm bg-green"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/slider/delete/'.$slider_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-orange"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class=" box-footer">
                </div>
                <!-- /.box-footer --> 
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#slider_photo_change')
            .attr('src', e.target.result)
            .width(600)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>

