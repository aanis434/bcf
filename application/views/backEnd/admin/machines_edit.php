<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-warning box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('machines_update'); ?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url('admin/machines/add'); ?>" class="btn btn-sm bg-purple" style="color: white"><i class="fa fa-list"></i> <?php echo $this->lang->line('machines'); ?></a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/machines/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                            <br>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label"><?php echo $this->lang->line('name'); ?> *</label>
                                        <div class="col-md-9">
                                            <input type="text" name="name" class="form-control" placeholder="<?php $this->lang->line('name'); ?>" required value="<?php echo $edit_info->name; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label"><?php echo $this->lang->line('machines_type'); ?> *</label>
                                        <div class="col-md-9">
                                            <select name="machine_type_id" id="" class="form-control select2" style="width: 100%;">
                                                <option value=""><?php echo $this->lang->line('select_one'); ?></option>
                                                <?php 

                                                    if ($machines_type) {
                                                        foreach ($machines_type as $key => $machines_type_value) {?>
                                                    <option value="<?php echo $machines_type_value->id; ?>"
                                                            <?php if ($edit_info->machine_type_id == $machines_type_value->id) echo 'selected'; ?>
                                                        ><?php echo $machines_type_value->name; ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label"><?php echo $this->lang->line('description'); ?></label>
                                        <div class="col-md-9">
                                            <textarea name="description" id="machine_description" class="form-control"><?php echo $edit_info->description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-warning">
                                        <div class="box-header"> <label> <?php echo $this->lang->line('photo') ?> </label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="machine_photo_change" class="img-responsive" src="<?php echo base_url($edit_info->photo); ?>" alt="Machine Photo">
                                                <br>
                                                <input type="file" name="photo[]" onchange="readpicture(this)" multiple="">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="reset" class="btn btn-sm btn-warning"><?php echo $this->lang->line('reset'); ?></button>
                                    <button type="submit" class="btn btn-sm btn-primary"><?php echo $this->lang->line('update'); ?></button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-warning box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('machines_photo'); ?> </h3>
                    <div class="box-tools pull-right">
                        
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <?php foreach ($machines_photos as $key => $machines_photo_value) { ?>

                            <div class="col-md-4">
                                <div id="to_be_hide_<?=$machines_photo_value->id?>" class="rzimg-wrap" style="margin-bottom: 5px;">
                                    <span class="rzclose" onclick="deleteThisImage(<?=$machines_photo_value->id?>);">x</span>
                                    <img src="<?php echo base_url($machines_photo_value->photo); ?>" alt="" style="width: 100%">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- /.box-body --> 
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#machine_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };

     function deleteThisImage(id) {

        if (confirm("Are you sure ?")) {

            $.post("<?php echo base_url('admin/delete_machine_photo'); ?>",
                    {id: id},
                    function (data) {
                        $("#to_be_hide_" + id).css('display', 'none');
                        alert("Deletion Complete.")
                    });
        } else
            return false;
    }
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('machine_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>