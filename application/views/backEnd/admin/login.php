<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h4>Login Form</h4>
				<form action="" method="post">
					<div class="form-group">
						<input type="email" name="user_email" placeholder="Enter E-mail">
					</div>
					<div class="form-group">
						<input type="password" name="user_password" placeholder="Enter Pasword">
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<h4>Registration Form</h4>
				<form>
					<div class="form-group">
						<input type="text" name="user_name" placeholder="Enter E-mail">
					</div>
					<div class="form-group">
						<input type="email" name="user_email" placeholder="Enter E-mail">
					</div>
					<div class="form-group">
						<input type="password" name="user_password" placeholder="Enter Pasword">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>





public function news(){
    	    $data = array();
    	    
    	    

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_news['news_title']        = $this->input->post('news_title', true);
    			$insert_news['news_description']        = $this->input->post('news_description', true);
    			/*$insert_certificate['insert_by']   = $_SESSION['userid'];*/
    			$insert_news['insert_time'] = date('Y-m-d H:i:s');

    			
					$config_c['upload_path']    = 'assets/newsPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_news['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
					}
			

				$add_news = $this->db->insert('tbl_latest_news',$insert_news);

				if ($add_news) {

					$this->session->set_flashdata('message','News Added Successfully!');
					redirect('admin/news','refresh');

				} else {

					$this->session->set_flashdata('message','News Add Failed!');
					redirect('admin/news','refresh');

				}
				
    		}

    		$data['title']        = 'Add News';
    		$data['activeMenu']   = 'add_news';
    		$data['page']         = 'backEnd/admin/add_news';

    	}