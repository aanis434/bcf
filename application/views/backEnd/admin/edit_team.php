<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> Update Team </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/team/list" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Team List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/team/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Member Type *</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="member_type" id="" required>
                                                <option value="1" <?=$edit_info->member_type==1?'selected'
                                                :'' ?>>Board of Honour</option>
                                                <option value="2" <?=$edit_info->member_type==2?'selected'
                                                :'' ?>>Executive Committee</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Name *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="name" value="<?php echo $edit_info->name ?>" class="form-control" placeholder="Name" required>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Designation *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="designation" class="form-control"  value="<?php echo $edit_info->designation ?>"  placeholder="Designation" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Facebook URL</label>
                                        <div class="col-md-10">
                                            <input type="text" name="fb_url" value="<?php echo $edit_info->fb_url ?>" class="form-control" placeholder="Facebook URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Twitter URL</label>
                                        <div class="col-md-10">
                                            <input type="text" name="tw_url" value="<?php echo $edit_info->tw_url ?>" class="form-control" placeholder="Twitter URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-2 control-label">LinkedIn URL</label>
                                        <div class="col-md-10">
                                            <input type="text" name="ln_url" value="<?php echo $edit_info->ln_url ?>" class="form-control" placeholder="LinkedIn URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-2 control-label">YouTube URL</label>
                                        <div class="col-md-10">
                                            <input type="text" name="yt_url" value="<?php echo $edit_info->yt_url ?>" class="form-control" placeholder="YouTube URL">
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Description</label>
                                        <div class="col-md-10">
                                            <textarea name="team_description" id="team_description" rows="1" class="form-control"></textarea>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-solid box-primary">
                                        <div class="box-header"> <label> User Photo *</label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="team_photo_change" class="img-responsive" src="<?php echo base_url($edit_info->photo); ?>" alt="Team Photo">
                                                <br>
                                                <input type="file" name="photo" onchange="readpicture(this)">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Team</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#application_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('news_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>