<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i> Team List</h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/team/add" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Team</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userListTable" class="table table-bordered table-striped table_th_primary">
                        <thead>
                            <tr>
                                <th width="10%">SL</th>
                                <th width="10%">Name</th>
                                <th width="10%">Designation</th>
                                <th width="10%">Facebook URl</th>
                                <th width="10%">Twitter URl</th>
                                <th width="10%">Linkedn URl</th>
                                <th width="10%">Youtube URl</th>
                                <th width="20%">Photo</th>
                                <th width="20%">Member Type</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($all_team_member as $key => $all_team_member_value) { ?>
                                 <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $all_team_member_value->name; ?></td>
                                <td><?php echo $all_team_member_value->designation; ?></td>
                                <td><?php echo $all_team_member_value->fb_url; ?></td>
                                <td><?php echo $all_team_member_value->tw_url; ?></td>
                                <td><?php echo $all_team_member_value->ln_url; ?></td>
                                <td><?php echo $all_team_member_value->yt_url; ?></td>
                                
                                <td> <img src="<?= base_url($all_team_member_value->photo); ?>" class="img img-responsive" style="width: 50px; height: 50px;"> </td>
                                <td><?=$all_team_member_value->member_type==1?'Board of Honour':'Executive'; ?></td>
                                <td> 
                                    <a href="<?php echo base_url('admin/team/edit/'.$all_team_member_value->id) ;?>" class="btn btn-sm bg-purple"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url('admin/team/delete/'.$all_team_member_value->id) ;?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                           <?php } ?>
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
</script>

