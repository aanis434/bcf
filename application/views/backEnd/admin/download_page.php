<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-teal box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('download_page'); ?> </h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <?php if(isset($edit_info)){ ?>
                    <div class="row">
                        <form action="<?php echo base_url('admin/download_page/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('title'); ?> *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="title" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('title'); ?>" required value="<?php echo $edit_info->title; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('classification'); ?></label>
                                    <div class="col-md-8">
                                        <input type="text" name="classification" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('classification'); ?>" value="<?php echo $edit_info->classification; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('priority'); ?> *</label>
                                    <div class="col-md-8">
                                        <input type="number" name="priority" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('priority'); ?>" min="1" required value="<?php echo $edit_info->priority; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('attachment'); ?> *</label>
                                    <div class="col-md-8">
                                        <img src="<?php echo base_url($edit_info->attachment); ?>" alt="" id="download_page_photo_change" alt="Download Attachment"><br><br>
                                        <input type="file" name="attachment" onchange="readpicture(this)" required="">
                                    </div>
                                </div>
                                <center>
                                    <button type="reset" class="btn btn-sm bg-red"><?php echo $this->lang->line('cancel'); ?></button>
                                    <button type="submit" class="btn btn-sm bg-teal"><?php echo $this->lang->line('update'); ?></button>
                                </center>
                            </div>
                        </form>
                    </div>
                    <?php }else {?>
                    <div class="row">
                        <form action="<?php echo base_url('admin/download_page/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('title'); ?> *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="title" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('title'); ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('classification'); ?></label>
                                    <div class="col-md-8">
                                        <input type="text" name="classification" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('classification'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('priority'); ?> *</label>
                                    <div class="col-md-8">
                                        <input type="number" name="priority" class="form-control inner_shadow_teal" placeholder="<?php echo $this->lang->line('priority'); ?>" min="1" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('attachment'); ?> *</label>
                                    <div class="col-md-8">
                                        <img src="//placehold.it/100x100" alt="" id="download_page_photo_change" alt="Download Attachment"><br><br>
                                        <input type="file" name="attachment" onchange="readpicture(this)" required>
                                    </div>
                                </div>
                                <center>
                                    <button type="reset" class="btn btn-sm bg-red"><?php echo $this->lang->line('reset'); ?></button>
                                    <button type="submit" class="btn btn-sm bg-teal"><?php echo $this->lang->line('save'); ?></button>
                                </center>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="userListTable" class="table table-bordered table-striped table_th_teal">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;"><?php echo $this->lang->line('sl'); ?></th>
                                        <th style="width: 50%;"><?php echo $this->lang->line('title'); ?></th>
                                        <th style="width: 25%;"><?php echo $this->lang->line('classification'); ?></th>
                                        <th style="width: 10%;"><?php echo $this->lang->line('priority'); ?></th>
                                        <th style="width: 10%;"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($download_pages as $key => $download_page_value) {
                                        	?>
                                    <tr>
                                        <td> <?= ++$key; ?> </td>
                                        <td> <?= $download_page_value->title; ?> </td>
                                        <td> <?= $download_page_value->classification; ?> </td>
                                        <td> <?= $download_page_value->priority; ?> </td>
                                        <td>
                                            <a href="<?= base_url('admin/download_page/edit/'.$download_page_value->id); ?>" class="btn btn-sm bg-teal"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/download_page/delete/'.$download_page_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-red"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class=" box-footer">
                </div>
                <!-- /.box-footer --> 
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#download_page_photo_change')
            .attr('src', e.target.result)
            .width(100)
            .height(100);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>