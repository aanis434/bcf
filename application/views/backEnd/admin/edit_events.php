<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> Add Event </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/events/list" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Event List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/events/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Event Type *</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="type" id="" required>
                                                <option value="road" <?=$edit_info->type=='road'?'selected':''?>>Road Event</option>
                                                <option value="track" <?=$edit_info->type=='track'?'selected':''?>>Track Event</option>
                                                <option value="indoor-cycling" <?=$edit_info->type=='indoor-cycling'?'selected':''?>>Indoor Cycling</option>
                                                <option value="para-cycling" <?=$edit_info->type=='para-cycling'?'selected':''?>>Para Cycling</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Title *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="events_title" value="<?php echo $edit_info->title; ?>" class="form-control" placeholder="Title" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Location *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="location" value="<?php echo $edit_info->title; ?>" class="form-control" placeholder="Location" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Start Date *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="start_date" value="<?php echo $edit_info->start_date; ?>" class="form-control" placeholder="Event Start Date" id="datepicker_1" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">End Date *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="end_date" value="<?php echo $edit_info->end_date; ?>" class="form-control" placeholder="Event End Date" id="datepicker_2" required>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Description</label>
                                        <div class="col-md-10">
                                            <textarea name="events_description" id="events_description" rows="1" class="form-control"><?php echo $edit_info->detail; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-solid box-primary">
                                        <div class="box-header"> <label> Photo *</label> </div>
                                        <div class="box-body box-profile">
                                          <center>
                                                <img id="events_photo_change" class="img-responsive" src="<?php echo base_url($edit_info->photo); ?>" alt="News Photo">
                                                <br>
                                                <input name="photo" type="file" onchange="readpicture(this)">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $('#datepicker_1, #datepicker_2').datepicker({
        "setDate": new Date(),
        "autoclose": true
});
    $(function () {
        $("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#events_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('events_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>