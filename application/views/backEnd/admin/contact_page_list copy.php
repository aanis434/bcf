
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $this->lang->line("contact_list"); ?></h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url('admin/contactpage') ?>" class="btn bg-green btn-sm"><i class="fa fa-plus"></i> <?php echo $this->lang->line("contact_add"); ?></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userListTable" class="table table-responsive table-bordered table-striped table_th_maroon ">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('sl'); ?></th>
                                <th>Footer Description</th>
                                <th><?php echo $this->lang->line('address'); ?></th>
                                <th><?php echo $this->lang->line('phone'); ?></th>
                                <th><?php echo $this->lang->line('email'); ?></th>
                                <th><?php echo $this->lang->line('tel'); ?></th>
                                <th>Google Map</th>
                                <th>Website URL</th>
                                <th><?php echo $this->lang->line('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody class="table-responsive">
                            <?php
                                foreach ($contact_page_list as $key => $contact_page_list_value) {
                                	?>
                            <tr>
                                <td> <?php echo ++$key; ?> </td>
                                <td> <?php echo $contact_page_list_value->detail; ?> </td>
                                <td> <?php echo $contact_page_list_value->address; ?> </td>
                                <td> <?php echo $contact_page_list_value->phone; ?> </td>
                                <td> <?php echo $contact_page_list_value->email; ?> </td>
                                <td> <?php echo $contact_page_list_value->tel; ?> </td>
                                <td> <?php echo !empty($contact_page_list_value->g_map)?'Yes':'NO'; ?> </td>
                                <td> <?php echo $contact_page_list_value->w_url; ?> </td>
                                <td>
                                    <a href="<?php echo base_url('admin/contactpage/edit/'.$contact_page_list_value->id); ?>" class="btn-sm btn bg-green" style="color: white;"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url('admin/contactpage/delete/'.$contact_page_list_value->id); ?>" class="btn-sm btn bg-red" style="color: white;" onclick="return confirm('Are You Sure?');"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
      $("#userListTable").DataTable();
    });
    
</script>