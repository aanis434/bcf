<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> Add Follow </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/follow_us/list" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Follow List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/follow_us/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">             
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label">Facebook URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="fb_url" class="form-control" placeholder="Facebook URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-3 control-label">Twitter URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="tw_url" class="form-control" placeholder="Twitter URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-3 control-label">LinkedIn URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="ln_url" class="form-control" placeholder="LinkedIn URL">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="" class="col-md-3 control-label">YouTube URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="yt_url" class="form-control" placeholder="YouTube URL">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label">Pinterest URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="pi_url" class="form-control" placeholder="Pinterest URL">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-3 control-label">Instagram URL</label>
                                        <div class="col-md-9">
                                            <input type="text" name="ins_url" class="form-control" placeholder="Instagram URL">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i> Cancel</button>
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Follow</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#team_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('team_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>