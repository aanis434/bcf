<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-purple box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('application_type'); ?> </h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url('admin/application'); ?>" class="btn btn-sm bg-green" style="color: white"><i class="fa fa-plus"></i> <?php echo $this->lang->line('application'); ?></a>
                    </div>
                </div>
                <div class="box-body">
                    <?php if(isset($edit_info)){ ?>
                    <div class="row">
                        <br>
                        <form action="<?php echo base_url('admin/application_type/edit/'.$edit_info->id); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> <?php echo $this->lang->line('name'); ?> *</label>
                                        <div class="col-sm-8">
                                            <input name="name" class="form-control" placeholder="<?php echo $this->lang->line('name'); ?>" required="" type="text" value="<?php echo $edit_info->name; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="form-control btn bg-purple"> <?php echo $this->lang->line('update'); ?> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php }?>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="userListTable" class="table table-bordered table-striped table_th_purple">
                                <thead>
                                    <tr>
                                        <th style="width: 10%"><?php echo $this->lang->line('sl'); ?></th>
                                        <th style="width: 80%"><?php echo $this->lang->line('name'); ?></th>
                                        <th style="width: 10%"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($application_type as $key => $application_type_value) {
                                        	?>
                                    <tr>
                                        <td> <?= ++$key; ?> </td>
                                        <td> <?= $application_type_value->name; ?> </td>
                                        <td>
                                            <a href="<?= base_url('admin/application_type/edit/'.$application_type_value->id); ?>" class="btn btn-sm bg-purple"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/application_type/delete/'.$application_type_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-red"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>

<script type="text/javascript">

    $(function () {
        $("#userListTable").DataTable();
    });
    
</script>