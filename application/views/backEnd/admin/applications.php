<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-purple box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <?php echo $this->lang->line('application'); ?> </h3>
                    <div class="box-tools pull-right">
                        <a class="btn btn-sm bg-olive" style="color: white" data-target="#application_type_modal" data-toggle="modal"><i class="fa fa-plus"></i> <?php echo $this->lang->line('application_type_add'); ?></a>
                        <a href="<?php echo base_url('admin/application_type'); ?>" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> <?php echo $this->lang->line('application_type_list'); ?></a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/application/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('name'); ?> *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="name" class="form-control" placeholder="<?php $this->lang->line('name'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('application_type'); ?> *</label>
                                        <div class="col-md-10">
                                            <select name="application_type_id" id="" class="form-control select2" style="width: 100%;">
                                                <option value=""><?php echo $this->lang->line('select_one'); ?></option>
                                                <?php 

                                                    if ($application_types) {
                                                        foreach ($application_types as $key => $application_type_value) {?>
                                                    <option value="<?php echo $application_type_value->id; ?>"><?php echo $application_type_value->name; ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label"><?php echo $this->lang->line('description'); ?></label>
                                        <div class="col-md-10">
                                            <textarea name="description" id="machine_description" rows="1" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-purple">
                                        <div class="box-header"> <label> <?php echo $this->lang->line('photo') ?> *</label> </div>
                                        <div class="box-body box-profile">
                                            <center>
                                                <img id="application_photo_change" class="img-responsive" src="//placehold.it/400x400" alt="Application Photo">
                                                <br>
                                                <input type="file" name="photo[]" onchange="readpicture(this)" multiple="" required="">
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <button type="cancel" class="btn btn-sm btn-danger"><?php echo $this->lang->line('cancel'); ?></button>
                                    <button type="submit" class="btn btn-sm bg-purple"><?php echo $this->lang->line('save'); ?></button>
                                </center>
                            </div>
                        </form>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="userListTable" class="table table-bordered table-striped table_th_purple">
                                <thead>
                                    <tr>
                                        <th style="width: 5%"><?php echo $this->lang->line('sl'); ?></th>
                                        <th style="width: 15%"><?php echo $this->lang->line('name'); ?></th>
                                        <th style="width: 20%"><?php echo $this->lang->line('application_type'); ?></th>
                                        <th style="width: 50%"><?php echo $this->lang->line('description'); ?></th>
                                        <th style="width: 10%"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($applications as $key => $application_value) {
                                        	?>
                                    <tr>
                                        <td> <?= ++$key; ?> </td>
                                        <td> <?= $application_value->name; ?> </td>
                                        <td> <?= $application_value->type_name; ?> </td>
                                        <td> <?= character_limiter(strip_tags($application_value->description),80); ?> </td>
                                        <td>
                                            <a href="<?= base_url('admin/application/edit/'.$application_value->id); ?>" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/application/delete/'.$application_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-red"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body --> 
                <div class="modal fade" id="application_type_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"><?php echo $this->lang->line('application_type_add') ?></h4>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url('admin/application_type/add') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="col-md-12">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"> <?php echo $this->lang->line('name'); ?> *</label>
                                                <div class="col-sm-8">
                                                    <input name="name" class="form-control" placeholder="<?php echo $this->lang->line('name'); ?>" required="" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="form-control btn bg-purple"> <?php echo $this->lang->line('save'); ?> </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                  </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#application_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('machine_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>