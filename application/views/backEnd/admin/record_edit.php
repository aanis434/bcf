<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="fa fa-plus-square" aria-hidden="true"></i> <?=$title?> </h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url(); ?>admin/record_list" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-list"></i> Record Holder List</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form action="<?php echo base_url('admin/record_update') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="id" value="<?=$results[0]['id']?>">
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                  
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Title *</label>
                                        <div class="col-md-10">
                                            <input type="text" value="<?=$results[0]['title']?>" name="title" class="form-control" placeholder="Title" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Name *</label>
                                        <div class="col-md-10">
                                            <input type="text" value="<?=$results[0]['name']?>" name="name" class="form-control" placeholder="Name of record holder" required>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="" class="col-md-2 control-label">Date *</label>
                                        <div class="col-md-10">
                                            <input type="text" name="date" class="form-control" value="<?=Date('m/d/Y',strtotime($results[0]['date']))?>" date-format="Y-m-d" id="datepicker_1" placeholder="Record Date" required>
                                        </div>
                                    </div>
                            <div class="col-md-12">
                                <center>
                                   
                                    <button type="submit" class="btn bg-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
</section>
<script type="text/javascript">
    $('#datepicker_1, #datepicker_2').datepicker({
        "setDate": new Date('Y-m-d'),
        "autoclose": true
});
    $(function () {
    	$("#userListTable").DataTable();
    });
    
    //function for photo
    function readpicture(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('#events_photo_change')
            .attr('src', e.target.result)
            .width(300)
            .height(300);
        };
    
        reader.readAsDataURL(input.files[0]);
    }
    };
    
</script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('events_description')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>