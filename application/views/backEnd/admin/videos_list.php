<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i> Videos List</h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url();?>admin/videos/add" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Videos</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userListTable" class="table table-bordered table-striped table_th_primary">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="20%">Title</th>
                                <th width="45%">Description</th>
                                <th width="10%">video Link</th>
                                <th width="10%">Video Thumb</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($videos as $key => $videos_value) { ?>
                                  <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $videos_value->title; ?></td>
                                <td><?php echo $videos_value->detail; ?></td>
                                <td><?php echo $videos_value->video_link; ?></td>
                                 <td> <img src="<?= base_url($videos_value->photo); ?>" class="img img-responsive"> </td>
                                <td> 
                                   
                                    <a href="<?= base_url('admin/videos/edit/'.$videos_value->id); ?>" class="btn btn-sm bg-purple"><i class="fa fa-edit"></i></a>
                                    <a href="<?= base_url('admin/videos/delete/'.$videos_value->id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                          
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
</script>

