<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i> Event List</h3>
                    <div class="box-tools pull-right">
                        
                        <a href="<?php echo base_url('admin/events/add'); ?>" class="btn btn-sm bg-orange" style="color: white"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Event</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="userListTable" class="table table-bordered table-striped table_th_primary">
                        <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="20%">Title</th>
                                <th width="30%">Description</th>
                                <th width="10%">Start Date</th>
                                <th width="10%">End Date</th>
                                <th width="10%">Location</th>
                                <th width="5%">Photo</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                                 foreach ($all_upcoming_events as $key => $all_upcoming_events_value) {
                                            ?>
                            <tr>
                                        <td> <?= ++$key; ?> </td>
                                        <td> <?= $all_upcoming_events_value->title; ?> </td>
                                        <td> <?= $all_upcoming_events_value->detail; ?> </td>
                                        <td><?=Date('d-M-Y',strtotime($all_upcoming_events_value->start_date))?></td>
                                        <td><?=Date('d-M-Y',strtotime($all_upcoming_events_value->end_date))?> </td>
                                        <td> <?= $all_upcoming_events_value->location; ?> </td>
                                        <td> <img src="<?= base_url($all_upcoming_events_value->photo); ?>" class="img img-responsive" style="width: 50px; height: 50px;"> </td>
                                        <td>
                                            <a href="<?= base_url('admin/events/edit/'.$all_upcoming_events_value->id); ?>" class="btn btn-sm bg-teal"> <i class="fa fa-edit"></i> </a>
                                            <a href="<?= base_url('admin/events/delete/'.$all_upcoming_events_value->id); ?>" onclick="return confirm('Are you sure?')" class="btn btn-sm bg-red"> <i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("#userListTable").DataTable();
    });
    
</script>

