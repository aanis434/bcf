<!-- Start slider area -->
<section class="slider_area">
      <div class="owl-carousel owl-theme" id="home-slider">
            <?php
            foreach ($silde_images as $key => $slider_image_value) {?>
            <div class="item">
                  <img src="<?php echo base_url($slider_image_value->photo); ?>" class="img-responsive" alt="" >
            </div>
            <?php } ?>
      </div>
</section>
<!-- End slider area -->
<!-- Start about area -->
<section class="about_area">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <h4 class="mainTitle">
                        <?php echo $about_content->title; ?></h4>
                  </div>
                  <div class="col-md-6">
                        <div class="about_content">
                              
                              <p class="text-justify">
                                    <?=word_limiter($about_content->body,223); ?>
                              </p>
                        </div>
                  </div>
                  <div class="col-md-6">
                        <div class="about_img">
                              <?php if(file_exists($about_content->attatched)){ ?>
                              <img src="<?php echo base_url($about_content->attatched); ?>" alt="" class="img-responsive">
                              <?php } ?>
                              <div class="video_icon">
                                    <a href="" data-toggle="modal" data-target="#myModal">
                                          <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</section>
<!-- End about area -->
<!-- start lastest news -->
<section class="latest_news_area">
      <div class="container">
            <div class="row">
                  <div class="col-md-6">
                        <h4 class="mainTitle">Latest News</h4>
                  </div>
                  <div class="col-md-6 text-right">
                        <a style="font-size: 16px;text-transform: uppercase;font-weight: 900;" href="<?php echo base_url() ?>site/news">All News <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                  </div>
            </div>
            
            <div class="row">
                  <div class="owl-carousel" id="latest-news">
                        <?php
                        foreach ($get_latest_news as $key => $get_latest_news_value) { ?>
                        <div class="item" >
                              <div class="single_news" style="min-height:600px;max-height:600px">
                                    <img src="<?php echo base_url($get_latest_news_value->photo); ?>" class="img-responsive" alt="">
                                    <a href="<?php echo base_url();?>site/description/<?php echo $get_latest_news_value->id; ?>">
                                          <h4><?php echo $get_latest_news_value->news_title ?></h4>
                                    </a>
                                    <p><?php echo character_limiter(strip_tags($get_latest_news_value->news_description), 150); ?> </p>
                              </div>
                        </div>
                        <?php } ?>
                  </div>
            </div>
      </div>
</section>
<!-- end lastest news -->
<!--Start calender area -->
<section class="calender_area">
      <div class="container">
            <div class="row">
                  <div class="col-md-6"><h4 class="WhiteTitle">OFFICIAL CALENDAR</h4></div>
                  <div class="col-md-6"><ul class="nav nav-pills">
                        
                        <li><a data-toggle="pill" href="#menu1">Road</a></li>
                        <li><a data-toggle="pill" href="#menu2">Track</a></li>
                        <li><a data-toggle="pill" href="#menu3">Indoor Cycling</a></li>
                        <li><a data-toggle="pill" href="#menu4">Para Cycling</a></li>
                  </ul></div>
                  <div class="col-md-12"><div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                              <table class="table table-bordered table-hover" style="background-color: #eee">
                                    <thead>
                                          <tr>
                                                <th>Event Name</th>
                                                <th>Event Date</th>
                                                <th>Location</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                         
                                          <?php foreach($all_events as $event){ ?>
                                          <tr class="bg-success">
                                                <td><?=$event['title']?></td>
                                                <td><?=Date('d-M-Y',strtotime($event['start_date']))?> to <?=Date('d-M-Y',strtotime($event['end_date']))?></td>
                                                <td><?=$event['location']?></td>
                                          </tr>
                                          <?php } ?>
                                          
                                    </tbody>
                              </table>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                              <table class="table table-bordered table-hover" style="background-color: #eee">
                                    <thead>
                                          <tr>
                                                <th>Event Name</th>
                                                <th>Event Date</th>
                                                <th>Location</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          
                                    <?php foreach($road as $event){ ?>
                                          <tr class="bg-success">
                                                <td><?=$event['title']?></td>
                                                <td><?=Date('d-M-Y',strtotime($event['start_date']))?> to <?=Date('d-M-Y',strtotime($event['end_date']))?></td>
                                                <td><?=$event['location']?></td>
                                          </tr>
                                          <?php } ?>
                                    </tbody>
                              </table>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                              <table class="table table-bordered table-hover" style="background-color: #eee">
                                    <thead>
                                          <tr>
                                                <th>Event Name</th>
                                                <th>Event Date</th>
                                                <th>Location</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          
                                    <?php foreach($track as $event){ ?>
                                          <tr class="bg-success">
                                                <td><?=$event['title']?></td>
                                                <td><?=Date('d-M-Y',strtotime($event['start_date']))?> to <?=Date('d-M-Y',strtotime($event['end_date']))?></td>
                                                <td><?=$event['location']?></td>
                                          </tr>
                                          <?php } ?>
                                    </tbody>
                              </table>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                              <table class="table table-bordered table-hover" style="background-color: #eee">
                                    <thead>
                                          <tr>
                                                <th>Event Name</th>
                                                <th>Event Date</th>
                                                <th>Location</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          
                                    <?php foreach($indoor as $event){ ?>
                                          <tr class="bg-success">
                                                <td><?=$event['title']?></td>
                                                <td><?=Date('d-M-Y',strtotime($event['start_date']))?> to <?=Date('d-M-Y',strtotime($event['end_date']))?></td>
                                                <td><?=$event['location']?></td>
                                          </tr>
                                          <?php } ?>
                                    </tbody>
                              </table>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                              <table class="table table-bordered table-hover" style="background-color: #eee">
                                    <thead>
                                          <tr>
                                                <th>Event Name</th>
                                                <th>Event Date</th>
                                                <th>Location</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          
                                    <?php foreach($para as $event){ ?>
                                          <tr class="bg-success">
                                                <td><?=$event['title']?></td>
                                                <td><?=Date('d-M-Y',strtotime($event['start_date']))?> to <?=Date('d-M-Y',strtotime($event['end_date']))?></td>
                                                <td><?=$event['location']?></td>
                                          </tr>
                                          <?php } ?>
                                    </tbody>
                              </table>
                        </div>
                  </div></div>
            </div>
            
            
            
            
      </div>
</section>
<!--End calender area -->

<!-- start upcoming event -->
<!-- start lastest news -->
<section class="latest_news_area" style="margin-top: 30px;">
      <div class="container">
            <div class="row">
                  <div class="col-md-6">
                        <h4 class="mainTitle">Upcoming Events</h4>
                  </div>
                  <div class="col-md-6 text-right">
                        <a style="font-size: 16px;text-transform: uppercase;font-weight: 900;" href="<?php echo base_url();?>site/all-events">All Events <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                  </div>
            </div>
            
            <div class="row">
                  <div class="owl-carousel" id="upcoming_events">
                       <?php foreach ($get_all_events as $key => $get_all_events_value) { ?>
                             
                           <div class="item">
                              <div class="single_news" style="min-height:500px;max-height:500px;">
                                    <img src="<?php echo base_url($get_all_events_value->photo); ?>" class="img-responsive" alt="">
                                    
                                    <ul>
                                          <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $get_all_events_value->start_date; ?> to <?php echo $get_all_events_value->end_date; ?></li>
                                          
                                    </ul>
                                    <a href="<?php echo base_url();?>site/event_description/<?php echo $get_all_events_value->id; ?>">
                                          <h4><?php echo $get_all_events_value->title ?></h4>
                                    </a>
                                    <a href="<?php echo base_url();?>site/event_description/<?php echo $get_all_events_value->id; ?>" class="read_more"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a>
                              </div>
                        </div>


                      <?php } ?>

                        
                  </div>
            </div>
      </div>
</section>
<!-- end lastest news -->
<!-- Start Photo Gallery-->
<section class="portfolio" id="portfolio">
      <div class="container">
            <div class="row">
                  <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4 class="mainTitle">Gallery</h4>
                  </div>
                  <!-- <div class="col-md-6">
                        <div class="text-right">
                              <button class="filter-button" data-filter="all" style="background-color: #29578c; color: #fff">See All</button>
                              <button class="filter-button" data-filter="category1">2016</button>
                              <button class="filter-button" data-filter="category2">2017</button>
                              <button class="filter-button" data-filter="category3">2018</button>
                              <button class="filter-button" data-filter="category4">2019</button>
                        </div>
                  </div> -->
            </div>
            <div class="row">
                  <?php foreach ($get_all_photos as $key => $get_all_photos_value) { ?>
                        <div class="gallery_product col-sm-3 col-xs-6 filter category<?php echo ++$key; ?>">
                        <a class="fancybox" rel="ligthbox" href="<?php echo base_url($get_all_photos_value->photo); ?>">
                              <img class="img-responsive" alt="" src="<?php echo base_url($get_all_photos_value->photo); ?>" style="width: 100%; height: 200px;" />
                              <div class='img-info'>
                                    <h4><?php echo $get_all_photos_value->title; ?></h4>
                                    <!-- <p>
                                          <?php //echo character_limiter(strip_tags($get_all_photos_value->detail), 150); ?>
                                    </p> -->
                                    <!-- <p><?php //echo $get_all_photos_value->detail; ?></p> -->
                              </div>
                        </a>
                  </div>

                  <?php } ?>
            </div>
      </div>
</section>



<!--End Photo Gallery -->
<!-- Start Video Gallery  -->
<section class="video_area">
      <div class="container">
            <h4 class="mainTitle">Latest Video</h4>
            <div class="owl-carousel" id="video_slider">
                  <?php foreach ($get_all_videos as $key => $get_all_videos_value) { ?>
                        <div class="item">
                        <div class="single_video">
                              <img src="<?php echo base_url($get_all_videos_value->photo); ?>" class="img-responsive" alt="">
                              <div class="play_icon">
                                    <a href="#" class="popup-youtube" data-toggle="modal" data-target="#myModal">
                                          <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                              </div>
                              <div class="hoverable_content">
                                    <h4><?php echo $get_all_videos_value->title; ?></h4>
                                    <p><?php echo character_limiter(strip_tags($get_all_videos_value->detail), 100); ?></p>
                              </div>
                        </div>
                  </div>
                   <?php } ?>
                  
            </div>
      </div>
</section>
<!-- End Video Gallery  -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/38XmKNcgjSU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
            </div>
      </div>
</div>
<!-- Start Committee Managment -->
<section class="team_area">
      <div class="container">
            <h4 class="WhiteTitle">Committee Management</h4>
            <div class="row">
                  <div class="owl-carousel" id="team_slider">
                        <?php foreach ($get_all_team as $key => $get_all_team_value) { ?>
                              <div class="item">
                                    <div class="team_info">
                                          <div class="img-hover-effect">
                                           <img src="<?php echo base_url($get_all_team_value->photo); ?>" class="img-responsive" alt="">
                                     </div>
                                     <div class="team_footer_info">
                                          <h4><?php echo $get_all_team_value->name; ?></h4>
                                          <h5><?php echo $get_all_team_value->designation; ?></h5>
                                    </div>
                                    <!-- <div class="social_icon">
                                          <ul>
                                                <li><a href="<?php //echo $get_all_team_value->fb_url; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php //echo $get_all_team_value->tw_url; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php //echo $get_all_team_value->ln_url; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php //echo $get_all_team_value->yt_url; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                          </ul>
                                    </div> -->
                              </div>
                        </div>
                        <?php } ?>
                        
                  </div>

            </div>
      </div>
</section>
<!-- End Committee Managment -->

<!-- Start Partner Are -->
<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
<!-- End Partner Area -->
<!-- <section class="media_area">
      <div class="container">
            <h4 class="text-center">Follow us on</h4>
            <div class="media_icon">
                  <ul>
                        <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
            </div>
      </div>
</section> -->