<section class="news_area">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Latest News</h4>
   	 					</div>
   	 					<div class="col-md-4">
   	 						<div class="text-right">
   	 							<span>Home</span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span> Latest News</span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>
  			<div class="col-md-12">
  				<div class="row">
  					<?php foreach ($get_latest_news as $key => $get_latest_news_value) { ?>
  						<div class="col-md-4">
  						<div class="single_news" style="min-height:600px;">
					<img src="<?php echo base_url($get_latest_news_value->photo); ?>" class="img-responsive" alt="">
					<a href="<?php echo base_url();?>site/description/<?php echo $get_latest_news_value->id; ?>">
						<h4><?php echo $get_latest_news_value->news_title; ?></h4>
					</a>
					<p><?php echo character_limiter(strip_tags($get_latest_news_value->news_description), 70); ?></p>
					<a href="<?=base_url()?>site/description/<?=$get_latest_news_value->id?>" class="read_more">Read More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
				</div>
  					</div>
  					  <?php } ?>
  			</div>
  		</div>
  		<!-- <div class="col-md-12">
			<ul class="pagination">
				<li><a href="#">Pre</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li><a href="#">..</a></li>
				<li><a href="#">Next</a></li>
			</ul>
		</div> -->
  	</div>
  </section>


 <!-- Start Partner Are -->
	<!-- Start Partner Are -->
<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->

