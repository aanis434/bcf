<section class="description_area">
   	 <div class="container">
   	 	<div class="row">
   	 		<div class="col-md-12" style="margin-bottom: 10px">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Event Description</h4>
   	 					</div>
   	 					<div class="col-md-4">
   	 						<div class="text-right">
   	 							<span><a href="<?php echo base_url(); ?>">Home</a></span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span> <a href="<?php echo current_url(); ?>">Event Description</a></span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>
   	 		<div class="col-md-8">
   	 			<div class="wbw_effect">
   	 				<img src="<?php echo base_url($get_single_event_description->photo); ?>" class="img-responsive img-thumbnail" alt="">
   	 				
   	 				<h4><?php echo $get_single_event_description->title; ?></h4>
                    <!-- h5 style="padding-bottom: 10px"><?php echo $get_single_description->insert_time; ?></h5> -->
   	 				<p class="text-justify"><?php echo $get_single_event_description->detail; ?></p>
   	 								<!-- <div class="social_icon" style="padding-top: 30px">
										<ul>
											<li>Share On: </li>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
										</ul>
									</div> -->
   	 			</div>
   	 		</div>
   	 		<div class="col-md-4">
   	 			<div class="more_news">
   	 				<h4 class="title">Upcoming Events</h4>
   	 				<?php foreach ($get_all_events as $key => $get_all_events_value) {
                        
   	 				 ?>
   	 					<div class="media">
   	 					<div class="media-left">
   	 						<img src="<?php echo base_url($get_all_events_value->photo); ?>" class="media-object" style="width:100px">
   	 					</div>
							<a href="<?=base_url()?>site/event_description/<?=$get_all_events_value->id?>" style="color:black; text-decoration:none">
							<div class="media-body">
								<h4 class="media-heading"><?php echo $get_all_events_value->title; ?></h4>
								<!-- <h5 style="padding-bottom: 10px">
									<?php echo date('h:i:s A, d F, Y', strtotime($get_all_events_value->insert_time)); ?></h5> -->

								<p><?php echo character_limiter(strip_tags($get_all_events_value->detail), 70); ?></p>
							</div>
					
							</a>
   	 				</div>
       	 			 <?php	} ?>
   	 			</div>
   	 			
   	 		</div>
   	 	</div>
   	 </div>
   </section>

   
<!-- Start Partner Are -->
<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->

