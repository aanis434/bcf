<section class="circular_area" style="padding-bottom: 50px;">
   	 <div class="container">
   	 	<div class="row">
   	 		<div class="col-md-12">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Circular</h4>
   	 					</div>
   	 					<div class="col-md-3">
   	 						<div class="text-right">
   	 							<span>Home</span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span>Circular</span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>
   	 		<div class="col-md-9">
				<div class="row">
				<?php 
		 		$numOfCols = 3;
				$rowCount = 0;
				$bootstrapColWidth = 12 / $numOfCols;  
		   		foreach ($circular as $key => $value) {
			   	
			   ?>
					<div class="col-md-4">
						<div class="single_circular">
							<a href="">
								<h4><?=Date('d-M-Y',strtotime($value['date']))?></h4>
								<img src="<?php echo base_url();?>front_end_assets/images/pdf.png" class="img-responsive" alt="">
								<h3><?=$value['title']?></h3>
								<a target="_blank" href="<?=base_url($value['file'])?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download</a> 
								<!-- <span class="file-size">(688.35 KB)</span> -->
							</a>
						</div>
					</div>
					<?php   
			$rowCount++;
    		if($rowCount % $numOfCols == 0) echo '</div><div class="row">'; } ?>
				</div>
   	 		</div>
   	 		<div class="col-md-3">
					<h4 class="title">About Us</h4>
					 <div class="other_menu">
					 	<ul>
						<li><a href="<?php echo base_url();?>site/welcome-message"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Welcome Message</a></li>
						<li><a href="<?php echo base_url();?>site/history"><i class="fa fa-angle-double-right" aria-hidden="true"></i> History</a></li>
						<li><a href="<?php echo base_url();?>site/profile"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Profile</a></li>
						
						<li><a href="<?php echo base_url();?>site/mission-statement"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mission Statement</a></li>
						<li><a href="<?php echo base_url();?>site/board-honour"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Board of Honour</a></li>
						
						<li><a href="<?php echo base_url();?>site/management-committee"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Executive Committee</a></li>
						<li><a href="<?php echo base_url();?>site/circular"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Circular</a></li>
						</ul>
					</div>
					
			</div> 	 		
   	 		
   	 	</div>
   	 	<!-- <div class="row">
						<div class="col-md-12">
							<ul class="pagination">
								<li><a href="#">Pre</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">7</a></li>
								<li><a href="#">8</a></li>
								<li><a href="#">9</a></li>
								<li><a href="#">..</a></li>
								<li><a href="#">Next</a></li>
							</ul>
						</div>
					</div> -->
   	 </div>
   </section>




<!-- Start Partner Are -->
 <section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->

	<div class="clearfix" style="padding-bottom: 40px;"></div>
	<section class="fixed_news_bottom">
		<div class="row">
			<div class="col-md-3 pdr-0">
				<div class="news_title">
					<h4>Latest News <i class="fa fa-angle-double-right" aria-hidden="true"></i></h4>
				</div>
			</div>
			<div class="col-md-9 pdl-0">
				<div class="latest_news_list">
					<marquee onmouseover="this.stop();" onmouseout="this.start();">
						<ul>
							<li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>
							<li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>
							<li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>
							<li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>
							<li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>
						</ul>
					</marquee>
				</div>
			</div>
		</div>
	</section>

	
</body>
</html>