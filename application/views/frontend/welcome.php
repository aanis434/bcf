<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bangladesh Cycling Federation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front_end_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front_end_assets/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_end_assets/css/animations.css">
     
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front_end_assets/css/style.css" type="text/css">

    <script src="<?php echo base_url(); ?>front_end_assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/jquery.isotope.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/jquery.fancybox.min.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/animations.js"></script>
    <script src="<?php echo base_url(); ?>front_end_assets/js/main.js"></script>

</head>
<body>

    <!-- Start header area -->
    <header>
        <div class="top_header_navbar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <div class="social_icon"  style="text-align: right;margin-right: 96px;">
                                        <ul>
                                        <li><a href="<?=!empty($this->config->item('fb_url'))?$this->config->item('fb_url'):'javascript:void'?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="<?=!empty($this->config->item('tw_url'))?$this->config->item('tw_url'):'javascript:void'?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="<?=!empty($this->config->item('ln_url'))?$this->config->item('ln_url'):'javascript:void'?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="<?=!empty($this->config->item('yt_url'))?$this->config->item('yt_url'):'javascript:void'?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top_header_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="logo_area">
                            <a href="<?php echo base_url(); ?>">
                                <img src="<?php echo base_url(); ?>/front_end_assets/images/logo.png" alt="Bangladesh Cycling Federation Logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="advertisement_banner">
                        <a href="https://worlds.yorkshire.com/" target="_blank">
                        <img src="<?=base_url()?>front_end_assets/images/banner.png" alt="" class="img-responsive">
                    </a>
                    </div>
     
                    </div>
                </div>
            </div>
        </div>
        <div class="menu_area">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo base_url();?>">Home</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="">About Us <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>site/welcome-message">Welcome Message</a></li>
                                        <li><a href="<?php echo base_url();?>site/history">History</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/profile">Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/mission-statement">Mission Statement</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/board-honour">Board Of Honour</a></li>
                                        
                                        <li><a href="<?php echo base_url(); ?>site/management-committee">Executive Committee</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/circular">Circular</a></li>
                                        
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Rules & Regulations <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>site/rules">Rules & Regulations</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/record">Record Holder List</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>site/news">News</a></li>
                                
                                
                                <li><a href="<?php echo base_url(); ?>site/federation-activities">Activities Of Federation</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="upcoming-event.html">Upcoming Events <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>site/upcoming-event/road">Road</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/upcoming-event/track">Track</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/upcoming-event/indoor-cycling">Indoor Cycling</a></li>
                                        <li><a href="<?php echo base_url(); ?>site/upcoming-event/para-cycling">Para Cycling</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>site/gallery">Gallery</a></li>
                                <li><a href="<?php echo base_url(); ?>site/contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- End header area -->
    
    <!-- Start body content -->
    <?php $this->load->view($contents) ?>
    <!-- End body content -->
   
    <footer>
        <div class="top_footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="company_info">
                            <img src="<?php echo base_url(); ?>front_end_assets/images/logo3.png" alt="">
                            <p><?=!empty($this->config->item('detail'))?word_limiter($this->config->item('detail'),60):'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis.'?></p>
                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="footer_event">
                            <h4>Events</h4>
                            <ul>
                                <li><a href="<?=base_url()?>site/upcoming-event/road"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Road</a></li>
                                <li><a href="<?=base_url()?>site/upcoming-event/track"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Track</a></li>
                                <li><a href="<?=base_url()?>site/upcoming-event/indoor-cycling"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Indoor Cycling</a></li>
                                <li><a href="<?=base_url()?>site/upcoming-event/para-cycling"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Para Cycling</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_about">
                            <h4>About Us</h4>
                            <ul>
                                <li><a href="<?=base_url()?>site/welcome-message"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Welcome Message</a></li>
                                <li><a href="<?=base_url()?>site/history"><i class="fa fa-angle-double-right" aria-hidden="true"></i> History</a></li>
                                <li><a href="<?=base_url()?>site/profile"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Profile</a></li>
                                <li><a href="<?=base_url()?>site/mission-statement"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mission Statement</a></li>
                                <li><a href="<?=base_url()?>site/board-honour"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Board of Honour</a></li>
                                <li><a href="<?=base_url()?>site/management-committee"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Executive of Honour</a></li>
                                <li><a href="<?=base_url()?>site/circular"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Circular</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_contact">
                            <h4>Contact Info</h4>
                            <table class="table">
                                <tr>
                                    <td><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 20px;"></i></td>
                                    <td><?=!empty($this->config->item('address'))?$this->config->item('address'):'6/23, Block - E, Lalmatia Satmasjid Road, Dhaka'?> </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-mobile" aria-hidden="true" style="font-size: 25px;"></i></td>
                                    <td><?=!empty($this->config->item('phone'))?$this->config->item('phone'):'+8801625555007'?> </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-envelope-o" aria-hidden="true"></i></td>
                                    <td><?=!empty($this->config->item('email'))?$this->config->item('email'):'info@ignitesoftbd.com'?></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-globe" aria-hidden="true"></i></td>
                                    <td><?=!empty($this->config->item('w_url'))?$this->config->item('w_url'):'info@ignitesoftbd.com'?></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom_footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copyright">
                            Copyright ©2019. All Rights Reserved
                        </p>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="social_icon">
                                        <ul>
                                            <li><a href="<?=!empty($this->config->item('fb_url'))?$this->config->item('fb_url'):'javascript:void'?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="<?=!empty($this->config->item('tw_url'))?$this->config->item('tw_url'):'javascript:void'?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="<?=!empty($this->config->item('ln_url'))?$this->config->item('ln_url'):'javascript:void'?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="<?=!empty($this->config->item('yt_url'))?$this->config->item('yt_url'):'javascript:void'?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="clearfix" style="padding-bottom: 40px;"></div>
    <section class="fixed_news_bottom">
        <div class="row">
            <div class="col-md-3 pdr-0">
                <div class="news_title">
                    <h4>Latest News <i class="fa fa-angle-double-right" aria-hidden="true"></i></h4>
                </div>
            </div>
            <div class="col-md-9 pdl-0">
                <div class="latest_news_list">
                    
                    <marquee onmouseover="this.stop();" onmouseout="this.start();">
                        <ul>
                            <?php if(!empty($this->config->item('latest_news'))){ 
                                $latest = $this->config->item('latest_news');
                                foreach($latest as $value){
                                ?>
                                <li><a href="<?=base_url()?>site/upcoming-event/<?=$value['type']?>"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?=$value['title']?></a></li>
                                
                                <?php }  }else{ ?>
                           

                             <li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li> <li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li> <li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li> <li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li> <li><a href=""><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Seoul Hosts the 1st Cycling Event Despite Rains</a></li>

                             <?php } ?>
                            
                        </ul>
                    </marquee>
                </div>
            </div>
        </div>
    </section>
    <!-- toastr -->
    <link href="<?=base_url()?>assets/toastr/toastr.min.css" rel="stylesheet"/>
    <script src="<?=base_url()?>assets/toastr/toastr.min.js"></script>
     
        <script>
           // Display an info toast with no title
           var msg = "<?=$this->session->flashdata('message')?>";
           if(msg!=''){
                toastr.success('Thanks For Your Massage!');
           }
               
        </script>   
</body>
</html>