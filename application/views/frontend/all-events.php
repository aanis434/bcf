<section class="latest_news_area" style="margin-top: 30px;">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <h4 class="mainTitle">Upcoming Events</h4>
                  </div>
            </div>
            
            <div class="row" style="margin-bottom: 30px;">
                  
                       <?php 
                       	$numOfCols = 3;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols;  
                       foreach ($get_all_events as $key => $get_all_events_value) { ?>
                             
                           <div class="col-md-4">
                              <div class="single_news">
                                    <img src="<?php echo base_url($get_all_events_value->photo); ?>" class="img-responsive" alt="">
                                    
                                    <ul>
                                          <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $get_all_events_value->start_date; ?> to <?php echo $get_all_events_value->end_date; ?></li>
                                          
                                    </ul>
                                    <a href="<?php echo base_url();?>site/event_description/<?php echo $get_all_events_value->id; ?>">
                                          <h4><?php echo $get_all_events_value->title ?></h4>
                                    </a>
                                    <a href="<?php echo base_url();?>site/event_description/<?php echo $get_all_events_value->id; ?>" class="read_more"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Read More</a>
                              </div>
                        </div>


                      <?php $rowCount++;
    		if($rowCount % $numOfCols == 0) echo '</div><div class="row" style="margin-bottom: 30px;">';  } ?>
            <!-- </div> -->
            <!-- <div class="row">
               <div class="col-md-12">
                     <ul class="pagination">
                        <li><a href="#">Pre</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li><a href="#">..</a></li>
                        <li><a href="#">Next</a></li>
                     </ul>
                  </div>
            </div> -->
      </div>
</section>