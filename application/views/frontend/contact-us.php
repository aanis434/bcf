<section class="contact_area">
         <div class="container">
            <div class="row">
               <div class="col-md-12" style="margin-bottom: 10px">
                  <div class="small_navbar">
                     <div class="row">
                        <div class="col-md-8">
                           <h4>Contact Us</h4>
                        </div>
                        <div class="col-md-4">
                           <div class="text-right">
                              <span>Home</span>
                              <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                              <span> Contact Us</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="col-md-12">
                  <div class="row" style="margin-left: 0;">
                  <div class="con_info" style="background-color: #2b6cb2;min-height: 450px;">
                     <div class="col-md-8" style="padding-left: 0;">
                  <div class="google-map">
                  <iframe src="<?=!empty($get_contact_info)?$get_contact_info[0]['g_map']:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d233667.8223924372!2d90.27923775747219!3d23.780887456211758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b087026b81%3A0x8fa563bbdd5904c2!2sDhaka!5e0!3m2!1sen!2sbd!4v1568307650542!5m2!1sen!2sbd'?>" width="600" height="450" frameborder="0" style="border:0;width: 100%" allowfullscreen=""></iframe>
               </div>
               </div>
               <div class="col-md-4">
                  <div class="contact_info" style="color: #fff;padding-top: 20px;">
                     <h4 style="padding: 0px 0px 24px 0;font-size: 25px;text-transform: uppercase;">Contact Information</h4>
                     <table class="table">
                        <tbody>
                           <tr>
                              <td><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 20px;"></i></td>
                              <td><?=!empty($get_contact_info)?$get_contact_info[0]['address']:'6/23, Block - E, Lalmatia Satmasjid Road, Dhaka'?></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-mobile" aria-hidden="true" style="font-size: 25px;"></i></td>
                              <td><?=!empty($get_contact_info)?$get_contact_info[0]['phone']:'8801625555007'?></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-envelope-o" aria-hidden="true"></i></td>
                              <td><?=!empty($get_contact_info)?$get_contact_info[0]['email']:'info@ignitesoftbd.com'?></td>
                           </tr>
                           <tr>
                              <td><i class="fa fa-globe" aria-hidden="true"></i></td>
                              <td><?=!empty($get_contact_info)?$get_contact_info[0]['w_url']:'ignitesoftbd.com'?></td>
                           </tr>
                           
                        </tbody>
                     </table>
                  </div>
                  <center>
                     <div class="social_icon">
                     <h4 style="padding-bottom: 10px; color: #fff;">Follow Us</h4>
                     <ul>
                        <li><a href="<?=!empty($get_follow_us)?$get_follow_us[0]['fb_url']:'javascript:void'?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="<?=!empty($get_follow_us)?$get_follow_us[0]['tw_url']:'javascript:void'?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="<?=!empty($get_follow_us)?$get_follow_us[0]['ln_url']:'javascript:void'?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="<?=!empty($get_follow_us)?$get_follow_us[0]['yt_url']:'javascript:void'?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
                  </center>
               </div>
                  </div>
               </div>
            </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12 text-center">
                        <h2 style="padding-top: 50px;padding-bottom: 10px; color: #29578c">Contact Us</h2>
                        <h4>It would be great to hear from you! If you got any questions.</h4><br>
                     </div>
                  </div>
                  <div class="form_area">

                     <form action="<?=base_url('site/create_message')?>" method="post" enctype="multipart/form-data">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="">Name (Required)</label>
                              <input type="text" class="form-control" name="name" placeholder="Name" required>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="">Email (Required)</label>
                              <input type="text" class="form-control" name="email" placeholder="Email" required>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="">Phone No (Required)</label>
                              <input type="text" class="form-control" name="phone" placeholder="Phone No" required>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="">Subject (Required)</label>
                              <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="">Message (Required)</label>
                              <textarea name="message" placeholder="Message" rows="10" class="form-control" required></textarea>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Message</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            
         </div>
      </section>

 


