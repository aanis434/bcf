<section class="mission_statement_area">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Mission Statement</h4>
   	 					</div>
   	 					<div class="col-md-4">
   	 						<div class="text-right">
   	 							<span>Home</span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span>Mission Statement</span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>
  			<!-- <div class="col-md-9">
  				<div class="wbw_effect">
  					<img src="images/slider/12.jpg" class="img-responsive" alt="">
  					<p><?php //echo $mission_statement->body; ?></p>
  				</div>
			</div> -->
			<div class="col-md-9">
				<div class="wm_content">
					<div class="spech_area">
						<img src="<?php echo base_url($mission_statement->attatched); ?>" class="img-responsive img-thumbnail" alt="">
						<p>
							<?php echo $mission_statement->body; ?>
						</p>
					</div>
					<!-- <div class="specher_name">
						<h4>Vice President</h4>
						<h3>Cycling Federation Bangladesh</h3>
					</div> -->
				</div>
			</div>
  			<div class="col-md-3">
					<h4 class="title">About Us</h4>
					 <div class="other_menu">
					 	<ul>
						 <li><a href="<?php echo base_url();?>site/welcome-message"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Welcome Message</a></li>
						<li><a href="<?php echo base_url();?>site/history"><i class="fa fa-angle-double-right" aria-hidden="true"></i> History</a></li>
						<li><a href="<?php echo base_url();?>site/profile"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Profile</a></li>
						
						<li><a href="<?php echo base_url();?>site/mission-statement"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mission Statement</a></li>

						<li><a href="<?php echo base_url();?>site/board-honour"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Board of Honour</a></li>
						
						<li><a href="<?php echo base_url();?>site/management-committee"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Executive Committee</a></li>
						<li><a href="<?php echo base_url();?>site/circular"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Circular</a></li>
					</ul>
					 </div>
					
				</div>
  		</div>
  	</div>
  </section>


  <!-- Start Partner Are -->
	<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->

	