<section class="portfolio" id="portfolio" style="padding-top: 50px; padding-bottom: 50px; ">
	<div class="container">
		<div class="row">
          
			<div class="gallery col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<h4 class="mainTitle">Gallery</h4>
			</div>

			<div class="col-md-6">
				<div class="text-right">
				<button class="filter-button" data-filter="all" style="background-color: #29578c; color: #fff">See All</button>
				<button class="filter-button" data-filter="category1">2014</button>
				<button class="filter-button" data-filter="category2">2015</button>
				<button class="filter-button" data-filter="category1">2016</button>
				<button class="filter-button" data-filter="category2">2017</button>
				<button class="filter-button" data-filter="category3">2018</button>
				<button class="filter-button" data-filter="category9">2019</button>
			</div>
			</div>
			
			<div class="row" style="margin-bottom: 30px;">
		   <?php 
		 		$numOfCols = 4;
				$rowCount = 0;
				$bootstrapColWidth = 12 / $numOfCols;  
		   	foreach ($get_all_photos as $key => $get_all_photos_value) {
			   	
			   ?>
		 
            <div class="gallery_product col-sm-<?=$bootstrapColWidth?> col-xs-<?=$bootstrapColWidth?> filter all category9">
                <a class="fancybox" rel="ligthbox" href="<?php echo base_url($get_all_photos_value->photo); ?>">
                    <img class="img-responsive" alt="" src="<?php echo base_url($get_all_photos_value->photo); ?>" style="width: 100%; height: 200px;" />
                    <div class='img-info'>
                        <h4><?php echo $get_all_photos_value->title; ?></h4>
						<!-- <p class="text-justify">
                                          <?php //echo character_limiter(strip_tags($get_all_photos_value->detail), 50); ?>
                                    </p> -->
                    </div>
                </a>
			</div>
			
		
		
			<?php   
			$rowCount++;
    		if($rowCount % $numOfCols == 0) echo '</div><div class="row" style="margin-bottom: 30px;">'; } ?>

            

		</div>
	</div>
</section>


<!-- Start Partner Are -->
<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
				<ul>
					<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

				</ul>
			</div>
		</div>
	</section> -->

	