<section class="profile_area" style="padding-bottom: 50px;">
   	 <div class="container">
   	 	<div class="row">
   	 		<div class="col-md-12">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Executive Committee</h4>
   	 					</div>
   	 					<div class="col-md-4">
   	 						<div class="text-right">
   	 							<span>Home</span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span> Executive Committee</span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>

   	 		<div class="col-md-9">
   	 			<div class="wbw_effect">
   	 				 <div class="row">
   	 				 	<?php foreach ($get_all_team as $key => $get_all_team_value) { ?>
   	 				 		<div class="col-md-4">
   	 				 		<div class="team_info">
                                          <div class="img-hover-effect">
                                           <img src="<?php echo base_url($get_all_team_value->photo); ?>" class="img-responsive" alt="">
                                     </div>
                                     <div class="team_footer_info">
                                          <h4><?php echo $get_all_team_value->name; ?></h4>
                                          <h5><?php echo $get_all_team_value->designation; ?></h5>
                                    </div>
                                    <!-- <div class="social_icon">
                                          <ul>
                                                <li><a href="<?php echo $get_all_team_value->fb_url; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php echo $get_all_team_value->tw_url; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php echo $get_all_team_value->ln_url; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                <li><a href="<?php echo $get_all_team_value->yt_url; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                          </ul>
                                    </div> -->
                              </div>
   	 				 	</div>
   	 				 	<?php } ?>
   	 				 </div>
   	 				 <!-- <div class="row">
						<div class="col-md-12">
							<ul class="pagination">
								<li><a href="#">Pre</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">7</a></li>
								<li><a href="#">8</a></li>
								<li><a href="#">9</a></li>
								<li><a href="#">..</a></li>
								<li><a href="#">Next</a></li>
							</ul>
						</div>
					</div> -->
   	 			</div>
   	 		</div>
   	 		<div class="col-md-3">
					<h4 class="title">About Us</h4>
					 <div class="other_menu">
					 	<ul>
						 <li><a href="<?php echo base_url();?>site/welcome-message"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Welcome Message</a></li>
						<li><a href="<?php echo base_url();?>site/history"><i class="fa fa-angle-double-right" aria-hidden="true"></i> History</a></li>
						<li><a href="<?php echo base_url();?>site/profile"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Profile</a></li>
						
						<li><a href="<?php echo base_url();?>site/mission-statement"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mission Statement</a></li>

						<li><a href="<?php echo base_url();?>site/board-honour"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Board of Honour</a></li>
						
						<li><a href="<?php echo base_url();?>site/management-committee"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Executive Committee</a></li>
						<li><a href="<?php echo base_url();?>site/circular"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Circular</a></li>
					</ul>
					 </div>
					
				</div>
   	 	</div>
   	 </div>
   </section>

<!-- Start Partner Are -->
	<section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>
            </div>
      </div>
</section>

	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->
