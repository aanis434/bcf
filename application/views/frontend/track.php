<section class="road_event" style="margin-bottom: 50px">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
   	 			<div class="small_navbar">
   	 				<div class="row">
   	 					<div class="col-md-8">
   	 						<h4>Road Cycling</h4>
   	 					</div>
   	 					<div class="col-md-4">
   	 						<div class="text-right">
   	 							<span>Home</span>
   	 						<span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
   	 						<span> Road Cycling</span>
   	 						</div>
   	 					</div>
   	 				</div>
   	 			</div>
   	 		</div>
   	 		<div class="col-md-9">
  			<div class="wbw_effect">
  				<table class="table table-bordered table-hover">
  					<thead>
  						<tr class="bg-primary">
  							<th>Event Name</th>
  							<th>Event Date</th>
  							<th>Location</th>
  						</tr>
  					</thead>
  					<tbody>
  						
							<?php foreach($result as $value){ ?>
						<tr class="bg-success">
							<td><?=$value['title']?></td>
  							<td><?=$value['start_date']?> to <?=$value['end_date']?></td>
							<td><?=$value['location']?></td>
						</tr>
							<?php } ?>
  						
  					</tbody>
  				</table>
  			</div>
  		</div>

  		 <div class="col-md-3">
					<h4 class="title">Events</h4>
					 <div class="other_menu">
					 	<ul>
					 		<li><a href="<?=base_url()?>site/upcoming-event/road"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Road</a></li>
					 		<li><a href="<?=base_url()?>site/upcoming-event/track"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Track</a></li>
					 		<li><a href="<?=base_url()?>site/upcoming-event/indoor-cycling"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Indoor Cycling</a></li>
					 		<li><a href="<?=base_url()?>site/upcoming-event/para-cycling"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Para Cycling</a></li>
										
									
					 	</ul>
					 </div>
					
				</div>
  		</div>
  		
  	</div>
  </section>

  <!-- Start Partner Are -->
  <section class="partner_area" style="margin-top: 30px;">
      <div class="container">
            <h4 class="mainTitle text-center">Our <span>Partners</span></h4>
            <div class="owl-carousel" id="partner_slider">
                  <?php foreach ($get_all_partner as $key => $get_all_partner_value) { ?>
                        <div class="item item_box">
                        <img src="<?php echo base_url($get_all_partner_value->photo); ?>" class="img-responsive" alt="">
                  </div>
                  <?php } ?>

                  
            </div>
      </div>
</section>
	<!-- End Partner Area -->

	<!-- <section class="media_area">
		<div class="container">
			<h4 class="text-center">Follow us on</h4>
			<div class="media_icon">
										<ul>
											<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
											<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

										</ul>
									</div>
		</div>
	</section> -->

