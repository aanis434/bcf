public function certificate($param1 = '', $param2 = '', $param3 = '')
    {

        if ($param1 == 'add') {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $insert_certificate['name']        = $this->input->post('name', true);
                $insert_certificate['year']        = $this->input->post('year', true);
                $insert_certificate['insert_by']   = $_SESSION['userid'];
                $insert_certificate['insert_time'] = date('Y-m-d H:i:s');

                if (!empty($_FILES["photo"]['name'])){

                    //exta work
                    $path_parts                 = pathinfo($_FILES["photo"]['name']);
                    $newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
                    $dir                        = date("YmdHis", time());
                    $config_c['file_name']      = $newfile_name . '_' . $dir;
                    $config_c['remove_spaces']  = TRUE;
                    $config_c['upload_path']    = 'assets/certificatePhoto/';
                    $config_c['max_size']       = '20000'; //  less than 20 MB
                    $config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

                    $this->load->library('upload', $config_c);
                    $this->upload->initialize($config_c);
                    if (!$this->upload->do_upload('photo')) {

                    } else {

                        $upload_c = $this->upload->data();
                        $insert_certificate['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
                        $this->image_size_fix($insert_certificate['photo'], 595, 842);
                    }
                }

                $add_certificate = $this->db->insert('tbl_certificate',$insert_certificate);

                if ($add_certificate) {

                    $this->session->set_flashdata('message','Certificate Added Successfully!');
                    redirect('admin/certificate','refresh');

                } else {

                    $this->session->set_flashdata('message','Certificate Add Failed!');
                    redirect('admin/certificate','refresh');

                }
                
            }

        }elseif ($param1 == 'edit' && $param2 > 0) {

            $data['edit_info'] = $this->db->get_where('tbl_certificate',array('id'=>$param2));

            if ($data['edit_info']->num_rows() > 0) {

                $data['edit_info'] = $data['edit_info']->row();

                if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                    $update_certificate['name']        = $this->input->post('name', true);
                    $update_certificate['year']        = $this->input->post('year', true);

                    if (!empty($_FILES["photo"]['name'])){

                        //exta work
                        $path_parts                 = pathinfo($_FILES["photo"]['name']);
                        $newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
                        $dir                        = date("YmdHis", time());
                        $config_c['file_name']      = $newfile_name . '_' . $dir;
                        $config_c['remove_spaces']  = TRUE;
                        $config_c['upload_path']    = 'assets/certificatePhoto/';
                        $config_c['max_size']       = '20000'; //  less than 20 MB
                        $config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

                        $this->load->library('upload', $config_c);
                        $this->upload->initialize($config_c);
                        if (!$this->upload->do_upload('photo')) {

                        } else {

                            $upload_c = $this->upload->data();
                            $update_certificate['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
                            $this->image_size_fix($update_certificate['photo'], 595, 842);
                        }
                    }

                    if ($this->AdminModel->certificate_update($update_certificate, $param2)) {

                        $this->session->set_flashdata('message','Certificate Updated Successfully!');
                        redirect('admin/certificate','refresh');

                    } else {

                        $this->session->set_flashdata('message','Certificate Update Failed!');
                        redirect('admin/certificate','refresh');

                    }
                    
                }

            } else {

                $this->session->set_flashdata('message','Wrong Attempt!');
                redirect('admin/certificate','refresh');
            }
            
            
        }elseif ($param1 == 'delete' && $param2 > 0) {

            if ($this->AdminModel->certificate_delete($param2)) {

                $this->session->set_flashdata('message','Certificate Deleted Successfully!');
                redirect('admin/certificate','refresh');

            } else {

                $this->session->set_flashdata('message','Certificate Delete Failed!');
                redirect('admin/certificate','refresh');

            }
        }

        $data['certificates'] = $this->db->order_by('id','desc')->get('tbl_certificate')->result();

        $data['title']        = 'Certificate';
        $data['activeMenu']   = 'certificate';
        $data['page']         = 'backEnd/admin/certificate';
        $this->load->view('backEnd/master_page',$data);
    }
