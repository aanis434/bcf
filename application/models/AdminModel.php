<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	class AdminModel extends CI_Model{
		
	// $returnmessage can be num_rows, result_array, result
		public function isRowExist($tableName,$data, $returnmessage, $user_id = NULL){
		
				$this->db->where($data);
				if($user_id !== NULL) {
						$this->db->where('userId',$user_id);
				}
				if($returnmessage == 'num_rows'){
						return $this->db->get($tableName)->num_rows();
				}else if($returnmessage == 'result_array'){
						return $this->db->get($tableName)->result_array();
				}else{
						return $this->db->get($tableName)->result();
				}
		}
			// saveDataInTable table name , array, and return type is null or last inserted ID.
		public function saveDataInTable($tableName, $data, $returnInsertId = 'false'){
		
				$this->db->insert($tableName,$data);
				if($returnInsertId == 'true'){
						return $this->db->insert_id();
				}else{
						return -1;
				}
		}

	public function get_all()
    {
        // return $this->db->get('tbl_contact_page');
        return $this->db->query('select * from tbl_contact_page')->result_array(); 
    }
			
		public function check_campaign_ambigus($start_date, $end_date){
					
				if(date_format(date_create($start_date),"Y-m-d") > date_format(date_create($end_date),"Y-m-d")){
						return -2;
					}
		
				$this -> db -> limit(1);
				$this -> db -> where('end_date >=', $start_date);
				$this -> db -> where('available_status', 1);
				$query = $this->db->get('create_campaign')->num_rows();
				if($query > 0){
						return -1;
				}
				return 1;
		}
		
		public function end_date_extends($end_date, $id){
		
				$this -> db -> limit(1);
				$this -> db -> where('start_date >=', $end_date);
				$this -> db -> where('id', $id);
				$this -> db -> where('available_status', 1);
				$query = $this->db->get('create_campaign')->num_rows();
				if($query > 0){
						return -1;
				}
				$this -> db -> limit(1);
				$this -> db -> where('end_date >=', $end_date);
				$this -> db -> where('id !=', $id);
				$this -> db -> where('available_status', 1);
				$query2 = $this->db->get('create_campaign')->num_rows();
				if($query2 > 0){
						return -1;
				}
				return 1;
		}
		public function fetch_data_pageination($limit, $start, $table, $search=NULL, $approveStatus=NULL, $user_id =NULL) {
				
				$this->db->limit($limit, $start);
	
			if($approveStatus!==NULL ){
						$this->db->where('approveStatus',$approveStatus);
				}
	
				if($user_id !== NULL ){
						$this->db->where('userId', $user_id);
				}
	
				if($search !== NULL){
						$this->db->like('title',$search);
						$this->db->or_like('body',$search);
						$this->db->or_like('date',$search);
				}
	
				$this->db->order_by('date','desc');
				$query = $this->db->get($table);
	
				if ($query->num_rows() > 0) {
						foreach ($query->result_array() as $row) {
								$data[] = $row;
						}
						return $data;
				}
				return false;
		}
		public function fetch_images($limit=18, $start=0, $table, $search=NULL,$where_data=NULL) {
				
				$this->db->limit($limit, $start);
	
				if($search !== NULL){
						$this->db->like('date',$search);
						$this->db->or_like('photoCaption',$search);
				}
				if($where_data !== NULL){
						$this->db->where($where_data);
				}
				$this->db->group_by('photo');
				$this->db->order_by('date','desc');
				$query = $this->db->get($table);
	
				if ($query->num_rows() > 0) {
						foreach ($query->result_array() as $row) {
								$data[] = $row;
						}
						return $data;
				}
				return false;
		}
		
		public function usersCategory($userId){
	
				$this->db->select('category.*');
				$this->db->join('category' , 'category_user.categoryId = category.id', 'left');
				$this->db->where('category_user.userId',$userId);
				return $this->db->get('category_user')->result_array();
		}
		
		
		public function get_user($user_id)
		{
			 $query = $this->db->select('user.*,tbl_upozilla.*')
							->where('user.id',$user_id)
							->from('user')
							->join('tbl_upozilla','user.address = tbl_upozilla.id', 'left')
							->get();
	
				return $query->row();
		}
		
		public function update_pro_info($update_data, $user_id)
		{
			 return $this->db->where('id',$user_id)->update('user',$update_data);
		}

		public function update_contact_manage($data, $id)
		{
			return $this->db->where('id',$id)->update('tbl_contact_manage',$data);
		}

		public function slider_add($insert_slider)
		{
			 return $this->db->insert('tbl_slider',$insert_slider);
		}
	
		public function slider_list()
		{
			 return $this->db->order_by('priority','desc')->get('tbl_slider')->result();
		}
		
		public function slider_update($update_slider, $param2)
		{
				if (isset($update_slider['photo']) && file_exists($update_slider['photo'])) {
						
						$query = $this->db->select('photo')->from('tbl_slider')->where('id',$param2)->get();
						$result = $query->row()->photo;
	
						if(file_exists($result)){
								unlink($result);
						}
				}
			 return $this->db->where('id',$param2)->update('tbl_slider',$update_slider);
		}
		public function slider_delete($param2)
		{
				$query = $this->db->select('photo')->from('tbl_slider')->where('id',$param2)->get();
				$result = $query->row()->photo;
	
				if(file_exists($result)){
					 unlink($result);
				}
	
				return $this->db->where('id',$param2)->delete('tbl_slider');
		}


       
        //start News Update
        public function news_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_latest_news')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_latest_news',$data);
		}

		//End News Update
		

		//Start News Delete
		public function news_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_latest_news')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_latest_news');
		}
		//End News Delete
        

        //start Events Update
        public function events_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_upcoming_events')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_upcoming_events',$data);
		}

		//End events Update
		

		//Start events Delete
		public function events_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_upcoming_events')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_upcoming_events');
		}
		//End events Delete
       

        //start photo Update
        public function custom_photo_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_photos')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_photos',$data);
		}

		//End News Update
		

		//Start News Delete
		public function custom_photo_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_photos')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_photos');
		}
		//End News Delete
		


		//start photo Update
        public function custom_video_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_videos')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_videos',$data);
		}
		//End News Update
		

		//Start News Delete
		public function custom_video_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_videos')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_videos');
		}
		//End News Delete
		



		/////start team Update
        public function custom_team_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_team')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_team',$data);
		}

		//End News Update
		

		//Start News Delete
		public function custom_team_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_team')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_team');
		}
		//End News Delete
		
      
   //Start Partner Area
       public function partner_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_partner')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_partner',$data);
		}
		
		public function partner_delete($id)
		{
           $photo = $this->db->where('id', $id)->get('tbl_partner')->row()->photo;
           if(file_exists($photo)) {
           	unlink($photo);
           }
           return $this->db->where('id', $id)->delete('tbl_partner');
		}
    //End Partner Area
    


    //Start Follow Area
       public function follow_update($data, $id)
		{

			return $this->db->where('id',$id)->update('tbl_social_url',$data);
		}
		
		public function follow_delete($id)
		{
          
           return $this->db->where('id', $id)->delete('tbl_social_url');
		}
    //End Follow Area
    







		public function certificate_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_certificate')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_certificate',$data);
		}

		public function certificate_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_certificate')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_certificate');
		}

		public function photo_update($data, $id)
		{
			if (isset($data['photo']) && file_exists($data['photo'])) {

				$photo = $this->db->where('id',$id)->get('tbl_photos')->row()->photo;

				if (file_exists($photo)) {

					unlink($photo);
				}
			}

			return $this->db->where('id',$id)->update('tbl_photos',$data);
		}

		public function photo_delete($id)
		{
			$photo = $this->db->where('id',$id)->get('tbl_photos')->row()->photo;

			if (file_exists($photo)) {

				unlink($photo);
			}

			return $this->db->where('id',$id)->delete('tbl_photos');
		}

		public function get_machine_for_edit($id)
		{
			$this->db->select('tbl_machines.*, tbl_machine_photos.photo')
					 ->where('tbl_machines.id',$id)
					 ->join('tbl_machine_photos','tbl_machines.id = tbl_machine_photos.machine_id','left');
			return $this->db->get('tbl_machines');
		}

		public function get_machines_list()
		{
			$this->db->select('tbl_machines.*, tbl_machine_type.name as type_name')
					 ->join('tbl_machine_type','tbl_machines.machine_type_id = tbl_machine_type.id','left')
					 ->order_by('tbl_machines.id','desc')
					 ->group_by('tbl_machines.id');
			return $this->db->get('tbl_machines')->result();
		}

		public function get_applications_list()
		{
			$this->db->select('tbl_applications.*, tbl_application_type.name as type_name')
					 ->join('tbl_application_type','tbl_applications.application_type_id = tbl_application_type.id','left')
					 ->order_by('tbl_applications.id','desc')
					 ->group_by('tbl_applications.id');
			return $this->db->get('tbl_applications')->result();
		}

		public function get_application_for_edit($id)
		{
			$this->db->select('tbl_applications.*, tbl_application_photos.photo')
					 ->where('tbl_applications.id',$id)
					 ->join('tbl_application_photos','tbl_applications.id = tbl_application_photos.application_id','left');
			return $this->db->get('tbl_applications');
		}

		public function download_page_update($data, $id)
		{
			if (isset($data['attachment']) && file_exists($data['attachment'])) {

				$attachment = $this->db->where('id',$id)
									   ->get('tbl_download_page')
									   ->row()->attachment;

				if (file_exists($attachment)) {

					unlink($attachment);
				}
			}

			return $this->db->where('id',$id)->update('tbl_download_page',$data);
		}

		public function download_page_delete($id)
		{
			$attachment = $this->db->where('id',$id)
									   ->get('tbl_download_page')
									   ->row()->attachment;

			if (file_exists($attachment)) {

				unlink($attachment);
			}

			return $this->db->where('id',$id)->delete('tbl_download_page');
		}
	}
	
?>