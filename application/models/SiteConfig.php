<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php

class SiteConfig extends CI_Model{

    // public function __construct()
    // {
    //     parent::__construct();
    // }

    public function get_all()
    {
        // return $this->db->get('tbl_contact_page');
        return $this->db->query('select * from tbl_contact_page')->result_array(); 
    }

    public function get_follow_us()
    {
        return $this->db->get('tbl_social_url');
    }

    public function get_latest_news()
    {
        $this->db->from('tbl_upcoming_events');
        $this->db->order_by("id", "desc");
        $this->db->limit(10);
    
        $query  = $this->db->get();
        return $query;
    }

    public function update_config($data)
    {
        $success = true;
        foreach($data as $key=>$value)
        {
            if(!$this->save($key,$value))
            {
                $success=false;
                break;
            }
        }
        return $success;
    }

    public function save($key,$value)
    {
        $config_data=array(
            'settingName'=>$key,
            'value'=>$value
        );
        $this->db->where('settingName', $key);
        return $this->db->update('auto_ettings', $config_data);
    }
}
