<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	class FrontEndModel extends CI_Model{
		
		public function get_sliders($value='')
		{
			$res = $this->db->limit(10)->order_by('priority', 'desc')->get('tbl_slider')->result();
			return $res;
		}

		public function get_latest_news(){
           
        //    $res = $this->db->limit(5)->get('tbl_latest_news')->result();
           $res = $this->db->get('tbl_latest_news')->result();
           return $res;
		}
		public function get_all_events(){
           
        //    $res = $this->db->limit(6)->get('tbl_upcoming_events')->result();
           $res = $this->db->get('tbl_upcoming_events')->result();
           return $res;
		}

		public function get_all_photos(){
           
        //    $res = $this->db->limit(6)->get('tbl_photos')->result();
           $res = $this->db->get('tbl_photos')->result();
           return $res;
		}

		public function get_all_videos(){
           
           $res = $this->db->limit(6)->get('tbl_videos')->result();
           return $res;
		}

		public function get_all_team(){
           
           $res = $this->db->get('tbl_team')->result();
        //    $res = $this->db->limit(6)->get('tbl_team')->result();
           return $res;
		}
		public function get_all_partner(){
           
           $res = $this->db->limit(6)->get('tbl_partner')->result();
           return $res;
		}

		public function get_common_pages($name='')
		{
			$res = $this->db->where('name', $name)->get('tbl_common_pages')->row();
			return $res;
		}

		

        
        public function get_application_type_list($limit=10)
		{
			$res = $this->db->limit($limit)->get('tbl_application_type')->result();
			return $res;
		}

		public function get_latest_application($limit=4)
		{
			$res = $this->db->limit($limit)->order_by('insert_time', 'desc')->get('tbl_applications')->result();
			return $res;
		}

		public function get_single_description($id='')
		{
			$res = $this->db->where('id', $id)->get('tbl_latest_news')->row();
			return $res;
		}

		public function get_single_event_description($id='')
		{
			$res = $this->db->where('id', $id)->get('tbl_upcoming_events')->row();
			return $res;
		}

		public function get_application_type_wise($id)
		{
			$this->db->select('tbl_application_photos.photo, tbl_applications.*');
			$this->db->join('tbl_application_photos', 'tbl_application_photos.		application_id=tbl_applications.id', 'left');
			$res = $this->db->where('tbl_applications.application_type_id', $id)->order_by('tbl_applications.insert_time', 'desc')->get('tbl_applications')->result();
			
			return $res; 
		}

		public function get_application_item_details($id='')
		{
			$res = $this->db->where('id', $id)->get('tbl_applications')->row();
			return $res;
		}



		public function get_product_type_list($limit=10)
		{
			$res = $this->db->limit($limit)->get('tbl_machine_type')->result();
			return $res;
		}

		public function get_latest_machines($limit=4)
		{
			$res = $this->db->limit($limit)->order_by('insert_time', 'desc')->get('tbl_machines')->result();
			return $res;
		}


		public function get_machine_type_wise($id=4)
		{
			$this->db->select('tbl_machine_photos.photo, tbl_machines.*');
			$this->db->join('tbl_machine_photos', 'tbl_machine_photos.machine_id=tbl_machines.id', 'left');
			$res = $this->db->where('tbl_machines.machine_type_id', $id)->order_by('tbl_machines.insert_time', 'desc')->get('tbl_machines')->result();
			
			return $res;
		}

		public function get_product_item_details($id='')
		{
			$res = $this->db->where('id', $id)->get('tbl_machines')->row();
			return $res;
		}

		

		public function get_certificates(){
			$res = $this->db->get('tbl_certificate')->result();
			return $res;
		}
		public function get_video(){
			$res = $this->db->get('tbl_videos')->result();
			return $res;
		}

		public function get_download_list(){
			$res = $this->db->get('tbl_download_page')->result();
			return $res;
		}
		public function get_download_details($id ='')
		{
			$res = $this->db->where('id', $id)->get('tbl_download_page')->row();
			return $res;
		}

		public function get_contact_info($branch='', $limit=1)
		{

			$this->db->limit($limit);
			//if($branch == 'Main' || $branch == "Agent") $this->db->where('barnch_name', $branch);
			$res = $this->db->get('tbl_contact_page');
			
			// if($limit == 1) $res = $res->row();

			// else $res = $res->result();
			
			return $res->result_array();
		}

		public function get_follow_us()
		{

			//$this->db->limit($limit);
			//if($branch == 'Main' || $branch == "Agent") $this->db->where('barnch_name', $branch);
			$res = $this->db->get('tbl_social_url');
			
			// if($limit == 1) $res = $res->row();

			// else $res = $res->result();
			
			return $res->result_array();
		}
	}
	
?>