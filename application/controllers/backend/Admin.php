<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {

		parent::__construct();

		$this->lang->load('content', $_SESSION['lang']);

		if (!isset($_SESSION['user_auth']) || $_SESSION['user_auth'] != true) {
			redirect('login', 'refresh');
		}
		if ($_SESSION['userType'] != 'admin')
			redirect('login', 'refresh');
		//Model Loading
		$this->load->model('AdminModel');
		$this->load->library("pagination");
		$this->load->helper("url");
		$this->load->helper("text");

		date_default_timezone_set("Asia/Dhaka");
	}

	public function index() {
		
		$data['title']         = 'Admin Panel • HRSOFTBD News Portal Admin Panel';
		$data['page']          = 'backEnd/dashboard_view';
		$data['activeMenu']    = 'dashboard_view';
		
		$this->load->view('backEnd/master_page', $data);
	}

	public function add_user($param1 = '') {


		$messagePage['divissions'] = $this->db->get('tbl_divission')->result_array();
		$messagePage['userType']   = $this->db->get('user_type')->result_array();

		$messagePage['title']      = 'Add User Admin Panel • HRSOFTBD News Portal Admin Panel';
		$messagePage['page']       = 'backEnd/admin/add_user';
		$messagePage['activeMenu'] = 'add_user';
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$saveData['firstname'] = $this->input->post('first_name', true);
			$saveData['lastname']  = $this->input->post('last_name', true);
			$saveData['username']  = $this->input->post('user_name', true);
			$saveData['email']     = $this->input->post('email', true);
			$saveData['phone']     = $this->input->post('phone', true);
			$saveData['password']  = sha1($this->input->post('password', true));
			$saveData['address']   = $this->input->post('address', true);
			$saveData['roadHouse'] = $this->input->post('road_house', true);
			$saveData['userType']  = $this->input->post('user_type', true);
			$saveData['photo']     = 'assets/userPhoto/defaultUser.jpg';


			//This will returns as third parameter num_rows, result_array, result
			$username_check = $this->AdminModel->isRowExist('user', array('username' => $saveData['username']), 'num_rows');
			$email_check = $this->AdminModel->isRowExist('user', array('email' => $saveData['email']), 'num_rows');

			if ($username_check > 0 || $email_check > 0) {
				//Invalid message
				$messagePage['page'] = 'backEnd/admin/insertFailed';
				$messagePage['noteMessage'] = "<hr> UserName: " . $saveData['username'] . " can not be create.";
				if ($username_check > 0) {

					$messagePage['noteMessage'] .= '<br> Cause this username is already exist.';
				} else if ($email_check > 0) {

					$messagePage['noteMessage'] .= '<br> Cause this email is already exist.';
				}
			} else {
				//success
				$insertId = $this->AdminModel->saveDataInTable('user', $saveData, 'true');

				$messagePage['page'] = 'backEnd/admin/insertSuccessfull';
				$messagePage['noteMessage'] = "<hr> UserName: " . $saveData['username'] . " has been created successfully.";

				// Category allocate for users
				if (!empty($this->input->post('selectCategory', true))) {

					foreach ($this->input->post('selectCategory', true) as $cat_value) {

						$this->db->insert('category_user', array('userId' => $insertId, 'categoryId' => $cat_value));
					}
				}
			}
		}


		$this->load->view('backEnd/master_page', $messagePage);
	}

	public function edit_user($param1 = '') {
		// Update using post method 
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$saveData['firstname'] = $this->input->post('first_name', true, true);
			$saveData['lastname']  = $this->input->post('last_name', true, true);
			$saveData['phone']     = $this->input->post('phone', true, true);
			$saveData['address']   = $this->input->post('address', true, true);
			$saveData['roadHouse'] = $this->input->post('road_house', true, true);
			$saveData['userType']  = $this->input->post('user_type', true, true);
			$user_id               = $this->input->post('user_id', true, true);


			$this->db->where('id', $user_id);
			$this->db->update('user', $saveData);
			
			$data['page']        = 'backEnd/admin/insertSuccessfull';
			$data['noteMessage'] = "<hr> Data has been Updated successfully.";

		} else if ($this->AdminModel->isRowExist('user', array('id' => $param1), 'num_rows') > 0) {

			$data['userDetails']   = $this->AdminModel->isRowExist('user', array('id' => $param1), 'result_array');

			$myupozilla_id         = $this->db->get_where('tbl_upozilla', array("id"=>$data['userDetails'][0]['address']))->row();

			$data['myzilla_id']    = $myupozilla_id->zilla_id;
			$data['mydivision_id'] = $myupozilla_id->division_id;

			$data['divissions']    = $this->db->get('tbl_divission')->result();
		
			$data['distrcts']      = $this->db->get_where('tbl_zilla',array('divission_id'=>$data['mydivision_id']))->result();
			$data['upozilla']      = $this->db->get_where('tbl_upozilla',array('zilla_id'=>$data['myzilla_id']))->result();

			$data['userType']      = $this->db->get('user_type')->result_array();
			$data['user_id']       = $param1;
			$data['page']          = 'backEnd/admin/edit_user';

		} else {

			$data['page']        = 'errors/invalidInformationPage';
			$data['noteMessage'] = $this->lang->line('wrong_info_search');
		}
		
		$data['title']      = 'Users List Admin Panel • HRSOFTBD News Portal Admin Panel';
		$data['activeMenu'] = 'user_list';
		$this->load->view('backEnd/master_page', $data);
	}

	public function suspend_user($id, $setvalue) {

		$this->db->where('id', $id);
		$this->db->update('user', array('status' => $setvalue));
		$this->session->set_flashdata('message', 'Data Saved Successfully.');
		redirect('admin/user_list', 'refresh');
	}

	public function delete_user($id) {

		$old_image_url = $this->db->where('id', $id)->get('user')->row();
		$this->db->where('id', $id)->delete('user');
		if(isset($old_image_url->photo)){
			unlink($old_image_url->photo);
		}

		$this->session->set_flashdata('message', 'Data Deleted.');
		redirect('admin/user_list', 'refresh');
	}

	public function user_list() {

		$this->db->where('userType !=', 'admin');
		$data['myUsers']    = $this->db->get('user')->result_array();
		$data['title']      = 'Users List Admin Panel • HRSOFTBD News Portal Admin Panel';
		$data['page']       = 'backEnd/admin/user_list';
		$data['activeMenu'] = 'user_list';
		$this->load->view('backEnd/master_page', $data);
	}

	public function image_size_fix($filename, $width = 600, $height = 400, $destination = '') {

		// Content type
		// header('Content-Type: image/jpeg');
		// Get new dimensions
		list($width_orig, $height_orig) = getimagesize($filename);

		// Output 20 May, 2018 updated below part
		if ($destination == '' || $destination == null)
			$destination = $filename;

		$extention = pathinfo($destination, PATHINFO_EXTENSION);
		if ($extention != "png" && $extention != "PNG" && $extention != "JPEG" && $extention != "jpeg" && $extention != "jpg" && $extention != "JPG") {
 
			return true;
		}
		// Resample
		$image_p = imagecreatetruecolor($width, $height);
		$image = imagecreatefromstring(file_get_contents($filename));
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

		

		if ($extention == "png" || $extention == "PNG") {
			imagepng($image_p, $destination, 9);
		} else if ($extention == "jpg" || $extention == "JPG" || $extention == "jpeg" || $extention == "JPEG") {
			imagejpeg($image_p, $destination, 70);
		} else {
			imagepng($image_p, $destination);
		}
		return true;
	}

	public function get_division() {

		$result = $this->db->select('id, name')->get('tbl_divission')->result();
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function get_zilla_from_division($division_id = 1) {

		$result = $this->db->select('id, name')->where('divission_id', $division_id)->get('tbl_zilla')->result();
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function get_upozilla_from_division_zilla($zilla_id = 1) {

		$result = $this->db->select('id, name')->where('zilla_id', $zilla_id)->get('tbl_upozilla')->result();
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function download_file($file_name = '', $fullpath='') {

		// echo $file_name; exit();
		$filePath = $file_name;

		if($file_name=='full' && ($fullpath != '' || $fullpath != null)) $filePath = $fullpath;

		if($_GET['file_path']) $filePath = $_GET['file_path'];
		// echo $filePath; exit();
		if (file_exists($filePath)) {
			$fileName = basename($filePath);
			$fileSize = filesize($filePath);

			// Output headers.
			header("Cache-Control: private");
			header("Content-Type: application/stream");
			header("Content-Length: " . $fileSize);
			header("Content-Disposition: attachment; filename=" . $fileName);

			// Output file.
			readfile($filePath);
			exit();
		} else {
			die('The provided file path is not valid.');
		}
	}
	
	public function profile($param1 = '')
	{

		$user_id            = $this->session->userdata('userid');
		$data['user_info']  = $this->AdminModel->get_user($user_id);


		$myzilla_id         = $data['user_info']->zilla_id;
		$mydivision_id      = $data['user_info']->division_id;

		$data['divissions'] = $this->db->get('tbl_divission')->result();

		$data['distrcts']   = $this->db->get_where('tbl_zilla', array('divission_id' => $mydivision_id))->result();
		$data['upozilla']   = $this->db->get_where('tbl_upozilla', array('zilla_id'  => $myzilla_id))->result();

		if ($param1 == 'update_photo') {

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			    
			    
			    //exta work
                $path_parts               = pathinfo($_FILES["photo"]['name']);
                $newfile_name             = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
                $dir                      = date("YmdHis", time());
                $config['file_name']      = $newfile_name . '_' . $dir;
                $config['remove_spaces']  = TRUE;
                //exta work
                $config['upload_path']    = 'assets/userPhoto/';
                $config['max_size']       = '20000'; //  less than 20 MB
                $config['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('photo')) {

                    // case - failure
					$upload_error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('message', "Failed to update image.");

                } else {

                    $upload                 = $this->upload->data();
                    $newphotoadd['photo']   = $config['upload_path'] . $upload['file_name'];

                    $old_photo              = $this->db->where('id', $user_id)->get('user')->row()->photo;
                    
                    if(file_exists($old_photo)) unlink($old_photo);

                    $this->image_size_fix($newphotoadd['photo'], 200, 200);

                    $this->db->where('id', $user_id)->update('user', $newphotoadd);

                    $this->session->set_userdata('userPhoto', $newphotoadd['photo']);
					$this->session->set_flashdata('message', 'User Photo Updated Successfully!');
					
					redirect('admin/profile','refresh');
                }
                
			  }
			  
		}else if($param1 == 'update_pass'){

		   if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		       
			   $old_pass    = sha1($this->input->post('old_pass', true)); 
			   $new_pass    = sha1($this->input->post('new_pass', true)); 
			   $user_id     = $this->session->userdata('userid');

			   $get_user    = $this->db->get_where('user',array('id'=>$user_id, 'password'=>$old_pass));
			   $user_exist  = $get_user->row();

			   if($user_exist){
			       
					$this->db->where('id',$user_id)
							->update('user',array('password'=>$new_pass));
					$this->session->set_flashdata('message', 'Password Updated Successfully');
					redirect('admin/profile','refresh');
					
			   }else{
			       
				    $this->session->set_flashdata('message', 'Password Update Failed');
				    redirect('admin/profile','refresh');
				   
			   }
			   
			}
			
		}else if($param1 == 'update_info'){

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			    
				$update_data['firstname']   = $this->input->post('firstname', true);
				$update_data['lastname']    = $this->input->post('lastname', true);
				$update_data['roadHouse']   = $this->input->post('roadHouse', true);
				$update_data['address']     = $this->input->post('address', true);


				$db_email     = $this->db->where('id!=', $user_id)->where('email', $this->input->post('email', true))->get('user')->num_rows();
				$db_username  = $this->db->where('id!=', $user_id)->where('username', $this->input->post('username', true))->get('user')->num_rows();


				if ( $db_username == 0) {

					 $update_data['username']    = $this->input->post('username', true);
					 
				}if ( $db_email == 0) {

					 $update_data['email']       = $this->input->post('email', true);
					 
				}
				

    			if ($this->AdminModel->update_pro_info($update_data, $user_id)) {
    			    
    			    $this->session->set_userdata('username_first', $update_data['firstname']);
    			    $this->session->set_userdata('username_last', $update_data['lastname']);
    			    $this->session->set_userdata('username', $update_data['username']);
    			    
    				$this->session->set_flashdata('message', 'Information Updated Successfully!');
    				redirect('admin/profile', 'refresh');
    				
    			} else {
    			    
    				$this->session->set_flashdata('message', 'Information Update Failed!');
    				redirect('admin/profile', 'refresh');
    				
    			} 
				
			}
		}
		
		$data['title'] = 'Profile Admin Panel • HRSOFTBD News Portal Admin Panel';
		$data['activeMenu'] = 'Profile';
		$data['page'] = 'backEnd/admin/profile';
		
		$this->load->view('backEnd/master_page',$data);
	}

	public function general($param1 = '', $param2 = '', $param3 = '')
	{
		if ($param1 == 'add') {

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				$insert_general['name']  = $this->input->post('name', true);
				$insert_general['value'] = $this->input->post('value', true);

				$add_general = $this->db->insert('tbl_general',$insert_general);

				if ($add_general) {

					$this->session->set_flashdata('message',"General Add Successfully!");
					redirect('admin/general','refresh');

				} else {

					$this->session->set_flashdata('message',"General Add Failed");
					redirect('admin/general','refresh');
				}
				
			}

		}elseif ($param1 == 'edit') {

					   $param2 = $this->input->post('general_id');

			$data['edit_info'] = $this->db->get_where('tbl_general',array('id'=>$param2));

			if ($data['edit_info']->num_rows() > 0) {;

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {

					$update_general['name']  = $this->input->post('name', true);
					$update_general['value'] = $this->input->post('value', true);

					$general_update = $this->db->where('id',$param2)->update('tbl_general',$update_general);

					if ($general_update) {

						$this->session->set_flashdata('message',"General Update Successfully!");
						redirect('admin/general','refresh');

					} else {

						$this->session->set_flashdata('message',"General Update Failed");
						redirect('admin/general','refresh');
					}
					
				}

			} else {

				$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/general','refresh');
			}
			

		}elseif($param1 == 'delete' && $param2 > 0){

			$delete_general = $this->db->where('id',$param2)->delete('tbl_general');

			if ($delete_general) {

					$this->session->set_flashdata('message',"General Delete Successfully!");
					redirect('admin/general','refresh');

				} else {

					$this->session->set_flashdata('message',"General Delete Failed");
					redirect('admin/general','refresh');
				}
		}

		$data['title']      = 'General Settings';
		$data['activeMenu'] ='general';
		$data['page']       = 'backEnd/admin/general';
		$data['generals']   = $this->db->get('tbl_general')->result();
		$this->load->view('backEnd/master_page',$data);
	}

	public function contactmanage($param1 = 'list', $param2 = '', $param3 = '')
	{
		if ($param1 == 'list') {

			$data['activeMenu']         = 'contact_manage_list';
			$data['title']              = 'Contact Manage List';
			$data['all_contact_manage'] = $this->db->order_by('id','desc')->get('tbl_contact_manage')->result();
			$data['page']               = 'backEnd/admin/contact_manage_list';

		}else if ($param1 == 'edit' && $param2 > 0) {

			$data['activeMenu']          = 'contact_manage_edit';
			$data['title']               = 'Contact Manage Update';
			$data['page']                = 'backEnd/admin/contact_manage_edit';

			$data['contact_manage_edit'] = $this->db->get_where('tbl_contact_manage',array('id'=>$param2));

			if ($data['contact_manage_edit']->num_rows() > 0) {
			   
			   $data['contact_manage_edit'] = $data['contact_manage_edit']->row();
			   $data['contact_edit_id']     = $param2;

			   if ($_SERVER['REQUEST_METHOD'] == 'POST') {

					$update_contact_manage['name']         = $this->input->post('name', true);
					$update_contact_manage['email']        = $this->input->post('email', true);
					$update_contact_manage['subject']      = $this->input->post('subject', true);
					$update_contact_manage['message_body'] = $this->input->post('message_body', true);

					if ($this->AdminModel->update_contact_manage($update_contact_manage, $param2)) {

						$this->session->set_flashdata('message','Contact Manage Updated Successfully!');
						redirect('admin/contactmanage/list','refresh');

					} else {

						$this->session->set_flashdata('message','Contact Manage Update Failed!');
						redirect('admin/contactmanage/list','refresh');
					}
				}

			} else {

				$this->session->set_flashdata('message','Wrong Attempt!');
				redirect ('admin/contactmanage/list','refresh');
			}
			
		}elseif ($param1 == 'delete' && $param2 > 0) {

			$delete_contact_manage = $this->db->where('id',$param2)->delete('tbl_contact_manage');

			if ($delete_contact_manage) {

				$this->session->set_flashdata('message','Contact Manage Delete Successfully!');
				redirect('admin/contactmanage','refresh');

			} else {

				$this->session->set_flashdata('message','Contact Manage Delete Failed!');
				redirect('admin/contactmanage','refresh');
			}
			
		}

		$this->load->view('backEnd/master_page',$data);
	}

	public function slider($param1 = 'add', $param2 = '', $param3 = '')
	{

		$data['title']      = 'Slider Add';
		$data['activeMenu'] = 'slider';
		$data['page']       = 'backEnd/admin/slider';

		if ($param1 == 'add') {

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				$insert_slider['priority']    = $this->input->post('priority', true); 
				$insert_slider['insert_by']   = $this->session->userdata('userid');
				$insert_slider['insert_time'] = date("Y-m-d H:i:s");

				if (!empty($_FILES["photo"]['name'])){

					//exta work
					$path_parts                 = pathinfo($_FILES["photo"]['name']);
					$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
					$dir                        = date("YmdHis", time());
					$config_c['file_name']      = $newfile_name . '_' . $dir;
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/sliderPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					$this->upload->initialize($config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_slider['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						$this->image_size_fix($insert_slider['photo'], 1240, 380);
					}
				}

				if ($this->AdminModel->slider_add($insert_slider)) {

					$this->session->set_flashdata('message', "Slider Added Successfully.");
					redirect('admin/slider/', 'refresh'); 

				} else {

					$this->session->set_flashdata('message', "Slider Add Failed.");
					redirect('admin/slider/', 'refresh'); 
				}
			}
			
		} else if ($param1 == 'edit' && $param2 > 0) {

			$data['title']       = 'Slider Update';
			$data['activeMenu']  = 'slider'; 

			$data['slider_info'] = $this->db->get_where('tbl_slider',array('id'=>$param2));

			if ($data['slider_info']->num_rows() > 0) {

				$data['slider_info']    = $data['slider_info']->row();
				$data['slider_info_id'] = $param2;

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {

					$update_slider['priority'] = $this->input->post('priority', true);  

					if (!empty($_FILES["photo"]['name'])){

						//exta work
						$path_parts                 = pathinfo($_FILES["photo"]['name']);
						$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
						$dir                        = date("YmdHis", time());
						$config_c['file_name']      = $newfile_name . '_' . $dir;
						$config_c['remove_spaces']  = TRUE;
						$config_c['upload_path']    = 'assets/sliderPhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						$this->upload->initialize($config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_slider['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
							$this->image_size_fix($update_slider['photo'], 1240, 380);
						}
					}

					if ($this->AdminModel->slider_update($update_slider,$param2)) {

						$this->session->set_flashdata('message', "Slider Updated Successfully.");
						redirect('admin/slider', 'refresh');

					} else {

						$this->session->set_flashdata('message', "Slider Update Failed.");
						redirect('admin/slider', 'refresh'); 
					}
				}

			}else{

				$this->session->set_flashdata('message', "Wrong Attempt !");
				redirect('admin/slider', 'refresh'); 
			}
		}else if ($param1 == 'delete' && $param2 > 0) {

		   if ($this->AdminModel->slider_delete($param2)) {

				$this->session->set_flashdata('message', "Slider Deleted Successfully.");
				redirect('admin/slider', 'refresh'); 

			} else {

				$this->session->set_flashdata('message', "Slider Delete Failed.");
				redirect('admin/slider', 'refresh'); 
			}
		}


		$data['all_slider'] = $this->AdminModel->slider_list();

		$this->load->view('backEnd/master_page',$data);
	}

	public function contactpage($param1 = 'add', $param2 = '', $param3 = '')
	{
		if ($param1 == 'add') {

			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				$insert_contact_page['detail']        = $this->input->post('footer_description', true);
				$insert_contact_page['address']            = $this->input->post('address', true);
				$insert_contact_page['g_map']              = $this->input->post('g_map', true);
				$insert_contact_page['email']              = $this->input->post('email', true);
				$insert_contact_page['phone']              = $this->input->post('phone', true);
				
				$insert_contact_page['tel']                = $this->input->post('tel', true);
				$insert_contact_page['w_url']              = $this->input->post('w_url', true);

				$contact_page_add = $this->db->insert('tbl_contact_page', $insert_contact_page);

				if ($contact_page_add) {

					$this->session->set_flashdata('message','Contact Added Successfully!');
					redirect('admin/contactpage','refresh');

				} else {

					$this->session->set_flashdata('message','Contact Add Failed!');
					redirect('admin/contactpage','refresh');

				}
				
			}

			$data['title']      = 'Contact Add';
			$data['activeMenu'] = 'contact_page_add';
			$data['page']       = 'backEnd/admin/contact_page_add';

		}elseif ($param1 == 'edit' && $param2 > 0) {

			$data['edit_info'] = $this->db->get_where('tbl_contact_page',array('id'=>$param2));

			if ($data['edit_info']->num_rows() > 0) {

				$data['edit_info'] = $data['edit_info']->row();

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {

					// $update_contact_page['barnch_name']        = $this->input->post('barnch_name', true);
					$update_contact_page['email']              = $this->input->post('email', true);
					$update_contact_page['phone']              = $this->input->post('phone', true);
					$update_contact_page['address']            = $this->input->post('address', true);
					$update_contact_page['tel']                = $this->input->post('tel', true);
					$update_contact_page['w_url']              = $this->input->post('w_url', true);
					$update_contact_page['detail']        = $this->input->post('footer_description', true);
					
					$update_contact_page['g_map']              = $this->input->post('g_map', true);
					// $update_contact_page['company_agent_name'] = $this->input->post('company_agent_name', true);
					// $update_contact_page['contact_person']     = $this->input->post('contact_person', true);

					$contact_page_update = $this->db->where('id',$param2)->update('tbl_contact_page', $update_contact_page);

					if ($contact_page_update) {

						$this->session->set_flashdata('message','Contact Updated Successfully!');
						redirect('admin/contactpage/edit/'.$param2,'refresh');

					} else {

						$this->session->set_flashdata('message','Contact Update Failed!');
						redirect('admin/contactpage/edit/'.$param2,'refresh');

					}
					
				}

			} else {

				$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/contactpage/list','refresh');
			}

			$data['title']      = 'Contact Update';
			$data['activeMenu'] = 'contact_page_edit';
			$data['page']       = 'backEnd/admin/contact_page_edit';

		}elseif ($param1 == 'list') {

			$data['contact_page_list'] = $this->db->get('tbl_contact_page')->result();
			$data['title']             = 'Contact List';
			$data['activeMenu']        = 'contact_page_list';
			$data['page']              = 'backEnd/admin/contact_page_list';
		}elseif ($param1 == 'delete' && $param2 > 0) {

			$delete_contact_page = $this->db->where('id',$param2)->delete('tbl_contact_page');

			if ($delete_contact_page) {

				$this->session->set_flashdata('message','Contact Deleted Successfully!');
				redirect('admin/contactpage/list','refresh');

			} else {

				$this->session->set_flashdata('message','Contact Delete Failed!');
				redirect('admin/contactpage/list','refresh');

			}
		}

		
		$this->load->view('backEnd/master_page',$data);
	}

	public function page_settings($param1 = '', $param2 = '', $param3 = '') {
        
        $data['title']      = 'Page Setting List';
        $data['page']       = 'backEnd/admin/page_settings_list';
        $data['activeMenu'] = 'page_settings';

        if ($param1 == 'edit' && (int) $param2 > 0) {

            $data['title']      = 'Page Setting Update';
            $data['page']       = 'backEnd/admin/page_settings_edit';
            $data['table_info'] = $this->db->where('id', $param2)->get('tbl_common_pages')->row();
            $data['activeMenu'] = 'page_settings';

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                //exta work
				$path_parts                 = pathinfo($_FILES["photo"]['name']);
				$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
				$dir                        = date("YmdHis", time());
				$config['file_name']        = $newfile_name . '_' . $dir;
				$config['remove_spaces']    = TRUE;
				$config['max_size']         = '20000'; //  less than 20 MB
				$config['allowed_types']    = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG|pdf|docx';
                $config['upload_path']      = 'assets/page_settings';

                $old_file_url               = $data['table_info'];
                $update_data['title']       = $this->input->post('title', true);
                $update_data['body']        = $this->input->post('body');

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('photo')) {

                    $this->session->set_flashdata('message', $this->upload->display_errors());
                    $this->db->where('id', $param2)->update('tbl_common_pages', $update_data);
                } else {

                    $upload = $this->upload->data();

                    $update_data['attatched'] = $config['upload_path'] . '/' . $upload['file_name'];
                    $this->db->where('id', $param2)->update('tbl_common_pages', $update_data);
                    $file_parts = pathinfo($update_data['attatched']);
                    if ($file_parts['extension'] != "pdf") {
                        $this->image_size_fix($update_data['attatched'], $width = 2469, $height = 1510);
                    }
                    if(file_exists($old_file_url->attatched)) unlink($old_file_url->attatched);
                }

                $this->session->set_flashdata('message', 'Data Updated Successfully');
                redirect('admin/page_settings', 'refresh');
            }
        }else {
        	$data['table_info'] = $this->db->get('tbl_common_pages')->result_array();
        }

        $this->load->view('backEnd/master_page', $data);
    }

    public function certificate($param1 = '', $param2 = '', $param3 = '')
    {

    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_certificate['name']        = $this->input->post('name', true);
    			$insert_certificate['year']        = $this->input->post('year', true);
    			$insert_certificate['insert_by']   = $_SESSION['userid'];
    			$insert_certificate['insert_time'] = date('Y-m-d H:i:s');

    			if (!empty($_FILES["photo"]['name'])){

					//exta work
					$path_parts                 = pathinfo($_FILES["photo"]['name']);
					$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
					$dir                        = date("YmdHis", time());
					$config_c['file_name']      = $newfile_name . '_' . $dir;
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/certificatePhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					$this->upload->initialize($config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_certificate['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						$this->image_size_fix($insert_certificate['photo'], 595, 842);
					}
				}

				$add_certificate = $this->db->insert('tbl_certificate',$insert_certificate);

				if ($add_certificate) {

					$this->session->set_flashdata('message','Certificate Added Successfully!');
					redirect('admin/certificate','refresh');

				} else {

					$this->session->set_flashdata('message','Certificate Add Failed!');
					redirect('admin/certificate','refresh');

				}
				
    		}

    	}else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_certificate',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_certificate['name']        = $this->input->post('name', true);
	    			$update_certificate['year']        = $this->input->post('year', true);

	    			if (!empty($_FILES["photo"]['name'])){

						//exta work
						$path_parts                 = pathinfo($_FILES["photo"]['name']);
						$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
						$dir                        = date("YmdHis", time());
						$config_c['file_name']      = $newfile_name . '_' . $dir;
						$config_c['remove_spaces']  = TRUE;
						$config_c['upload_path']    = 'assets/certificatePhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						$this->upload->initialize($config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_certificate['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
							$this->image_size_fix($update_certificate['photo'], 595, 842);
						}
					}

					if ($this->AdminModel->certificate_update($update_certificate, $param2)) {

						$this->session->set_flashdata('message','Certificate Updated Successfully!');
						redirect('admin/certificate','refresh');

					} else {

						$this->session->set_flashdata('message','Certificate Update Failed!');
						redirect('admin/certificate','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/certificate','refresh');
    		}
    		
    		
    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->certificate_delete($param2)) {

				$this->session->set_flashdata('message','Certificate Deleted Successfully!');
				redirect('admin/certificate','refresh');

			} else {

				$this->session->set_flashdata('message','Certificate Delete Failed!');
				redirect('admin/certificate','refresh');

			}
    	}

    	$data['certificates'] = $this->db->order_by('id','desc')->get('tbl_certificate')->result();

    	$data['title']        = 'Certificate';
    	$data['activeMenu']   = 'certificate';
    	$data['page']         = 'backEnd/admin/certificate';
    	$this->load->view('backEnd/master_page',$data);
    }

   

    public function photo_gallery($param1 = '', $param2 = '', $param3 = '')
    {
    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_photo_gallery['name'] = $this->input->post('name', true);

				$add_photo_gallery = $this->db->insert('tbl_photo_gallery',$insert_photo_gallery);

				

				if ($add_photo_gallery) {

					$this->session->set_flashdata('message','Photo Gallery Added Successfully!');
					redirect('admin/photo','refresh');

				} else {

					$this->session->set_flashdata('message','Photo Gallery Add Failed!');
					redirect('admin/photo','refresh');

				}
				
    		}

    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_photo_gallery',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			
	    			$update_photo_gallery['name'] = $this->input->post('name', true);

	    			$photo_gallery_update = $this->db->where('id',$param2)->update('tbl_photo_gallery',$update_photo_gallery);

					if ($photo_gallery_update) {

						$this->session->set_flashdata('message','Photo Gallery Updated Successfully!');
						redirect('admin/photo_gallery','refresh');

					} else {

						$this->session->set_flashdata('message','Photo Gallery Update Failed!');
						redirect('admin/photo_gallery','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/photo_gallery','refresh');
    		}
    		
    		
    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		$delete_photo_gallery = $this->db->where('id',$param2)->delete('tbl_photo_gallery');

    		if ($delete_photo_gallery) {

				$this->session->set_flashdata('message','Photo Gallery Deleted Successfully!');
				redirect('admin/photo_gallery','refresh');

			} else {

				$this->session->set_flashdata('message','Photo Gallery Delete Failed!');
				redirect('admin/photo_gallery','refresh');

			}
    	}

    	$data['photo_galleries'] = $this->db->order_by('id','desc')->get('tbl_photo_gallery')->result();

    	$data['title']        = 'Photo Gallery';
    	$data['activeMenu']   = 'photo';
    	$data['page']         = 'backEnd/admin/photo_gallery';
    	$this->load->view('backEnd/master_page',$data);
    }

    public function machines_type($param1 = '', $param2 = '', $param3 = '')
    {
    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_machines_type['name'] = $this->input->post('name', true);

				$add_machines_type = $this->db->insert('tbl_machine_type',$insert_machines_type);

				if ($add_machines_type) {

					$this->session->set_flashdata('message','Machine Type Added Successfully!');
					redirect('admin/machines','refresh');

				} else {

					$this->session->set_flashdata('message','Machine Type Add Failed!');
					redirect('admin/machines','refresh');

				}
				
    		}

    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_machine_type',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			
	    			$update_machines_type['name'] = $this->input->post('name', true);

	    			$machines_type_update = $this->db->where('id',$param2)->update('tbl_machine_type',$update_machines_type);

					if ($machines_type_update) {

						$this->session->set_flashdata('message','Machine Type Updated Successfully!');
						redirect('admin/machines_type','refresh');

					} else {

						$this->session->set_flashdata('message','Machine Type Update Failed!');
						redirect('admin/machines_type','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/machines_type','refresh');
    		}
    		
    		
    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		$delete_machines_type = $this->db->where('id',$param2)->delete('tbl_machine_type');

    		if ($delete_machines_type) {

				$this->session->set_flashdata('message','Machine Type Deleted Successfully!');
				redirect('admin/machines_type','refresh');

			} else {

				$this->session->set_flashdata('message','Machine Type Delete Failed!');
				redirect('admin/machines_type','refresh');

			}
    	}

    	$data['machines_type'] = $this->db->order_by('id','desc')->get('tbl_machine_type')->result();

    	$data['title']        = 'Machine Type';
    	$data['activeMenu']   = 'machine_type';
    	$data['page']         = 'backEnd/admin/machines_type';
    	$this->load->view('backEnd/master_page',$data);
    }








     public function news($param1 = 'add', $param2 = '', $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_news['news_title']        = $this->input->post('news_title', true);
    			$insert_news['news_source']        = $this->input->post('news_source', true);
    			$insert_news['news_description']        = $this->input->post('news_description', true);
    			$insert_news['insert_time'] = date('Y-m-d H:i:s');

				if (!empty($_FILES["photo"]['name'])){

					//exta work
					$path_parts                 = pathinfo($_FILES["photo"]['name']);
					$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
					$dir                        = date("YmdHis", time());
					$config_c['file_name']      = $newfile_name . '_' . $dir;
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/newsPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					$this->upload->initialize($config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_news['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						// $this->image_size_fix($insert_news['photo'], 595, 842);
					}
				}
			

				$add_news = $this->db->insert('tbl_latest_news',$insert_news);

				if ($add_news) {

					$this->session->set_flashdata('message','News Added Successfully!');
					redirect('admin/news/add','refresh');

				} else {

					$this->session->set_flashdata('message','News Add Failed!');
					redirect('admin/news/add','refresh');

				}    		
			}
    		$data['title']        = 'Add News';
    		$data['activeMenu']   = 'add_news';
    		$data['page']         = 'backEnd/admin/add_news';	
    	}

    	else if($param1 == 'list'){
    		$data['title']        = 'News List';
    		$data['activeMenu']   = 'news_list';
    		$data['all_latest_news'] = $this->db->order_by('id','desc')->get('tbl_latest_news')->result();
    		$data['page']         = 'backEnd/admin/news_list';
    	}

    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_latest_news',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				$update_news['news_title']  = $this->input->post('news_title', true);
				$update_news['news_source']  = $this->input->post('news_source', true);
    			$update_news['news_description']  = $this->input->post('news_description', true);
    			$update_news['insert_time'] = date('Y-m-d H:i:s');

    			
				if (!empty($_FILES["photo"]['name'])){

					//exta work
					$path_parts                 = pathinfo($_FILES["photo"]['name']);
					$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
					$dir                        = date("YmdHis", time());
					$config_c['file_name']      = $newfile_name . '_' . $dir;
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/newsPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					$this->upload->initialize($config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$update_news['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						// $this->image_size_fix($update_news['photo'], 595, 842);
					}
				}
			
					

					if ($this->AdminModel->news_update($update_news, $param2)) {

						$this->session->set_flashdata('message','News Updated Successfully!');
						redirect('admin/news','refresh');

					} else {

						$this->session->set_flashdata('message','News Update Failed!');
						redirect('admin/news','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/news','refresh');
    		}
    		
    		$data['title']        = 'Edit News';
    		$data['activeMenu']   = 'edit_news';
    		$data['page']         = 'backEnd/admin/edit_news';
    	}
     
        elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->news_delete($param2)) {

				$this->session->set_flashdata('message','News Deleted Successfully!');
				redirect('admin/news/list','refresh');

			} else {

				$this->session->set_flashdata('message','News Delete Failed!');
				redirect('admin/news/list','refresh');

			}
    	}

    	$this->load->view('backEnd/master_page',$data);
    }


    public function events($param1 = 'add', $param2 = '', $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_events['type'] = $this->input->post('type', true);
    			$insert_events['location'] = $this->input->post('location', true);
    			$insert_events['title'] = $this->input->post('events_title', true);
    			$insert_events['start_date'] = Date('Y-m-d',strtotime($this->input->post('start_date', true)));
    			$insert_events['end_date'] = Date('Y-m-d',strtotime($this->input->post('end_date', true)));
    			$insert_events['detail'] = $this->input->post('events_description', true);

    			$config_c['upload_path']    = 'assets/eventsPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_events['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
					}
			

				$add_events = $this->db->insert('tbl_upcoming_events',$insert_events);

				if ($add_events) {

					$this->session->set_flashdata('message','News Added Successfully!');
					redirect('admin/events/add','refresh');

				} else {

					$this->session->set_flashdata('message','News Add Failed!');
					redirect('admin/events/add','refresh');

				}    		
			}
    		$data['title']        = 'Add Events';
    		$data['activeMenu']   = 'add_events';
    		$data['page']         = 'backEnd/admin/add_events';	
    	}

    	else if($param1 == 'list'){
    		$data['all_upcoming_events'] = $this->db->order_by('id','desc')->get('tbl_upcoming_events')->result();
    		$data['title']        = 'Events List';
    		$data['activeMenu']   = 'events_list';
    		$data['page']         = 'backEnd/admin/events_list';
    	}

    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_upcoming_events',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

				$update_events['type'] = $this->input->post('type', true);
				$update_events['location'] = $this->input->post('location', true);
	    		$update_events['title'] = $this->input->post('events_title', true);
    			$insert_events['start_date'] = Date('Y-m-d',strtotime($this->input->post('start_date', true)));
    			$insert_events['end_date'] = Date('Y-m-d',strtotime($this->input->post('end_date', true)));
    			$update_events['detail'] = $this->input->post('events_description', true);
						
						$config_c['upload_path']    = 'assets/eventsPhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_events['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						}
					

					if ($this->AdminModel->events_update($update_events, $param2)) {

						$this->session->set_flashdata('message','Events Updated Successfully!');
						redirect('admin/events','refresh');

					} else {

						$this->session->set_flashdata('message','Evetns Update Failed!');
						redirect('admin/events','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/events','refresh');
    		}
    		
    		$data['title']        = 'Edit Events';
    		$data['activeMenu']   = 'edit_events';
    		$data['page']         = 'backEnd/admin/edit_events';
    	}
     
        elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->events_delete($param2)) {

				$this->session->set_flashdata('message','Events Deleted Successfully!');
				redirect('admin/events/list','refresh');

			} else {

				$this->session->set_flashdata('message','Events Delete Failed!');
				redirect('admin/events/list','refresh');

			}
    	}

    	$this->load->view('backEnd/master_page',$data);
    }

    public function photo($param1 = 'add', $param2 = '', $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_photo['title'] = $this->input->post('photo_title', true);
    			
    			$insert_photo['detail'] = $this->input->post('photo_description', true);

    			$insert_photo['insert_time'] = date('Y-m-d H:i:s');

    			$config_c['upload_path']    = 'assets/photo/';
					//$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_photo['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
					}
			

				$add_photo = $this->db->insert('tbl_photos',$insert_photo);

				if ($add_photo) {

					$this->session->set_flashdata('message','Photo Added Successfully!');
					redirect('admin/photo/add','refresh');

				} else {

					$this->session->set_flashdata('message','Photo Add Failed!');
					redirect('admin/photo/add','refresh');

				}    		
			}
    		$data['title']        = 'Add Photo';
    		$data['activeMenu']   = 'add_photo';
    		$data['page']         = 'backEnd/admin/add_photo';	
    	}

    	else if($param1 == 'list'){
    		$data['all_photos'] = $this->db->order_by('id','desc')->get('tbl_photos')->result();
    		$data['title']        = 'Photo List';
    		$data['activeMenu']   = 'photo_list';
    		$data['page']         = 'backEnd/admin/photo_list';
    	}

    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_photos',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_photo['title'] = $this->input->post('photo_title', true);
    			
    			$update_photo['detail'] = $this->input->post('photo_description', true);

    			$update_photo['insert_time'] = date('Y-m-d H:i:s');
						$config_c['upload_path']    = 'assets/newsPhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_photo['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						}
					

					if ($this->AdminModel->custom_photo_update($update_photo, $param2)) {

						$this->session->set_flashdata('message','Photo Updated Successfully!');
						redirect('admin/photo','refresh');

					} else {

						$this->session->set_flashdata('message','Photo Update Failed!');
						redirect('admin/photo','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/photo','refresh');
    		}
    		
    		$data['title']        = 'Edit Photo';
    		$data['activeMenu']   = 'edit_photo';
    		$data['page']         = 'backEnd/admin/edit_photo';
    	}

        elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->custom_photo_delete($param2)) {

				$this->session->set_flashdata('message','Photo Deleted Successfully!');
				redirect('admin/photo/list','refresh');

			} else {

				$this->session->set_flashdata('message','Photo Delete Failed!');
				redirect('admin/photo/list','refresh');

			}
    	}

    	$this->load->view('backEnd/master_page',$data);
    }



    public function videos($param1 = 'add', $param2 = '', $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_videos['title']       = $this->input->post('video_title', true);
    			$insert_videos['insert_time'] = date('Y-m-d H:i:s');

    			$insert_videos['detail']       = $this->input->post('video_description', true);

    			  $insert_videos['video_link']  = str_replace('https://www.youtube.com/watch?v=','https://www.youtube.com/embed/',$this->input->post('video_link', true));
    			

    			$config_c['upload_path']    = 'assets/videosThumb/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload', $config_c);
					if (!$this->upload->do_upload('photo')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_videos['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
					}
			

				$add_videos = $this->db->insert('tbl_videos',$insert_videos);

				if ($add_videos) {

					$this->session->set_flashdata('message','Videos Added Successfully!');
					redirect('admin/videos/add','refresh');

				} else {

					$this->session->set_flashdata('message','Videos Add Failed!');
					redirect('admin/videos/add','refresh');

				}    		
			}
    		$data['title']        = 'Add Videos';
    		$data['activeMenu']   = 'add_videos';
    		$data['page']         = 'backEnd/admin/add_videos';	
    	}

    	else if($param1 == 'list'){
    		$data['videos']     = $this->db->order_by('id','desc')->get('tbl_videos')->result();
	    	$data['title']      = 'Videos List';
	    	$data['activeMenu'] = 'videos_list';
	    	$data['page']       = 'backEnd/admin/videos_list';
    	}

    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_videos',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_videos['title']       = $this->input->post('video_title', true);
    			$update_videos['insert_time'] = date('Y-m-d H:i:s');

    			$update_videos['detail']       = $this->input->post('video_description', true);

    			$update_videos['video_link']  = str_replace('https://www.youtube.com/watch?v=','https://www.youtube.com/embed/',$this->input->post('video_link', true));
    			
						$config_c['upload_path']    = 'assets/videosThumb/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_videos['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						}
					

					if ($this->AdminModel->custom_video_update($update_videos, $param2)) {

						$this->session->set_flashdata('message','Videos Updated Successfully!');
						redirect('admin/videos','refresh');

					} else {

						$this->session->set_flashdata('message','Videos Update Failed!');
						redirect('admin/videos','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/videos','refresh');
    		}
    		
    		$data['title']        = 'Edit Videos';
    		$data['activeMenu']   = 'edit_videos';
    		$data['page']         = 'backEnd/admin/edit_videos';
    	}
     
        elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->custom_video_delete($param2)) {

				$this->session->set_flashdata('message','Videos Deleted Successfully!');
				redirect('admin/videos/list','refresh');

			} else {

				$this->session->set_flashdata('message','Videos Delete Failed!');
				redirect('admin/videos/list','refresh');

			}
    	}

    	$this->load->view('backEnd/master_page',$data);
    }


    public function team($param1 = 'add', $param2 = '', $param3 = ''){

    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_team['name'] = $this->input->post('name', true);
    			$insert_team['designation'] = $this->input->post('designation', true);
    			$insert_team['fb_url'] = $this->input->post('fb_url', true);
    			$insert_team['tw_url'] = $this->input->post('tw_url', true);
    			$insert_team['ln_url'] = $this->input->post('ln_url', true);
    			$insert_team['yt_url'] = $this->input->post('yt_url', true);
    			$insert_team['member_type'] = $this->input->post('member_type');

    			$insert_team['detail'] = $this->input->post('photo_description', true);
/*$insert_team['insert_time'] = date('Y-m-d H:i:s');
*/
$config_c['upload_path']    = 'assets/teamPhoto/';
$config_c['max_size']       = '20000'; //  less than 20 MB
$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';
$this->load->library('upload', $config_c);
if (!$this->upload->do_upload('photo')) {
} else {
	$upload_c = $this->upload->data();
	$insert_team['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
}

$add_team = $this->db->insert('tbl_team',$insert_team);
if ($add_team) {
	$this->session->set_flashdata('message','Team Added Successfully!');
	redirect('admin/team/add','refresh');
} else {
	$this->session->set_flashdata('message','Team Add Failed!');
	redirect('admin/team/add','refresh');
}
}
$data['title']        = 'Add Team';
$data['activeMenu']   = 'add_team';
$data['page']         = 'backEnd/admin/add_team';
}
else if($param1 == 'list'){
	$data['all_team_member'] = $this->db->order_by('id','desc')->get('tbl_team')->result();
	$data['title']        = 'Team List';
	$data['activeMenu']   = 'team_list';
	$data['page']         = 'backEnd/admin/team_list';
}
else if ($param1 == 'edit' && $param2 > 0) {
	$data['edit_info'] = $this->db->get_where('tbl_team',array('id'=>$param2));
	if ($data['edit_info']->num_rows() > 0) {
		$data['edit_info'] = $data['edit_info']->row();
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$update_team['name'] = $this->input->post('name', true);
			$update_team['designation'] = $this->input->post('designation', true);
			$update_team['fb_url'] = $this->input->post('fb_url', true);
			$update_team['tw_url'] = $this->input->post('tw_url', true);
			$update_team['ln_url'] = $this->input->post('ln_url', true);
			$update_team['yt_url'] = $this->input->post('yt_url', true);
			$update_team['member_type'] = $this->input->post('member_type');
			$config_c['upload_path']    = 'assets/teamPhoto/';
$config_c['max_size']       = '20000'; //  less than 20 MB
$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';
$this->load->library('upload', $config_c);
if (!$this->upload->do_upload('photo')) {
} else {
	$upload_c = $this->upload->data();
	$update_team['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
}

if ($this->AdminModel->custom_team_update($update_team, $param2)) {
	$this->session->set_flashdata('message','Team Updated Successfully!');
	redirect('admin/team','refresh');
} else {
	$this->session->set_flashdata('message','Team Update Failed!');
	redirect('admin/team','refresh');
}

}
} else {
	$this->session->set_flashdata('message','Wrong Attempt!');
	redirect('admin/team','refresh');
}

$data['title']        = 'Edit Team';
$data['activeMenu']   = 'edit_team';
$data['page']         = 'backEnd/admin/edit_team';
}

elseif ($param1 == 'delete' && $param2 > 0) {
	if ($this->AdminModel->custom_team_delete($param2)) {
		$this->session->set_flashdata('message','Team Deleted Successfully!');
		redirect('admin/team/list','refresh');
	} else {
		$this->session->set_flashdata('message','Team Delete Failed!');
		redirect('admin/team/list','refresh');
	}
}
$this->load->view('backEnd/master_page',$data);
}




    public function partner($param1 = 'add', $param2 = '', $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
           
            $config_c['upload_path'] = 'assets/partnerPhoto/';
			$config_c['max_size']       = '20000';
			$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

			$this->load->library('upload', $config_c);
			if(!$this->upload->do_upload('photo')){

			}
			else{
				$upload_c = $this->upload->data();
				$insert_partner['photo'] = $config_c['upload_path'].$upload_c['file_name'];
			}

			$add_partner = $this->db->insert('tbl_partner',$insert_partner);

				if ($add_partner) {

					$this->session->set_flashdata('message','Partner Added Successfully!');
					redirect('admin/partner/add','refresh');

				} else {

					$this->session->set_flashdata('message','Partner Add Failed!');
					redirect('admin/partner/add','refresh');

				}
			} 


    		$data['title']        = 'Add Partner';
    		$data['activeMenu']   = 'add_partner';
    		$data['page']         = 'backEnd/admin/add_partner';	
  
    }

    	else if($param1 == 'list'){
    		$data['all_partner'] = $this->db->order_by('id', 'desc')->get('tbl_partner')->result();
    		$data['title']        = 'Partner List';
    		$data['activeMenu']   = 'partner_list';
    		$data['page']         = 'backEnd/admin/partner_list';
    	}


    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_partner',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

						$config_c['upload_path']    = 'assets/partnerPhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload', $config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$update_partner['photo']   = $config_c['upload_path'] . $upload_c['file_name'];
						}
					

					if ($this->AdminModel->partner_update($update_partner, $param2)) {

						$this->session->set_flashdata('message','Partner Updated Successfully!');
						redirect('admin/partner','refresh');

					} else {

						$this->session->set_flashdata('message','Partner Update Failed!');
						redirect('admin/partner','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/partner','refresh');
    		}
    		
    		$data['title']        = 'Edit Partner';
    		$data['activeMenu']   = 'edit_partner';
    		$data['page']         = 'backEnd/admin/edit_partner';
    	}


    	elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->partner_delete($param2)) {

				$this->session->set_flashdata('message','Partner Deleted Successfully!');
				redirect('admin/partner/list','refresh');

			} else {

				$this->session->set_flashdata('message','Events Delete Failed!');
				redirect('admin/partner/list','refresh');

			}
    	}
    	

    	$this->load->view('backEnd/master_page',$data);
    }




 public function follow_us($param1 = 'add', $param2 = 1, $param3 = ''){
    	$data = array();
    	if($param1 == 'add'){
    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    			$insert_follow['fb_url'] = $this->input->post('fb_url', true);
    			$insert_follow['tw_url'] = $this->input->post('tw_url', true);
    			$insert_follow['ln_url'] = $this->input->post('ln_url', true);
    			$insert_follow['yt_url'] = $this->input->post('yt_url', true);
    			$insert_follow['pi_url'] = $this->input->post('pi_url', true);
    			$insert_follow['ins_url'] = $this->input->post('ins_url', true);

				$add_follow = $this->db->insert('tbl_social_url',$insert_follow);

				if ($add_follow) {

					$this->session->set_flashdata('message','Follow Added Successfully!');
					redirect('admin/follow_us/add','refresh');

				} else {

					$this->session->set_flashdata('message','Follow Add Failed!');
					redirect('admin/follow_us/add','refresh');

				}    		
			}
    		$data['title']        = 'Add Follow';
    		$data['activeMenu']   = 'add_follow';
    		$data['page']         = 'backEnd/admin/add_follow';	
    	}

    	else if($param1 == 'list'){
    		$data['all_social_url'] = $this->db->order_by('id','desc')->get('tbl_social_url')->result();
    		$data['title']        = 'follow List';
    		$data['activeMenu']   = 'follow_list';
    		$data['page']         = 'backEnd/admin/follow_list';
    	}

    	else if ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_social_url',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                   $update_follow['fb_url'] = $this->input->post('fb_url', true);
    			$update_follow['tw_url'] = $this->input->post('tw_url', true);
    			$update_follow['ln_url'] = $this->input->post('ln_url', true);
    			$update_follow['yt_url'] = $this->input->post('yt_url', true);
    			$update_follow['pi_url'] = $this->input->post('pi_url', true);
    			$update_follow['ins_url'] = $this->input->post('ins_url', true);
					

					if ($this->AdminModel->follow_update($update_follow, $param2)) {

						$this->session->set_flashdata('message','Follow Updated Successfully!');
						redirect($_SERVER['HTTP_REFERER'],'refresh');

					} else {

						$this->session->set_flashdata('message','Follow Update Failed!');
						redirect('admin/follow_us','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/follow_us','refresh');
    		}
    		
    		$data['title']        = 'Edit Follow';
    		$data['activeMenu']   = 'edit_follow';
    		$data['page']         = 'backEnd/admin/edit_follow';
    	}
     
        else if ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->follow_delete($param2)) {

				$this->session->set_flashdata('message','Follow Deleted Successfully!');
				redirect('admin/follow_us/list','refresh');

			} else {

				$this->session->set_flashdata('message','Follow Delete Failed!');
				redirect('admin/follow_us/list','refresh');

			}
    	}

    	$this->load->view('backEnd/master_page',$data);
    }








    public function machines($param1 = 'add', $param2 = '', $param3 = '')
    {

    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_machine['name']            = $this->input->post('name', true);
    			$insert_machine['description']     = $this->input->post('description', true);
    			$insert_machine['insert_by']       = $_SESSION['userid'];
    			$insert_machine['insert_time']     = date('Y-m-d H:i:s');
    			$insert_machine['machine_type_id'] = $this->input->post('machine_type_id', true);

    			$this->db->insert('tbl_machines',$insert_machine);

    			$machines_id = $this->db->insert_id();

    			$insert_machine_photo['machine_id'] = $machines_id;

    			if (!empty($_FILES["photo"]['name'])){

					//exta work
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/machinePhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload');

					$files = $_FILES;
					for($i=0; $i< count($files['photo']['name']); $i++)
					{           
						$_FILES['photo']['name']     = $files['photo']['name'][$i];
						$_FILES['photo']['type']     = $files['photo']['type'][$i];
						$_FILES['photo']['tmp_name'] = $files['photo']['tmp_name'][$i];
						$_FILES['photo']['error']    = $files['photo']['error'][$i];
						$_FILES['photo']['size']     = $files['photo']['size'][$i]; 

						$path_parts                 = pathinfo($_FILES['photo']['name']);
						$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
						$dir                        = date("YmdHis", time());
						$config_c['file_name']      = $newfile_name . '_' . $dir;


						$this->upload->initialize($config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$insert_machine_photo['photo'] = $config_c['upload_path'] . $upload_c['file_name'];
							$this->image_size_fix($insert_machine_photo['photo'], 400, 400);

							$machine_photo = $this->db->insert('tbl_machine_photos',$insert_machine_photo);
						}    
					}

				}

				if ($machine_photo) {

					$this->session->set_flashdata('message','Machine Added Successfully!');
					redirect('admin/machines','refresh');

				} else {

					$this->session->set_flashdata('message','Machine Add Failed!');
					redirect('admin/machines','refresh');

				}
				


    		}

    		$data['machines']       = $this->AdminModel->get_machines_list();
	    	$data['machines_type'] = $this->db->get('tbl_machine_type')->result();
	    	$data['title']          = 'Machine';
	    	$data['activeMenu']     = 'machine';
	    	$data['page']           = 'backEnd/admin/machines';
    		
    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->AdminModel->get_machine_for_edit($param2);

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();
    			$data['machines_photos'] = $this->db->where('machine_id',$param2)->get('tbl_machine_photos')->result();
    			//print_r($data['machines_photo']);

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_machine['name']            = $this->input->post('name', true);
	    			$update_machine['description']     = $this->input->post('description', true);
	    			$update_machine['machine_type_id'] = $this->input->post('machine_type_id', true);

	    			$this->db->where('id',$param2)->update('tbl_machines',$update_machine);

	    			$machines_id = $param2;

	    			$insert_machine_photo['machine_id'] = $machines_id;

	    			if (!empty($_FILES["photo"]['name'])){

						//exta work
						$config_c['remove_spaces']  = TRUE;
						$config_c['upload_path']    = 'assets/machinePhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload');

						$files = $_FILES;
						for($i=0; $i< count($files['photo']['name']); $i++)
						{           
							$_FILES['photo']['name']     = $files['photo']['name'][$i];
							$_FILES['photo']['type']     = $files['photo']['type'][$i];
							$_FILES['photo']['tmp_name'] = $files['photo']['tmp_name'][$i];
							$_FILES['photo']['error']    = $files['photo']['error'][$i];
							$_FILES['photo']['size']     = $files['photo']['size'][$i]; 

							$path_parts                 = pathinfo($_FILES['photo']['name']);
							$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
							$dir                        = date("YmdHis", time());
							$config_c['file_name']      = $newfile_name . '_' . $dir;


							$this->upload->initialize($config_c);
							if (!$this->upload->do_upload('photo')) {

							} else {

								$upload_c = $this->upload->data();
								$insert_machine_photo['photo'] = $config_c['upload_path'] . $upload_c['file_name'];
								$this->image_size_fix($insert_machine_photo['photo'], 400, 400);

								$machine_photo = $this->db->insert('tbl_machine_photos',$insert_machine_photo);
							}    
						}

					}

					if ($machine_photo) {

						$this->session->set_flashdata('message','Machine Updated Successfully!');
						redirect('admin/machines','refresh');

					} else {

						$this->session->set_flashdata('message','Machine Update Failed!');
						redirect('admin/machines','refresh');

					}
	    		}

    		}else{

    		}

	    	$data['machines_type']  = $this->db->get('tbl_machine_type')->result();
	    	$data['title']          = 'Machine Update';
	    	$data['activeMenu']     = 'machine';
	    	$data['page']           = 'backEnd/admin/machines_edit';

    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		$delete_machine = $this->db->where('id',$param2)->delete('tbl_machines');

    		$photos = $this->db->where('machine_id',$param2)->get('tbl_machine_photos')->result();
    		foreach ($photos as $key => $photo_value) {

    			if (file_exists($photo_value->photo)) {

	    			unlink($photo_value->photo);
	    		}   			
    		}

    		$this->db->where('machine_id',$param2)->delete('tbl_machine_photos');
    		
    		if ($delete_machine) {

				$this->session->set_flashdata('message','Machine Deleted Successfully!');
				redirect('admin/machines','refresh');

			} else {

				$this->session->set_flashdata('message','Machine Delete Failed!');
				redirect('admin/machines','refresh');

			}
    	}
    	$this->load->view('backEnd/master_page',$data);
    }

    public function delete_machine_photo()
    {
    	$machine_photo_id = $this->input->post('id', true);

    	$photo = $this->db->where('id',$machine_photo_id)->get('tbl_machine_photos')->row()->photo;

    	if (file_exists($photo)) {

    		unlink($photo);
    	}

    	$this->db->where('id',$machine_photo_id)->delete('tbl_machine_photos');
    }

    public function delete_application_photo()
    {
    	$application_photo_id = $this->input->post('id', true);

    	$photo = $this->db->where('id',$application_photo_id)->get('tbl_application_photos')->row()->photo;

    	if (file_exists($photo)) {

    		unlink($photo);
    	}

    	$this->db->where('id',$application_photo_id)->delete('tbl_application_photos');
    }

    public function application_type($param1 = '', $param2 = '', $param3 = '')
    {
    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_application_type['name']        = $this->input->post('name', true);
    			$insert_application_type['insert_by']   = $_SESSION['userid'];
    			$insert_application_type['insert_time'] = date('Y-m-d H:i:s');

				$add_application_type = $this->db->insert('tbl_application_type',$insert_application_type);

				if ($add_application_type) {

					$this->session->set_flashdata('message','Application Type Added Successfully!');
					redirect('admin/application','refresh');

				} else {

					$this->session->set_flashdata('message','Application Type Add Failed!');
					redirect('admin/application','refresh');

				}
				
    		}

    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_application_type',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			
	    			$update_application_type['name'] = $this->input->post('name', true);

	    			$application_type_update = $this->db->where('id',$param2)->update('tbl_application_type',$update_application_type);

					if ($application_type_update) {

						$this->session->set_flashdata('message','Application Type Updated Successfully!');
						redirect('admin/application_type','refresh');

					} else {

						$this->session->set_flashdata('message','Application Type Update Failed!');
						redirect('admin/application_type','refresh');

					}
					
	    		}

    		} else {

    			$this->session->set_flashdata('message','Wrong Attempt!');
				redirect('admin/application_type','refresh');
    		}
    		
    		
    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		$delete_application_type = $this->db->where('id',$param2)->delete('tbl_application_type');

    		if ($delete_application_type) {

				$this->session->set_flashdata('message','Application Type Deleted Successfully!');
				redirect('admin/application_type','refresh');

			} else {

				$this->session->set_flashdata('message','Application Type Delete Failed!');
				redirect('admin/application_type','refresh');

			}
    	}

    	$data['application_type'] = $this->db->order_by('id','desc')->get('tbl_application_type')->result();

    	$data['title']        = 'Application Type';
    	$data['activeMenu']   = 'application_type';
    	$data['page']         = 'backEnd/admin/application_type';
    	$this->load->view('backEnd/master_page',$data);
    }

    public function application($param1 = 'add', $param2 = '', $param3 = '')
    {

    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_application['name']            = $this->input->post('name', true);
    			$insert_application['description']     = $this->input->post('description', true);
    			$insert_application['insert_by']       = $_SESSION['userid'];
    			$insert_application['insert_time']     = date('Y-m-d H:i:s');
    			$insert_application['application_type_id'] = $this->input->post('application_type_id', true);

    			$this->db->insert('tbl_applications',$insert_application);

    			$applications_id = $this->db->insert_id();

    			$insert_application_photo['application_id'] = $applications_id;

    			if (!empty($_FILES["photo"]['name'])){

					//exta work
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/applicationPhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

					$this->load->library('upload');

					$files = $_FILES;
					for($i=0; $i< count($files['photo']['name']); $i++)
					{           
						$_FILES['photo']['name']     = $files['photo']['name'][$i];
						$_FILES['photo']['type']     = $files['photo']['type'][$i];
						$_FILES['photo']['tmp_name'] = $files['photo']['tmp_name'][$i];
						$_FILES['photo']['error']    = $files['photo']['error'][$i];
						$_FILES['photo']['size']     = $files['photo']['size'][$i]; 

						$path_parts                 = pathinfo($_FILES['photo']['name']);
						$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
						$dir                        = date("YmdHis", time());
						$config_c['file_name']      = $newfile_name . '_' . $dir;


						$this->upload->initialize($config_c);
						if (!$this->upload->do_upload('photo')) {

						} else {

							$upload_c = $this->upload->data();
							$insert_application_photo['photo'] = $config_c['upload_path'] . $upload_c['file_name'];
							$this->image_size_fix($insert_application_photo['photo'], 400, 400);

							$application_photo = $this->db->insert('tbl_application_photos',$insert_application_photo);
						}    
					}

				}

				if ($application_photo) {

					$this->session->set_flashdata('message','Application Added Successfully!');
					redirect('admin/application','refresh');

				} else {

					$this->session->set_flashdata('message','Application Add Failed!');
					redirect('admin/application','refresh');

				}
				


    		}

    		$data['applications']       = $this->AdminModel->get_applications_list();
	    	$data['application_types']  = $this->db->get('tbl_application_type')->result();
	    	$data['title']              = 'Application';
	    	$data['activeMenu']         = 'application';
	    	$data['page']               = 'backEnd/admin/applications';
    		
    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->AdminModel->get_application_for_edit($param2);

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();
    			$data['applications_photos'] = $this->db->where('application_id',$param2)->get('tbl_application_photos')->result();
    			//print_r($data['applications_photo']);

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_application['name']            = $this->input->post('name', true);
	    			$update_application['description']     = $this->input->post('description', true);
	    			$update_application['application_type_id'] = $this->input->post('application_type_id', true);

	    			$this->db->where('id',$param2)->update('tbl_applications',$update_application);

	    			$applications_id = $param2;

	    			$insert_application_photo['application_id'] = $applications_id;

	    			if (!empty($_FILES["photo"]['name'])){

						//exta work
						$config_c['remove_spaces']  = TRUE;
						$config_c['upload_path']    = 'assets/applicationPhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|JPG|JPG|PNG|JPEG';

						$this->load->library('upload');

						$files = $_FILES;
						for($i=0; $i< count($files['photo']['name']); $i++)
						{           
							$_FILES['photo']['name']     = $files['photo']['name'][$i];
							$_FILES['photo']['type']     = $files['photo']['type'][$i];
							$_FILES['photo']['tmp_name'] = $files['photo']['tmp_name'][$i];
							$_FILES['photo']['error']    = $files['photo']['error'][$i];
							$_FILES['photo']['size']     = $files['photo']['size'][$i]; 

							$path_parts                 = pathinfo($_FILES['photo']['name']);
							$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
							$dir                        = date("YmdHis", time());
							$config_c['file_name']      = $newfile_name . '_' . $dir;


							$this->upload->initialize($config_c);
							if (!$this->upload->do_upload('photo')) {

							} else {

								$upload_c = $this->upload->data();
								$insert_application_photo['photo'] = $config_c['upload_path'] . $upload_c['file_name'];
								$this->image_size_fix($insert_application_photo['photo'], 400, 400);

								$application_photo = $this->db->insert('tbl_application_photos',$insert_application_photo);
							}    
						}

					}

					if ($application_photo) {

						$this->session->set_flashdata('message','Application Updated Successfully!');
						redirect('admin/application','refresh');

					} else {

						$this->session->set_flashdata('message','Application Update Failed!');
						redirect('admin/application','refresh');

					}
	    		}

    		}else{

    		}

	    	$data['application_type']  = $this->db->get('tbl_application_type')->result();
	    	$data['title']              = 'Application Update';
	    	$data['activeMenu']         = 'application';
	    	$data['page']               = 'backEnd/admin/applications_edit';

    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		$delete_application = $this->db->where('id',$param2)->delete('tbl_applications');

    		$photos = $this->db->where('application_id',$param2)->get('tbl_application_photos')->result();
    		foreach ($photos as $key => $photo_value) {

    			if (file_exists($photo_value->photo)) {

	    			unlink($photo_value->photo);
	    		}   			
    		}

    		$this->db->where('application_id',$param2)->delete('tbl_application_photos');
    		
    		if ($delete_application) {

				$this->session->set_flashdata('message','Application Deleted Successfully!');
				redirect('admin/application','refresh');

			} else {

				$this->session->set_flashdata('message','Application Delete Failed!');
				redirect('admin/application','refresh');

			}
    	}
    	$this->load->view('backEnd/master_page',$data);
    }

    public function download_page($param1 = '', $param2 = '', $param3 = '')
    {
    	if ($param1 == 'add') {

    		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    			$insert_download_page['title']          = $this->input->post('title', true);
    			$insert_download_page['priority']       = $this->input->post('priority', true);
    			$insert_download_page['insert_by']      = $_SESSION['userid'];
    			$insert_download_page['insert_time']    = date('Y-m-d H:i:s');
    			$insert_download_page['classification'] = $this->input->post('classification', true);

    			if (!empty($_FILES["attachment"]['name'])){

					//exta work
					$path_parts                 = pathinfo($_FILES["attachment"]['name']);
					$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
					$dir                        = date("YmdHis", time());
					$config_c['file_name']      = $newfile_name . '_' . $dir;
					$config_c['remove_spaces']  = TRUE;
					$config_c['upload_path']    = 'assets/downloadpagePhoto/';
					$config_c['max_size']       = '20000'; //  less than 20 MB
					$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|pdf|JPG|JPG|PNG|JPEG|PDF';

					$this->load->library('upload', $config_c);
					$this->upload->initialize($config_c);
					if (!$this->upload->do_upload('attachment')) {

					} else {

						$upload_c = $this->upload->data();
						$insert_download_page['attachment']   = $config_c['upload_path'] . $upload_c['file_name'];
						$this->image_size_fix($insert_download_page['attachment'], 400, 400);
					}
				}

				$add_download_page = $this->db->insert('tbl_download_page',$insert_download_page);

				if ($add_download_page) {

					$this->session->set_flashdata('message', "Download Page Added Successfully.");
					redirect('admin/download_page', 'refresh'); 

				} else {

					$this->session->set_flashdata('message', "Download Page Add Failed.");
					redirect('admin/download_page', 'refresh'); 
				}

    		}

    	}elseif ($param1 == 'edit' && $param2 > 0) {

    		$data['edit_info'] = $this->db->get_where('tbl_download_page',array('id'=>$param2));

    		if ($data['edit_info']->num_rows() > 0) {

    			$data['edit_info'] = $data['edit_info']->row();

    			if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	    			$update_download_page['title']          = $this->input->post('title', true);
	    			$update_download_page['priority']       = $this->input->post('priority', true);
	    			$update_download_page['classification'] = $this->input->post('classification', true);

	    			if (!empty($_FILES["attachment"]['name'])){

						//exta work
						$path_parts                 = pathinfo($_FILES["attachment"]['name']);
						$newfile_name               = preg_replace('/[^A-Za-z]/', "", $path_parts['filename']);
						$dir                        = date("YmdHis", time());
						$config_c['file_name']      = $newfile_name . '_' . $dir;
						$config_c['remove_spaces']  = TRUE;
						$config_c['upload_path']    = 'assets/downloadpagePhoto/';
						$config_c['max_size']       = '20000'; //  less than 20 MB
						$config_c['allowed_types']  = 'jpg|png|jpeg|jpg|pdf|JPG|JPG|PNG|JPEG|PDF';

						$this->load->library('upload', $config_c);
						$this->upload->initialize($config_c);
						if (!$this->upload->do_upload('attachment')) {

						} else {

							$upload_c = $this->upload->data();
							$update_download_page['attachment']   = $config_c['upload_path'] . $upload_c['file_name'];
							$this->image_size_fix($update_download_page['attachment'], 400, 400);
						}
					}

					if ($this->AdminModel->download_page_update($update_download_page, $param2)) {

						$this->session->set_flashdata('message', "Download Page Updated Successfully.");
						redirect('admin/download_page', 'refresh'); 

					} else {

						$this->session->set_flashdata('message', "Download Page Update Failed.");
						redirect('admin/download_page', 'refresh'); 
					}

	    		}

    		} else {

    			$this->session->set_flashdata('message', "Wrong Attempt.");
				redirect('admin/download_page', 'refresh');
    		}
    		

    	}elseif ($param1 == 'delete' && $param2 > 0) {

    		if ($this->AdminModel->download_page_delete($param2)) {

				$this->session->set_flashdata('message', "Download Page Updated Successfully.");
				redirect('admin/download_page', 'refresh'); 

			} else {

				$this->session->set_flashdata('message', "Download Page Update Failed.");
				redirect('admin/download_page', 'refresh'); 
			}

    	}


    	$data['title']          = 'Download Page';
    	$data['activeMenu']     = 'download_page';
    	$data['page']           = 'backEnd/admin/download_page';
    	$data['download_pages'] = $this->db->order_by('id','desc')->get('tbl_download_page')->result();
    	$this->load->view('backEnd/master_page',$data);
    }


    public function login_registration(){
    	$data = array();
    	$data['page'] = 'backEnd/admin/login';
    	$this->load->view('backEnd/master_page', $data);
	}

	# ============Circular Section Start==============
	
	public function circular_list()
	{
		$data['title']          = 'Circular List';
    	$data['activeMenu']     = 'circular_list';
    	$data['page']           = 'backEnd/admin/circular_list';
    	$data['results'] = $this->db->where('status',1)->order_by('id','desc')->get('tbl_circular')->result();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function circular_add()
	{
		$data['title']          = 'Circular Add';
    	$data['activeMenu']     = 'add_circular';
    	$data['page']           = 'backEnd/admin/circular_add';
    	$this->load->view('backEnd/master_page',$data);
	}

	public function circular_create()
	{
		$data = array();
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/circular/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/circular/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/circular/' . $target_path;
			}
			
		}

		$save = $this->db->insert('tbl_circular',$data);
		if($save)
			$this->session->set_flashdata('message', "Data Add Successfully.");
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function circular_edit($id)
	{
		$data['title']          = 'Circular Update';
    	$data['activeMenu']     = 'edit_circular';
		$data['page']           = 'backEnd/admin/circular_edit';
		$data['results'] = $this->db->query("select * from tbl_circular where id=$id")->result_array();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function circular_update()
	{
		$data = array();
		$id = $this->input->post('id');
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/circular/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/circular/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/circular/' . $target_path;
				#get old images and unlink 
				$get_files = $this->db->query("select * from tbl_circular where id=$id")->result_array();
					
				$remove_file = $get_files[0]['file'];
				
				if($remove_file)
					unlink($remove_file);
			}
			
		}

		$update = $this->db->where('id',$id)->update('tbl_circular',$data);

		if($update)
			$this->session->set_flashdata('message', "Data update Successfully.");
		
		redirect('admin/circular_list');
	}

	public function circular_delete($id)
	{	
		#get old images and unlink 
		$files = $this->db->query("select * from tbl_circular where id=$id")->result_array();
			
		$file = $files[0]['file'];
		
		if($file)
			unlink($file);

    	$soft_delt = $this->db->query("update tbl_circular set status=0 where id=$id");
		if($soft_delt)
			$this->session->set_flashdata('message', "Data Remove Successfully.");

			
		redirect($_SERVER['HTTP_REFERER']);
	}

	# ============Federation Activities Section Start==============

	public function federation_list()
	{
		$data['title']          = 'Federation Activities List';
    	$data['activeMenu']     = 'federation_list';
    	$data['page']           = 'backEnd/admin/federation_list';
    	$data['results'] = $this->db->where('status',1)->order_by('id','desc')->get('tbl_federation')->result();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function federation_add()
	{
		$data['title']          = 'Federation Add';
    	$data['activeMenu']     = 'federation_add';
    	$data['page']           = 'backEnd/admin/federation_add';
    	$this->load->view('backEnd/master_page',$data);
	}

	public function federation_create()
	{
		$data = array();
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/federation/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/federation/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/federation/' . $target_path;
			}
			
		}

		$save = $this->db->insert('tbl_federation',$data);
		if($save)
			$this->session->set_flashdata('message', "Data Add Successfully.");
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function federation_edit($id)
	{
		$data['title']          = 'Federation Update';
    	$data['activeMenu']     = 'federation_edit';
		$data['page']           = 'backEnd/admin/federation_edit';
		$data['results'] = $this->db->query("select * from tbl_federation where id=$id")->result_array();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function federation_update()
	{
		$data = array();
		$id = $this->input->post('id');
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/federation/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/federation/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/federation/' . $target_path;
				#get old images and unlink 
				$get_files = $this->db->query("select * from tbl_federation where id=$id")->result_array();
					
				$remove_file = $get_files[0]['file'];
				
				if($remove_file)
					unlink($remove_file);
			}
			
		}

		$update = $this->db->where('id',$id)->update('tbl_federation',$data);

		if($update)
			$this->session->set_flashdata('message', "Data update Successfully.");
		
		redirect('admin/federation_list');
	}

	public function federation_delete($id)
	{	
		#get old images and unlink 
		$files = $this->db->query("select * from tbl_federation where id=$id")->result_array();
			
		$file = $files[0]['file'];
		
		if($file)
			unlink($file);

    	$soft_delt = $this->db->query("update tbl_federation set status=0 where id=$id");
		if($soft_delt)
			$this->session->set_flashdata('message', "Data Remove Successfully.");

			
		redirect($_SERVER['HTTP_REFERER']);
	}

	
	# ============Record Holder Section Start==============

	public function record_list()
	{
		$data['title']          = 'Record List';
    	$data['activeMenu']     = 'record_list';
    	$data['page']           = 'backEnd/admin/record_list';
    	$data['results'] = $this->db->where('status',1)->order_by('id','desc')->get('tbl_record')->result();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function record_add()
	{
		$data['title']          = 'Record Add';
    	$data['activeMenu']     = 'record_add';
    	$data['page']           = 'backEnd/admin/record_add';
    	$this->load->view('backEnd/master_page',$data);
	}

	public function record_create()
	{
		$data = array();
		$data['title'] = $title = $this->input->post('title');
		$data['name'] = $name = $this->input->post('name');
		$data['date'] = $date = Date('Y-m-d',strtotime($this->input->post('date')));

		

		$save = $this->db->insert('tbl_record',$data);
		if($save)
			$this->session->set_flashdata('message', "Data Add Successfully.");
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function record_edit($id)
	{
		$data['title']          = 'Record Update';
    	$data['activeMenu']     = 'record_edit';
		$data['page']           = 'backEnd/admin/record_edit';
		$data['results'] = $this->db->query("select * from tbl_record where id=$id")->result_array();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function record_update()
	{
		$data = array();
		$id = $this->input->post('id');
		$data['title'] = $title = $this->input->post('title');
		$data['name'] = $name = $this->input->post('name');
		$data['date'] = $date = Date('Y-m-d',strtotime($this->input->post('date')));


		$update = $this->db->where('id',$id)->update('tbl_record',$data);

		if($update)
			$this->session->set_flashdata('message', "Data update Successfully.");
		
		redirect('admin/record_list');
	}

	public function record_delete($id)
	{	

    	$soft_delt = $this->db->query("update tbl_record set status=0 where id=$id");
		if($soft_delt)
			$this->session->set_flashdata('message', "Data Remove Successfully.");

			
		redirect($_SERVER['HTTP_REFERER']);
	}

	# ============Rules and Regulations Section Start==============

	public function rules_list()
	{
		$data['title']          = 'Rules and Regulations List';
    	$data['activeMenu']     = 'rules_list';
    	$data['page']           = 'backEnd/admin/rules_list';
    	$data['results'] = $this->db->where('status',1)->order_by('id','desc')->get('tbl_rules')->result();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function rules_add()
	{
		$data['title']          = 'Rules Add';
    	$data['activeMenu']     = 'rules_add';
    	$data['page']           = 'backEnd/admin/rules_add';
    	$this->load->view('backEnd/master_page',$data);
	}

	public function rules_create()
	{
		$data = array();
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/rules/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/rules/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/rules/' . $target_path;
			}
			
		}

		$save = $this->db->insert('tbl_rules',$data);
		if($save)
			$this->session->set_flashdata('message', "Data Add Successfully.");
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function rules_edit($id)
	{
		$data['title']          = 'Rules and Regulations Update';
    	$data['activeMenu']     = 'rules_edit';
		$data['page']           = 'backEnd/admin/rules_edit';
		$data['results'] = $this->db->query("select * from tbl_rules where id=$id")->result_array();
    	$this->load->view('backEnd/master_page',$data);
	}

	public function rules_update()
	{
		$data = array();
		$id = $this->input->post('id');
		$data['title'] = $title = $this->input->post('title');

		$config["upload_path"] = base_url().'/assets/rules/';
		$config["allowed_types"] = 'pdf|jpg|png|jpeg';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if($_FILES["files"]["name"] != '')
		{	
			
			$_FILES["file"]["name"] = $_FILES["files"]["name"];
			$_FILES["file"]["type"] = $type = $_FILES["files"]["type"];
			$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"];
			$_FILES["file"]["error"] = $_FILES["files"]["error"];
			$_FILES["file"]["size"] = $size = $_FILES["files"]["size"];

			$i_ext = explode('.', $_FILES['file']['name']);

			$target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
			if (move_uploaded_file($_FILES['file']['tmp_name'], 'assets/rules/' . $target_path))
			{
				$data['file'] = $pdf = 'assets/rules/' . $target_path;
				#get old images and unlink 
				$get_files = $this->db->query("select * from tbl_rules where id=$id")->result_array();
					
				$remove_file = $get_files[0]['file'];
				
				if($remove_file)
					unlink($remove_file);
			}
			
		}

		$update = $this->db->where('id',$id)->update('tbl_rules',$data);

		if($update)
			$this->session->set_flashdata('message', "Data update Successfully.");
		
		redirect('admin/rules_list');
	}

	public function rules_delete($id)
	{	
		#get old images and unlink 
		$files = $this->db->query("select * from tbl_rules where id=$id")->result_array();
			
		$file = $files[0]['file'];
		
		if($file)
			unlink($file);

    	$soft_delt = $this->db->query("update tbl_rules set status=0 where id=$id");
		if($soft_delt)
			$this->session->set_flashdata('message', "Data Remove Successfully.");

			
		redirect($_SERVER['HTTP_REFERER']);
	}

	


}

?>
