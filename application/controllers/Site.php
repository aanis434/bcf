<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Site extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('download'); 
        $this->load->model('FrontEndModel'); 
        $this->load->helper('text');
        $this->load->helper('form');
    }

    public function index(){
        $data = array();
        $data['silde_images']    =  $this->FrontEndModel->get_sliders();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_events']    =  $this->FrontEndModel->get_all_events();
        $data['get_all_photos']    =  $this->db->limit(4)->get('tbl_photos')->result();
        $data['get_all_videos']    =  $this->FrontEndModel->get_all_videos();
        $data['get_all_team']    =  $this->FrontEndModel->get_all_team();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['about_content']   =  $this->FrontEndModel->get_common_pages($name = 'about');

        $data['all_events'] = $this->db->query("select * from tbl_upcoming_events  order by id desc limit 10")->result_array();
        $data['road'] = $this->db->query("select * from tbl_upcoming_events where type='road'")->result_array();
        $data['track'] = $this->db->query("select * from tbl_upcoming_events where type='track'")->result_array();
        $data['indoor'] = $this->db->query("select * from tbl_upcoming_events where type='indoor-cycling'")->result_array();
        $data['para'] = $this->db->query("select * from tbl_upcoming_events where type='para-cycling'")->result_array();

        $data['contents'] = 'frontend/index';
        $this->load->view('frontend/welcome', $data);
    }
    public function welcome_message(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['welcome_message']   =  $this->FrontEndModel->get_common_pages($name = 'welcome');
        $data['contents'] = 'frontend/welcome-message';
        $this->load->view('frontend/welcome', $data);
    }

     public function history(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['history']   =  $this->FrontEndModel->get_common_pages($name = 'history');
        $data['contents'] = 'frontend/history';
        $this->load->view('frontend/welcome', $data);
    }

     public function profile(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['profile']   =  $this->FrontEndModel->get_common_pages($name = 'profile');
        $data['contents'] = 'frontend/profile';
        $this->load->view('frontend/welcome', $data);
    }

     public function mission_statement(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['mission_statement']   =  $this->FrontEndModel->get_common_pages($name = 'mission');
        $data['contents'] = 'frontend/mission-statement';
        $this->load->view('frontend/welcome', $data);
    }

     public function management_committee(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['get_all_team']    =  $this->db->query("select * from tbl_team where member_type=2")->result();
        $data['contents'] = 'frontend/management-committee';
        $this->load->view('frontend/welcome', $data);
    }
    public function circular(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['circular']   =  $this->db->query("select * from tbl_circular where status=1")->result_array();
        $data['contents'] = 'frontend/circular';
        $this->load->view('frontend/welcome', $data);
    }
    public function rules(){
        $data = array();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['results'] = $this->db->query("select * from tbl_rules where status=1")->result_array();
        $data['contents'] = 'frontend/rules'; 
        $this->load->view('frontend/welcome', $data);
    }

    public function record()
    {
        $data = array();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['results'] = $this->db->query("select * from tbl_record where status=1")->result_array();
        $data['contents'] = 'frontend/record'; 
        $this->load->view('frontend/welcome', $data);
    }

    public function news(){
        $data = array();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['contents'] = 'frontend/description';
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $data['contents'] = 'frontend/news';
        $this->load->view('frontend/welcome', $data);
    }

    public function description($id = ''){
        $data = array();
        $data['get_single_description'] = $this->FrontEndModel->get_single_description($id = $id);
        $data['contents'] = 'frontend/description';
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['get_latest_news']    =  $this->FrontEndModel->get_latest_news();
        $this->load->view('frontend/welcome', $data);
    }

    public function federation_activities(){
        $data = array();        
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['results'] = $this->db->query("select * from tbl_federation where status=1")->result_array();
        $data['contents'] = 'frontend/federation-activities';
        $this->load->view('frontend/welcome', $data);
    }

    public function all_events(){
        $data = array();
        $data['get_all_events']    =  $this->FrontEndModel->get_all_events();
        $data['contents'] = 'frontend/all-events';
        $this->load->view('frontend/welcome', $data);
    }

     public function upcoming_event($param1 = 'road'){
        
        if($param1 == "road"){
            $data = array();
            $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
            $data['result'] = $this->db->query("select * from tbl_upcoming_events where type='road'")->result_array();
           $data['contents'] = 'frontend/road'; 
        }
        else if($param1 == "track"){
            $data = array();
            $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
            $data['result'] = $this->db->query("select * from tbl_upcoming_events where type='track'")->result_array();
           $data['contents'] = 'frontend/track'; 
        }
        else if($param1 == "indoor-cycling"){
            $data = array();
            $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
            $data['result'] = $this->db->query("select * from tbl_upcoming_events where type='indoor-cycling'")->result_array();
           $data['contents'] = 'frontend/indoor-cycling'; 
        }
        else if($param1 == "para-cycling"){
            $data = array();
            $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
            $data['result'] = $this->db->query("select * from tbl_upcoming_events where type='para-cycling'")->result_array();
           $data['contents'] = 'frontend/para-cycling'; 
        }
        
        $this->load->view('frontend/welcome', $data);
    }

    public function event_description($id = ''){
        $data = array();
        $data['get_single_event_description'] = $this->FrontEndModel->get_single_event_description($id = $id);
        $data['contents'] = 'frontend/event-description';
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['get_all_events']    =  $this->FrontEndModel->get_all_events();
        $this->load->view('frontend/welcome', $data);
    }

    public function gallery(){
         $data = array();
         $data['get_all_photos']    =  $this->FrontEndModel->get_all_photos();
        $data['contents'] = 'frontend/gallery'; 
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $this->load->view('frontend/welcome', $data);
    }
    public function board_honour(){
         $data = array();
         $data['get_all_photos']    =  $this->FrontEndModel->get_all_photos();
         $data['get_all_team']    =  $this->db->query("select * from tbl_team where member_type=1")->result();
        $data['get_all_partner']    =  $this->FrontEndModel->get_all_partner();
        $data['contents'] = 'frontend/board_honour'; 
        $this->load->view('frontend/welcome', $data);
    }
    public function contact_us(){
        $data = array();
        $data['contents'] = 'frontend/contact-us'; 
        $data['get_contact_info']    =  $this->FrontEndModel->get_contact_info();
        $data['get_follow_us']    =  $this->FrontEndModel->get_follow_us();
        // print_r($data['get_contact_info']);exit;$this->session->set_flashdata('message','Contact Update Failed!');
        $this->load->view('frontend/welcome', $data);
    }

    public function create_message()
    {
        $data['name'] = $name = $this->input->post('name');
        $data['email'] = $email =$this->input->post('email');
        $data['phone'] = $phone =$this->input->post('phone');
        $data['message_body'] = $message =$this->input->post('message');
        $data['subject'] = $subject =$this->input->post('subject');

        if(!empty($name) && !empty($phone) && !empty($message))
        {
            $create_message_add = $this->db->insert('tbl_contact_manage', $data);
            if($create_message_add)
                $this->session->set_flashdata('message','Thanks For Your Massage!');
        }
        redirect($_SERVER['HTTP_REFERER'],'refresh');
    }



    
}