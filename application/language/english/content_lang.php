<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['main_navigation'] = 'MAIN NAVIGATION';

//common part

$lang['sl']          = 'SL';
$lang['name']        = 'Name';
$lang['body']        = 'Body';
$lang['title']       = 'Title';
$lang['photo']       = 'Photo';
$lang['subtitle']    = 'Subtitle';
$lang['priority']    = 'Priority';
$lang['cancel']      = 'Cancel';
$lang['reset']       = 'Reset';
$lang['save']        = 'Save';
$lang['image']       = 'Image';
$lang['email']       = 'Email';
$lang['action']      = 'Action';
$lang['delete']      = 'Delete';
$lang['update']      = 'Update';
$lang['submit']      = 'Submit';
$lang['description'] = 'Description';

// dashboard part

$lang['dashboard']     = 'Dashboard';



// user part

$lang['manage_user']           = 'Manage User';
$lang['add_user']              = 'Add User';
$lang['user_list']             = 'User List';
$lang['update_user']           = 'Update User';
$lang['first_name']            = 'First Name';
$lang['last_name']             = 'Last Name';
$lang['user_name']             = 'User Name';
$lang['email']                 = 'Email';
$lang['phone']                 = 'Phone';
$lang['password']              = 'Password';
$lang['division']              = 'Division';
$lang['district']              = 'District';
$lang['upozilla']              = 'Upozilla';
$lang['select_a_division']     = 'Select A Division';
$lang['select_division_first'] = 'Select Division First';
$lang['select_district_first'] = 'Select District First';
$lang['road_house']            = 'Road/House';
$lang['user_type']             = 'User Type';
$lang['user']                  = 'User';
$lang['user_photo']            = 'User Photo';
$lang['user_photo_caption']    = 'Photo will be uploaded by User';
$lang['suspend']               = 'Suspend';

//profile part

$lang['profile']         = 'Profile';
$lang['user_info']       = 'User Info';
$lang['password_change'] = 'Password Change';
$lang['old_password']    = 'Old Password';
$lang['new_password']    = 'New Password';
$lang['sign_out']        = 'Sign Out';
$lang['mail']            = 'Mail';
$lang['web_mail']        = 'Webmail';
$lang['facebook']        = 'Facebook';

//general part

$lang['add_general']       = 'Add General';
$lang['update_general']    = 'Update General';
$lang['general_list']      = 'General List';
$lang['value']             = 'Value';
$lang['general_setting']   = 'General Setting';


// slider part

$lang['slider']       = 'Slider';
$lang['add_slider']   = 'Add Slider';
$lang['first_title']  = 'First Title';
$lang['second_title'] = 'Second Title';
$lang['slider_photo'] = 'Slider Photo';
$lang['add']          = 'Add';
$lang['new_photo']    = 'New Photo';

//news part
$lang['news'] = "News";
$lang['add_news'] = "Add News";
$lang['news_list'] = "News List";
$lang['edit_news'] = "Edit News";

//events part
$lang['events'] = "Events";
$lang['add_events'] = "Add Events";
$lang['events_list'] = "Events List";
$lang['edit_events'] = "Edit Events";

//photo part
$lang['photo'] = "Photo";
$lang['add_photo'] = "Add Photo";
$lang['photo_list'] = "Photo List";
$lang['edit_photo'] = "Edit Photo";

//videos part
$lang['videos'] = "Videos";
$lang['add_videos'] = "Add Videos";
$lang['videos_list'] = "Videos List";
$lang['edit_videos'] = "Edit Videos";

//calender page

$lang['calendar'] = "Calendar";


//Team part
$lang["team"] = 'Team';
$lang['add_team'] = "Add Team";
$lang['team_list'] = "Team List";
$lang['edit_team'] = "Edit Team";

//partner part
$lang["partner"] = 'Partner';
$lang['add_partner'] = "Add Partner";
$lang['partner_list'] = "Partner List";
$lang['edit_partner'] = "Edit Partner";

$lang['follow_us'] = 'Follow Us';
$lang['add_follow'] = 'Add Follow';
$lang['follow_list'] = 'Follow List';
$lang['edit_follow'] = 'Edit Follow';

//contact manage part

$lang['manage_contact_page']   = 'View Query';
$lang['update_contact_manage'] = 'Update Query';
$lang['contact_manage_list']   = 'Query List';
$lang['subject']               = 'Subject';
$lang['message_body']          = 'Message Body';
$lang['message']               = 'Message';

// common page

$lang['body']               = 'Body';
$lang['old_file']           = 'Old File';
$lang['common_page']        = 'Common Page';
$lang['page_settings']      = 'Page Settings';
$lang['page_settings_list'] = 'Page Settings List';

// contact page

$lang['branch_name']        = 'Branch';
$lang['tel']                = 'Tel';
$lang['main']               = 'Main';
$lang['agent']              = 'Agent';
$lang['contact_person']     = 'Contact Person';
$lang['company_agent_name'] = 'Company Agent Name';
$lang['agent_name']         = 'Agent Name';
$lang['address']            = 'Address';
$lang['contact_list']       = 'Contact List';
$lang['contact_add']        = 'Contact Add';
$lang['contact_update']     = 'Contact Update';
$lang['contact_manage']     = 'Contact Manage';

// certificate

$lang['certificate'] = 'Certificate';
$lang['year']        = 'Year';


// photo

$lang['photo_list']    = 'Photo List';
$lang['caption']       = 'Caption';
$lang['photo_gallery'] = 'Photo Gallery';
$lang['photo']         = 'Photo';

// photo gallery

$lang['photo_gallery_add']  = 'Photo Gallery Add';
$lang['photo_gallery_list'] = 'Photo Gallery List';

// machines

$lang['machines']           = 'Machines';
$lang['machines_type_add']  = 'Machines Type Add';
$lang['machines_type_list'] = 'Machines Type List';
$lang['machines_type']      = 'Machines Type';
$lang['machines_update']    = 'Machines Update';

//applications

$lang['application']           = 'Application';
$lang['application_type_add']  = 'Application Type Add';
$lang['application_type_list'] = 'Application Type List';
$lang['application_type']      = 'Application Type';
$lang['application_update']    = 'Application Update';

//download_page

$lang['download_page']  = 'Download Page';
$lang['classification'] = 'Classification';
$lang['attachment']     = 'Attachment';
