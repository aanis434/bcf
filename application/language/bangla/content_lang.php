<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['main_navigation'] = 'MAIN NAVIGATION';

//common part

$lang['sl']            = 'ক্র';
$lang['name']          = 'নাম';
$lang['title']         = 'শিরোনাম';
$lang['photo']         = 'ছবি';
$lang['subtitle']      = 'সাবটাইটেল';
$lang['priority']      = 'অগ্রাধিকার';
$lang['cancel']        = 'বাতিল';
$lang['reset']         = 'রিসেট';
$lang['save']          = 'সেভ';
$lang['image']         = 'ছবি';
$lang['action']        = 'একশান';
$lang['delete']        = 'ডিলিট';
$lang['update']        = 'আপডেট';
$lang['submit']        = 'সাবমিট';
$lang['description']   = 'বিবরণ';

// dashboard part

$lang['dashboard']     = 'ড্যাশবোর্ড';

// user part

$lang['manage_user']            = 'ম্যানেজ ইউজার';
$lang['add_user']               = 'ইউজার যোগ';
$lang['user_list']              = 'ইউজার তালিকা';
$lang['first_name']             = 'নামের প্রথম অংশ';
$lang['last_name']              = 'নামের শেষাংশ';
$lang['user_name']              = 'ইউজারের নাম';
$lang['email']                  = 'ইমেইল'; 
$lang['phone']                  = 'ফোন';
$lang['password']               = 'পাসওয়ার্ড';
$lang['division']               = 'বিভাগ';
$lang['district']               = 'জেলা';
$lang['upozilla']               = 'উপজেলা'; 
$lang['select_a_division']      = 'বিভাগ নির্বাচন করুন';
$lang['select_division_first']  = 'জেলা নির্বাচন করুন';
$lang['select_district_first']  = 'উপজেলা নির্বাচন করুন';
$lang['road_house']             = 'রাস্তা/বাসা';
$lang['user_type']              = 'ইউজার টাইপ';
$lang['user']                   = 'ইউজার';
$lang['user_photo']             = 'ইউজারের ছবি';
$lang['user_photo_caption']     = 'ইউজার ছবি আপলোড করবে';
$lang['suspend']                = 'সাস্পেন্ড';

//profile part

$lang['profile']         = 'প্রোফাইল';
$lang['user_info']       = 'ইউজারের তথ্য';
$lang['password_change'] = 'পাসওয়ার্ড পরিবর্তন';
$lang['old_password']    = 'পুরাতন পাসওয়ার্ড';
$lang['new_password']    = 'নতুন পাসওয়ার্ড';
$lang['sing_out']        = 'সাইন আউট';
$lang['mail']            = 'মেইল';
$lang['web_mail']        = 'ওয়েব মেইল';
$lang['facebook']        = 'ফেসবুক';

//general part

$lang['add_general']       = 'জেনারেল সেটিং যোগ';
$lang['update_general']    = 'জেনারেল আপডেট';
$lang['general_list']      = 'জেনারেল তালিকা';
$lang['value']             = 'ভ্যালু';
$lang['general_setting']   = 'জেনারেল সেটিং';

// slider part

$lang['slider']       = 'স্লাইডার'; 
$lang['add_slider']   = 'স্লাইডার যোগ';
$lang['first_title']  = 'প্রথম শিরোনাম';
$lang['second_title'] = 'দ্বিতীয় শিরোনাম';
$lang['slider_photo'] = 'স্লাইডার ফটো';
$lang['add']          = 'যোগ';
$lang['photo']        = 'ছবি';
$lang['sl']           = 'ক্র';
$lang['new_photo']    = 'নতুন ছবি';

//contact manage part

$lang['manage_contact_page']   = 'ভিউ কোয়েরি';
$lang['update_contact_manage'] = 'আপডেট কোয়েরি';
$lang['contact_manage_list']   = 'কোয়েরি তালিকা';
$lang['subject']               = 'সাবজেক্ট';
$lang['message_body']          = 'বার্তাংশ';
$lang['message']               = 'বার্তা';

// common page

$lang['body']               = 'বডি';
$lang['old_file']           = 'ওল্ড ফাইল';
$lang['common_page']        = 'কমন পেজ';
$lang['page_settings']      = 'পেজ সেটিংস';
$lang['page_settings_list'] = 'পেজ সেটিংস তালিকা';

// contact page

$lang['branch_name']        = 'শাখা';
$lang['tel']                = 'টেল';
$lang['main']               = 'প্রধান';
$lang['agent']              = 'এজেন্ট';
$lang['contact_person']     = 'যোগাযোগের ব্যক্তি';
$lang['company_agent_name'] = 'কোম্পানির এজেন্ট নাম';
$lang['agent_name']         = 'এজেন্ট নাম';
$lang['address']            = 'ঠিকানা';
$lang['contact_list']       = 'যোগাযোগ তালিকা';
$lang['contact_add']        = 'যোগাযোগ যোগ';
$lang['contact_update']     = 'যোগাযোগ আপডেট';
$lang['contact_manage']     = 'যোগাযোগ ম্যানেজ';

// certificate

$lang['certificate'] = 'প্রশংসাপত্র';
$lang['year']        = 'বছর';

//videos 

$lang['videos']       = 'ভিডিও';
$lang['video_link']   = 'ভিডিও লিংক';
$lang['video_list']   = 'ভিডিও তালিকা';
$lang['video_add']    = 'ভিডিও যোগ';
$lang['video_manage'] = 'ভিডিও ম্যানেজ';

// photo

$lang['photo_list']    = 'ছবি তালিকা';
$lang['caption']       = 'ক্যাপশন';
$lang['photo_gallery'] = 'ছবি গ্যালারি';
$lang['photo']         = 'ছবি';

// photo gallery

$lang['photo_gallery_add']  = 'ছবি গ্যালারি যোগ';
$lang['photo_gallery_list'] = 'ছবি গ্যালারি তালিকা';

// machines

$lang['machines']           = 'মেশিন';
$lang['machines_type_add']  = 'মেশিন টাইপ যোগ';
$lang['machines_type_list'] = 'মেশিন টাইপ তালিকা';
$lang['machines_type']      = 'মেশিন টাইপ';
$lang['machines_update']    = 'মেশিন আপডেট';

//applications

$lang['application']           = 'অ্যাপ্লিকেশন';
$lang['application_type_add']  = 'অ্যাপ্লিকেশন টাইপ যোগ';
$lang['application_type_list'] = 'অ্যাপ্লিকেশন টাইপ তালিকা';
$lang['application_type']      = 'অ্যাপ্লিকেশন টাইপ';
$lang['application_update']    = 'অ্যাপ্লিকেশন আপডেট';

//download_page

$lang['download_page']  = 'ডাউনলোড পেজ';
$lang['classification'] = 'ক্লাসিফিকেশন';
$lang['attachment']     = 'সংযুক্তি';

