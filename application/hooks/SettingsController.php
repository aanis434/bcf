<?php

class SettingsController {

    function ConfigSettings() {

       $CI =& get_instance();
  //   $this->load->model('SiteConfig');
      $data = $CI->SiteConfig->get_all();
      $follow_us =$CI->SiteConfig->get_follow_us()->result_array();
      $latest_news =$CI->SiteConfig->get_latest_news()->result_array();
      //echo $site_config;

      $CI->config->set_item('latest_news', $latest_news);
        
      $CI->config->set_item('address', $data[0]['address']);
      $CI->config->set_item('email', $data[0]['email']);
      $CI->config->set_item('w_url', $data[0]['w_url']);
      $CI->config->set_item('detail', $data[0]['detail']);


      $CI->config->set_item('fb_url', $follow_us[0]['fb_url']);
      $CI->config->set_item('tw_url', $follow_us[0]['tw_url']);
      $CI->config->set_item('ln_url', $follow_us[0]['ln_url']);
      $CI->config->set_item('yt_url', $follow_us[0]['yt_url']);
      $CI->config->set_item('pi_url', $follow_us[0]['pi_url']);
      $CI->config->set_item('ins_url', $follow_us[0]['ins_url']);
       
    }

}
