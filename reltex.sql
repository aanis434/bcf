-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2019 at 10:19 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reltex`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_status`
--

CREATE TABLE `log_status` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `logInOut` int(11) NOT NULL,
  `ipAddress` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_recovery`
--

CREATE TABLE `password_recovery` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `newPassword` text CHARACTER SET latin1 NOT NULL,
  `oldPassword` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='password will recover using sms only through verified phone number.' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applications`
--

CREATE TABLE `tbl_applications` (
  `id` int(11) NOT NULL,
  `application_type_id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin,
  `description` text COLLATE utf8_bin,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_applications`
--

INSERT INTO `tbl_applications` (`id`, `application_type_id`, `name`, `description`, `insert_time`, `insert_by`) VALUES
(4, 3, 'Sport & leisurewear', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 12:38:14', 1),
(5, 2, 'Home furnishings', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 12:38:26', 1),
(6, 2, 'Underwear', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 12:59:25', 1),
(7, 2, 'Leggings', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 13:02:16', 1),
(8, 2, 'Automotive Fabrics', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 13:04:14', 1),
(9, 2, 'Medical healthcare', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 13:07:31', 1),
(10, 2, 'Industrial', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 13:10:19', 1),
(11, 2, 'Footwear', 'We provide sporting goods & sportswear to Youth Clubs and stand alone Amateur Teams & Proffessional organisations all at discounted prices', '2019-04-07 13:11:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_application_photos`
--

CREATE TABLE `tbl_application_photos` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `photo` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_application_photos`
--

INSERT INTO `tbl_application_photos` (`id`, `application_id`, `photo`) VALUES
(1, 1, 'assets/applicationPhoto/EdibuQwwrMjavgjpgxa_20190407123323.jpg'),
(3, 3, 'assets/applicationPhoto/JetESTSPRGCdYhHgjpgxa_20190407123701.jpg'),
(4, 4, 'assets/applicationPhoto/EdibuQwwrMjavgjpgxa_20190407123814.jpg'),
(5, 5, 'assets/applicationPhoto/JetESTSPRGCdYhHgjpgxa_20190407123826.jpg'),
(6, 6, 'assets/applicationPhoto/OHtRoXrRtipSNmlmSclwjpgxa_20190407125925.jpg'),
(7, 7, 'assets/applicationPhoto/VPykvPbsQRuthSAFgUegjpgxa_20190407130216.jpg'),
(8, 8, 'assets/applicationPhoto/nbrUeTJFzsnEiiyAjpgxa_20190407130414.jpg'),
(9, 9, 'assets/applicationPhoto/GoMjNTCRRiytjwaTnAjpgxa_20190407130731.jpg'),
(10, 10, 'assets/applicationPhoto/ldYSDGCRXGbUbpQjpgxa_20190407131019.jpg'),
(11, 11, 'assets/applicationPhoto/mRtrVHRASjumWJcYnkPiNQjpgxa_20190407131138.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_application_type`
--

CREATE TABLE `tbl_application_type` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_application_type`
--

INSERT INTO `tbl_application_type` (`id`, `name`, `insert_time`, `insert_by`) VALUES
(3, 'Sport & Leisurewear', '0000-00-00 00:00', 0),
(4, 'Home furnishings', '0000-00-00 00:00', 0),
(5, 'Underwear', '0000-00-00 00:00', 0),
(6, 'Leggings', '0000-00-00 00:00', 0),
(7, 'Automotive Fabrics', '0000-00-00 00:00', 0),
(8, 'Medical & healthcare', '0000-00-00 00:00', 0),
(9, 'Industrial', '0000-00-00 00:00', 0),
(10, 'Footwear', '0000-00-00 00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_certificate`
--

CREATE TABLE `tbl_certificate` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `year` varchar(4) COLLATE utf8_bin NOT NULL DEFAULT '0000',
  `photo` text COLLATE utf8_bin NOT NULL,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_certificate`
--

INSERT INTO `tbl_certificate` (`id`, `name`, `year`, `photo`, `insert_time`, `insert_by`) VALUES
(1, 'Shrabon Ahmed', '2011', 'assets/certificatePhoto/personstandingpinksilhouettep_20190401153133.jpg', '2019-04-01 15:16:50', 1),
(3, 'Md. Foisal Ahmed', '2014', 'assets/certificatePhoto/cloudsdaylightforest_20190401153413.jpg', '2019-04-01 15:34:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_common_pages`
--

CREATE TABLE `tbl_common_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `title` text NOT NULL,
  `body` text NOT NULL,
  `attatched` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_common_pages`
--

INSERT INTO `tbl_common_pages` (`id`, `name`, `title`, `body`, `attatched`) VALUES
(1, 'about', 'ABOUT REL-TEX', '<p><span style=\"font-size:16px\"><strong>Quanzhou LuoJiang YouFeng(HongShuo) Precision Machinery Co., Ltd.</strong></span><br />\r\nAs an experienced Chinese manufacturer, HONGSHUO designs and assembles all kinds of circular knitting machine. Also available is the recently introduced seamless underwear knitting machine and tights knitting machine.Premium spare parts can be provided for all catalogued products.<br />\r\nFounded in 1996, our company is now located within the picturesque Luojiang District of Quanzhou city. We have imported advanced production equipment from countries such as Japan and Germany. At our disposal is the CNC vertical lathe, high-speed milling machine, CNC machining center, and other types of sophisticated machinery. Supported by sophisticated software such as CAD and CAM, we are now able to assemble up to 2000 machines each year. Through computerized rendering, we can fully analyze each component of our machines.</p>\r\n', 'assets/page_settings/aboutrightimg_20190407115428.jpg'),
(2, 'development', 'Development History', '<p><strong>1996</strong>—Hongshuo Machinery Co.,Ltd. was founded in Quanzhou city, the starting point of Chinese maritime silk road.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>\r\n 					<p><strong>1997-2004</strong>—Our company enjoyed a rapid development, gathered rich experience in the design and manufacture of knitting machines, and turned into a professional manufacturer.</p><br>', ''),
(3, 'certificate', 'Certificates', '', 'assets/page_settings/mWLaFUNQWOebNrtZxg_20190408144728.jpg'),
(4, 'market', 'Our market', '<p><strong>1. Domestic Market</strong></p>\r\n\r\n<p>Now we have formed an efficient sales service network in large and medium size cities as well as coastal areas of China, including Beijing, Tianjin, Chongqing, Liaoning, Hebei, Zhejiang, Shandong, Henan, Jiangsu, Fujian, Jiangxi, Guangdong, etc.</p>\r\n\r\n<p><strong>2. International Market</strong></p>\r\n\r\n<p>Our products are mainly exported to the Middle East, India, Bangladesh, Egypt, Iran, Syria, Pakistan, Turkey, South America, Brazil, Argentina, Chile, Ecuador, Mexico and other countries and regions. We have signed related agreement with the agents in India, Bangladesh, Pakistan, Egypt, Iran, Syria, Uzbekistan etc.Kindly contact us if you are interested in becoming our agent.</p>\r\n', 'assets/page_settings/NvrucPgShacoeSxxEDA_20190408151010.jpg'),
(5, 'choose', 'Why choose us', '<p><strong>Our services</strong></p>\r\n\r\n<p>1. Pre-sale Service</p>\r\n\r\n<p>We can provide you with fabric samples and product brochures for free, inclusive of freight.</p>\r\n\r\n<p>2. Selling Service</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>1) The delivery time of our products is approximately 20-35 days after we have received the deposit or letter of credit.</p>\r\n\r\n<p>2) There is no service for quick-wear parts.</p>\r\n\r\n<p>2. Selling Service</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>1) The delivery time of our products is approximately 20-35 days after we have received the deposit or letter of credit.</p>\r\n\r\n<p>2) There is no service for quick-wear parts.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Design Advantage</strong></p>\r\n\r\n<p>Our durable machinery effectively combines the strengths of analogous products. Manufactured from special alloy, the closed cams are automatically produced by the CNC engraving machine for features such as improved precision, excellent interchangeability, and long service life. We have collaborated extensively with a renowned Taiwanese manufacturer for the regulated production of high fidelity needle cylinders. Crafted from premium Japanese raw materials and processed with advanced technology, these cylinders are guaranteed to perform with exceptional efficiency.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Equipment Advantage</strong></p>\r\n\r\n<p>Our durable machinery effectively combines the strengths of analogous products. Manufactured from special alloy, the closed cams are automatically produced by the CNC engraving machine for features such as improved precision, excellent interchangeability, and long service life. We have collaborated extensively with a renowned Taiwanese manufacturer for the regulated production of high fidelity needle cylinders. Crafted from premium Japanese raw materials and processed with advanced technology, these cylinders are guaranteed to perform with exceptional efficiency.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Quality Advantage</strong></p>\r\n\r\n<p>Our durable machinery effectively combines the strengths of analogous products. Manufactured from special alloy, the closed cams are automatically produced by the CNC engraving machine for features such as improved precision, excellent interchangeability, and long service life. We have collaborated extensively with a renowned Taiwanese manufacturer for the regulated production of high fidelity needle cylinders. Crafted from premium Japanese raw materials and processed with advanced technology, these cylinders are guaranteed to perform with exceptional efficiency.</p>\r\n\r\n<p>HONGSHUO Machinery - trusted since 1996.</p>\r\n', ''),
(7, 'facility', 'Our facilities and photos', 'Our plant is equipped with a lot of advanced production equipment, such as the CNC vertical lathe, high-speed milling machines, CNC machining center and so on. There devices are imported from Japan & Germany and we gradually expand our plant, now our annual output can reach more than 2000 sets, before the shipment , machines will be adjusted and inspected by our technicians, so as to make sure that all machines are qualified.\r\nWe have sold machines to more than 30 countries and have appointed agents in several countries, including India, Iran ,Brazil ,North America,Syria,Egypt,Uzbekistan etc,', ''),
(8, 'secgeneral', 'এ বি এম রুহুল আমিন হাওলাদার', '<p>জাতীয় কাউন্সিলের পর দলকে শক্তিশালী করতে কোমর বেধেঁ মাঠে নেমেছে জাতীয় পাটি। তৃণমূল থেকে কেন্দ্র, সবখানে লেগেছে শক্তি সঞ্চয়ের ঢেউ। বিশেষ করে আগাম নির্বাচনের সম্ভব্নাকে সামনে রেখে মাঠ দখলের চেষ্টায় সাংগঠনিক কর্মকাণ্ডের মাধ্যমে দলের নেতাকর্মীদের ব্যস্ত রাখতে নেয়া হয়েছে কর্মসূচী।</p>\r\n', 'frontend_assets/images/page_settings/1515657962jpmosochib.png'),
(9, 'sovapoti', 'জি এম কাদের', 'জাতীয় কাউন্সিলের পর দলকে শক্তিশালী করতে কোমর বেধেঁ মাঠে নেমেছে জাতীয় পাটি। তৃণমূল থেকে কেন্দ্র, সবখানে লেগেছে শক্তি সঞ্চয়ের ঢেউ। বিশেষ করে আগাম নির্বাচনের সম্ভব্নাকে সামনে রেখে মাঠ দখলের চেষ্টায় সাংগঠনিক কর্মকাণ্ডের মাধ্যমে দলের নেতাকর্মীদের ব্যস্ত রাখতে নেয়া হয়েছে কর্মসূচী।', ''),
(10, 'secretary', 'এ বি এম রুহুল আমিন হাওলাদার', 'জাতীয় কাউন্সিলের পর দলকে শক্তিশালী করতে কোমর বেধেঁ মাঠে নেমেছে জাতীয় পাটি। তৃণমূল থেকে কেন্দ্র, সবখানে লেগেছে শক্তি সঞ্চয়ের ঢেউ। বিশেষ করে আগাম নির্বাচনের সম্ভব্নাকে সামনে রেখে মাঠ দখলের চেষ্টায় সাংগঠনিক কর্মকাণ্ডের মাধ্যমে দলের নেতাকর্মীদের ব্যস্ত রাখতে নেয়া হয়েছে কর্মসূচী।', ''),
(11, 'mission1', 'বাংলাদেশ জাতীয়তাবাদ ', 'জাতীয় কাউন্সিলের পর দলকে শক্তিশালী করতে কোমর বেধেঁ মাঠে নেমেছে জাতীয় পাটি। তৃণমূল থেকে কেন্দ্র, সবখানে লেগেছে শক্তি সঞ্চয়ের ঢেউ। বিশেষ করে আগাম নির্বাচনের সম্ভব্নাকে সামনে রেখে মাঠ দখলের চেষ্টায় সাংগঠনিক কর্মকাণ্ডের মাধ্যমে দলের নেতাকর্মীদের ব্যস্ত রাখতে নেয়া হয়েছে কর্মসূচী।', ''),
(12, 'mission2', 'গণতন্ত্র ', 'গণতন্ত্র হলো কোন জাতিরাষ্ট্রের (অথবা কোন সংগঠনের) এমন একটি শাসনব্যবস্থা যেখানে প্রত্যেক নাগরিকের নীতিনির্ধারণ বা সরকারি প্রতিনিধি নির্বাচনের ক্ষেত্রে সমান ভোট বা অধিকার আছে। গণতন্ত্রে আইন প্রস্তাবনা, প্রণয়ন ও তৈরীর ক্ষেত্রে সকল নাগরিকের অংশগ্রহনের সমান সু্যোগ রয়েছে, যা সরাসরি বা নির্বাচিত প্রতিনিধির মাধ্যমে হয়ে থাকে। ', ''),
(13, 'mission3', 'অর্থনৈতিক মুক্তি', 'জনগণের অর্থনৈতিক মুক্তি একটি দেশের মূল লক্ষ্য। বাংলাদেশের স্বাধীনতা অর্জনের পেছনে দুটি কারণ কাজ করেছিল। এক. পশ্চিম পাকিস্তান থেকে রাজনৈতিক মুক্তি, আরেকটি হলো অর্থনৈতিক মুক্তি। পাকিস্তান আমলে উন্নয়ন থেকে আমরা ছিলাম বঞ্চিত। ফলে রাজনৈতিক স্বাধিকার ও অর্থনৈতিক মুক্তি দুটিই ছিল বাংলাদেশের স্বাধীনতা অর্জনের মূল উদ্দেশ্য।', ''),
(14, 'mission4', 'প্রাথমিক অধিকার ', 'নাগরিক ও রাজনৈতিক অধিকার একরকমের অধিকারের শ্রেণী যা সরকার, সামাজিক সংগঠন, ও বেসরকারি ব্যক্তির দ্বারা লঙ্ঘনের থেকে একজন ব্যক্তির রাজনৈতিক স্বাধীনতা রক্ষা করে। একজন যাতে বৈষম্য বা নিপীড়ন ছাড়া সমাজ ও রাষ্ট্রের বেসামরিক ও রাজনৈতিক জীবনে অংশগ্রহণ করতে পারে তা নিশ্চিত করে।\n\nনাগরিক অধিকার অন্তর্ভুক্ত করে জনগণের শারীরিক ও মানসিক সততা, জীবন, ও নিরাপত্তার নিশ্চয়তা, জাতি, লিঙ্গ, জাতীয় মূল, রঙ, বয়স, রাজনৈতিক অন্তর্ভুক্তি, জাতিভুক্তি, ধর্ম, বা অক্ষমতার মতো ভিত্তিতে বৈষম্য থেকে সুরক্ষা  এবং একক অধিকার যেমন গোপনীয়তা এবং চিন্তার স্বাধীনতা, বাকশক্তি, ধর্ম, প্রেস, সমাবেশ, এবং আন্দোলনের স্বাধীনতা থেকে সুরক্ষা।\n\nরাজনৈতিক অধিকার অন্তর্ভুক্ত করে আইনের প্রাকৃতিক ন্যায় (পদ্ধতিগত সততা) যেমন আসামি অধিকার আইন, ন্যায্য বিচার করার অধিকার আইন যথাযথ প্রক্রিয়া প্রতিকার বা আইনগত প্রতিকার চাওয়ার অধিকার এবং এই ধরনের সমিতি স্বাধীনতার মত সুশীল সমাজ ও রাজনীতিতে অংশগ্রহণ অধিকার, জড় করার অধিকার, দরখাস্ত করার অধিকার, আত্মরক্ষাকরার অধিকার এবং ভোট করার অধিকার।', ''),
(15, 'jubosonghoti_me', 'যুব সংহতি', 'জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি । জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি। জাতীয় পার্টির ভ্যানর্গাড হিসেবে পরিচিত দলের অঙ্গসংগঠন জাতীয় যুব সংহতি।', ''),
(16, 'mailmessage', 'মেইল বাণী', 'আমাদের সকল কর্মসূচি, ইভেন্ট ও অন্যান্য সংবাদ সবসময় জানতে এখানে আপনার মেইলটি প্রদান করুন। সাবস্ক্রাইব করে জাতীয় পার্টির সাথে থাকুন। ', ''),
(17, 'amadersofolota1', 'উন্নয়নমূলক কর্মকাণ্ড', '৫০,০০০+', ''),
(18, 'amadersofolota2', 'সদস্য', '১ কোটি +', ''),
(19, 'amadersofolota3', 'ভলেন্টিয়ার', '৫০,০০,০০০', ''),
(20, 'whystudyat', 'Why Study @ DJ State College', '<p>With over 4,000 colleges and universities, the United States has more institutions of higher learning than any other country in the world. Many of them are highly ranked, offering top-notch educational programs, opportunities for hands-on learning, and cutting-edge research at the graduate and undergraduate levels. Many professors at U.S. institutions have terminal degrees in their field of expertise, are internationally recognized for their scholarship, and represent a diversity of ethnicities and cultural backgrounds. In addition, a significant number of the teaching staff have traveled or lived abroad, which contributes to an enriched classroom experience. Moreover, graduates from a U.S. university or college often find enormous success in the international job market. Employers recognize the value of such an education and the unique skills and qualities that these graduates possess. In short, a degree from a U.S. institution opens doors and is recognized around the world.</p>\r\n\r\n<p>International students enrolled at U.S. universities can select from an endless list of majors or degree programs, ranging from business and social sciences (for example, accounting) to the natural and physical sciences (for example, zoology) and everything in between. Along with the incredible variety of majors, you can also select a specialization, concentration, or minor, enabling you to craft a curriculum that is unique to your goals. Students also have the freedom to start as a &ldquo;no preference&rdquo; or &ldquo;undecided&rdquo; major! Imagine an educational system where you can begin your university studies uncertain of what you want to do&mdash;in many countries, this is simply not an option.</p>\r\n\r\n<p>Regardless of your academic choices, most universities require all students to complete basic or general education courses in areas such as math, writing, science, history, and social science. This educational philosophy is known as the liberal arts. Exposure to a wide range of subjects provides an excellent foundation for professional development and allows graduates to pursue jobs in areas outside of their major or expertise. In addition, graduates are well prepared to later pursue a master&rsquo;s or a Ph.D., if they so desire.</p>\r\n', 'frontend_assets/images/page_settings/1516603829students.jpg'),
(21, 'principalmessage', 'Principal\'s Message', '<p>The year 2018&nbsp;marked the beginning of an era as the gates of DJ STATE COLLEGE&nbsp;opened to cater to the education needs of PCMC. This institution was created with an objective of providing an outstanding 360 degree education in character building, leadership, extra and co-curricular activities along with academics along with a very strong, dedicated and capable team of educators who became the driving force in the growth of the School.</p>\r\n\r\n<p>The other highlights in the repertoire of achievements in this year have been the awards conferred upon by Education Today ranking us No 3 in Pune and India Today, which bestowed upon us the honor of Excellence in CBSE Education, Pune.</p>\r\n\r\n<p>The journey that is laced beautifully with triumphs would not have been possible without you. I thank you parents, in all humility for your generosity in time, talents and resources. Thank you for constantly supporting us in every step we take and reposing your confidence in us. I speak for the entire team of GIIS Chinchwad in saying we would not accomplish what we do without your help. I deeply cherish the revered bond we have formed with you over the years. Thank you for entrusting us with the future of your children, for entrusting us to mold and sculpt their minds, enrich their souls and ignite the spark of lifelong learning in them.</p>\r\n\r\n<p>We also dedicate this coveted honor to all our students for displaying exceptional hard work in the pursuit of excellence by achieving honors and distinctions in not only academics but also in allied arenas of Arts, Creativity, Sports and Debate. Their dedication and respect makes me proud. I take this opportunity to acknowledge the extreme hard work of our dedicated staff and faculty by working round the clock to keep your School at the zenith of success.</p>\r\n\r\n<p>I take humble pride in sharing with you that we are ready to present&nbsp;<strong>Pune&rsquo;s largest play school- Global Montessori Plus</strong>. It is my belief that all of you will be delighted as we unveil the School&rsquo;s building very soon. It is rightly said that &ldquo;A dream becomes a goal when action is taken towards its achievement&rdquo; and we are committed in taking constructive and purposeful actions to produce optimistic, independent, compassionate, life-long learners and leaders who will bring glory to the School, State and the Nation.</p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>Mrs. Faizu,&nbsp;</p>\r\n\r\n<p>Principal, DJ STATE COLLEGE.</p>\r\n', 'frontend_assets/images/page_settings/1516601132qwqwqw.jpg'),
(22, 'chairmanmessage', 'Chairman\'s Message', '<p>Dear members of the DJ STATE COLLEGE, the academic year 2017-2018 has just begun. I hope you all had a great time last year and a refreshing summer vacation.</p>\r\n\r\n<p>The first &ldquo;R&rdquo;, Remember - &ldquo;remember our Marist Brothers&rdquo;. 2016-2017 was a year of mixed emotions. On one hand, we co-celebrated the 200th anniversary of the founding of Marist Brothers with Marist schools across Asia; on the other, we had to wave a teary goodbye to our beloved school supervisor Brother Joseph, as he returned to his heavenly home in July 2017. Of course, the Marist Brothers will continue to help and guide us by sending us new brothers. But please do not take this for granted, my dear Xaverians. Cherish the Brothers&rsquo; presence among us while you can. Respect them when you see them and remember them in your daily prayers. So this is the first &ldquo;R&rdquo; - &ldquo;Remember&rdquo; our Marist Brothers.</p>\r\n\r\n<p>The second &ldquo;R&rdquo;, Return - &ldquo;return our hearts to study&rdquo;. If you are still students, I cannot tell how you have spent your summer. Some took a really long &ldquo;rest&rdquo;, while others kept engaged in learning mode even during holidays. Either way, at this beginning of a new school year, I urge you to return your hearts and attention to school work. Make studying your first priority. This does not mean that you cannot play, social with other or engage in other school activities. What I am asking is that your study should come first. When you have limited time and energy, and when there is a conflict in the schedule. You should make the right choices by making ways for studying.</p>\r\n\r\n<p>For teachers, parents and our old boys, I am pleased to tell you that our F.6 graduates had done brilliantly in the HKDSE 2017. However, we cannot, for a minute, think that the students can excel on their own without our support. &ldquo;Return&rdquo;, for us, means that we need to redirect our attention to our students&rsquo; studies and do our best as a part of the Xaverian family to make them succeed in what they are supposed to do as learners. So here is the second &ldquo;R&rdquo; of my message, &ldquo;Return&rdquo; our hearts to study.</p>\r\n\r\n<p>To conclude my school opening message in the beginning of 2017-2018, I want to emphasize two R&rsquo;s &ldquo;Remember&rdquo; our Marist Brothers and &ldquo;Return&rdquo; our hearts to study. I wish you all a wonderful year filled with memorable moments both inside and outside St. Francis Xavier&rsquo;s College.</p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>Mr. Al Alim,<br />\r\nChairman, DJ STATE COLLEGE.</p>\r\n', 'frontend_assets/images/page_settings/1516599814man.jpg'),
(23, 'scholarship', 'scholarship', '', ''),
(24, 'hostel', 'Hostel', '<p><strong>The DJ State College</strong>&nbsp;is a prestigious institution in our country, which provides accommodation to the students wishing to reside in the hostels. Each hostel is an independent unit in respects to its internal administration under the overall supervision of council of wardens and the hostel management. The hostels are located in the western side and adjacent to the college campus and have a separate hostel office and mess within the hostel premises. The mess has a centralized modern kitchen and a separate dining hall for boys and girls. Special menu will be introduced and served in the mess on all important religious festivals. Hostel celebrates almost all festivals celebrated across the country such as Independence day, holi, diwali, lakshmi, saraswathi and ganesha festivals etc. Every year hostel day showcases not only organizational abilities, but also active participation of all the hostelites in recreational, cultural, literary and social activities of the hostelites, who hail from various parts of the globe.</p>\r\n\r\n<p>The college has an exclusive placement and excellent training center to facilitate on campus placement opportunities to the students. Every year, the SCT hostilities shows remarkable results in the recruitment drive. It is proud to place on the records that, our hostilities have proved that they are among the top brains of the SCT. Almost entire hostel is placed and most of them have more than one job in their hands of their choice. The environment in the hostel is very conductive for placement preparations. Each and every hosteler co-operates in the placement activities by helping each other. The group studies gives every hostilities a very good opportunity to improve themselves in the areas in which they lack. The result of the strong team work shall be awesome placements from boys hostel and girls hostels.</p>\r\n', 'frontend_assets/images/page_settings/1516605168hostel.jpg'),
(25, 'medicallab', 'Medical Lab', '<p>Free healthcare services at DJ State College may seem too good to be true.&nbsp;After all, how can DJ State College afford to provide free medical treatment?&nbsp; The answer lies in innovation. &nbsp;Through creative programs, the college&nbsp;leverage student support to create opportunities for free healthcare.&nbsp;&nbsp;</p>\r\n\r\n<p>For example, students who are enrolled in dental programs, health care classes, and other&nbsp;medical fields are often required to participate in hands-on field practice.&nbsp;DJ State College clinics provide&nbsp;students in the medical fields&nbsp;with realistic training experiences &ndash; which translate into a mutually beneficial situation for both the students and community.&nbsp;</p>\r\n\r\n<p>In these clinics, students often hold the responsibility of providing examination and health services to any incoming patients.&nbsp;These students, of course, are highly qualified in their area of study, and they are also required to be supervised by a certified and licensed professional.&nbsp;This translates into solid medical care for the community. DJ State College also provides students with additional health amenities such as:</p>\r\n\r\n<ul>\r\n	<li>Information on flu vaccinations</li>\r\n	<li>Fitness center access</li>\r\n	<li>Swimming pool access</li>\r\n	<li>Tobacco prevention and cessation support</li>\r\n	<li>Substance abuse education</li>\r\n</ul>\r\n', 'frontend_assets/images/page_settings/1516612951free_health_service.jpg'),
(26, 'transport', 'Transport', '<p>The academy has safe and secure bus and van transport facility to pick-up and drop all the day-scholars at their scheduled stop matching well to the notified timings. The parents are expected to bring their child/ward to the bus stop as notified and escort them back from the same stop. Private/personal conveyance is not permitted without prior permission of the principal. Students can avail the school transport, subject to availability of seats. The school does not entertain any request for change in bus route to suit on individual&rsquo;s convenience. As a rule such request should not be made. In case of emergency, parents should take their wards personally with prior permission of the principal.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n<span style=\"font-size:14px\"><strong>Parents to please note bus rules:</strong></span><br />\r\na) No students will board the bus without an identity card.<br />\r\nb) Unauthorized persons are not allowed to travel by the school bus.<br />\r\nc) The buses will pick-up and drop the students from their respective stops as per the school schedule.<br />\r\nd) It is the responsibility of the parents to escort their wards into the bus in the morning and from the bus stop in the afternoon.<br />\r\ne) Bus commuters and escort are expected to reach their respective stops 10 minutes before scheduled time.<br />\r\nf) Discipline at the stop and inside the bus should be maintained. Students will be held responsible for any damage to buses caused by negligence or vandalism.<br />\r\ng) Bus charge will be charged for all th eleven month of the session and should be paid in advance along with the school fee.<br />\r\nh) If a student wants to discontinue the transport facility during the session, he/she will have to pay six month extra charges.<br />\r\ni) Parents are requested to refrain from breaking into grievance regarding bus services or the staff on board. Any grievance regarding bus services or the staff on board should be made to the transport incharge.<br />\r\nj) The bus facility is extended as a privilege and cannot be claimed as a right. It can be withdrawn any time due to any emergency.<br />\r\nk) Parents are expected to co-operate with the school authorities in case of delay of bus arrival at the stop. During morning hours a bus commuter is expected to wait at the stop at least 40 minutes for the bus before returning home.</p>\r\n', 'frontend_assets/images/page_settings/1516615708transportation.jpg'),
(27, 'digitallab', 'Digital Lab', '<p>The expansion and modernization of the College of Science laboratories was the main impetus behind the construction of the Jordan Hall of Science, and there is little doubt that the 40 laboratories throughout the building represent a huge qualitative&mdash;not to mention quantitative&mdash;leap for science education on campus. In one swift move, the opening of Jordan Hall propelled the College&nbsp;to the head of the class in terms of creating unparalleled learning environments for preparing future generations of scientists. The labs provide hood access for all students, pre-lab lecture space, and data analysis rooms.</p>\r\n\r\n<p>In addition to state-of-the-art equipment and an abundance of space, the labs also offer students unprecedented opportunities for interdisciplinary collaboration through carefully planned adjacent lab spaces. For example, the biochemistry lab is adjacent to the cell biology/genetics lab, and analytical chemistry is adjacent to ecology. By locating lab spaces in close proximity, the College of Science curriculum is on the cutting edge of the trend in science education toward multidisciplinary study. In short, these labs now make it possible for our students to perform a wide range of complex experiments and acquire the skills necessary to conduct modern research in a safe and progressive scientific environment.</p>\r\n', 'frontend_assets/images/page_settings/1516606381digital_lab.jpg'),
(28, 'admissionmessage', 'Admission Message', '<p>Once students have received their acceptance letter to Keyano College, they will be advised on how to register for their program and/or courses. Along with an outline of the procedure, students will be notified about program fees and will be provided with other relevant information about the College. A student is considered registered once their courses are entered in the student information system in the Office of the Registrar and an assessment sheet of tuition and related fees is produced. Upon registration in College credit courses, students are classified in either of these categories:</p>\r\n\r\n<ul>\r\n	<li>Full-time Students</li>\r\n	<li>Part-time Students</li>\r\n</ul>\r\n\r\n<p>For the purposes of enrollment reporting for agencies such as Alberta Students&#39; Finance, Alberta Colleges Athletic Conference (ACAC) and Employment and Immigration Canada, students taking 60% or more of the full program load in each term are classified as full-time.A student who is registered in less than 60% of the full program load are classified as part-time students. Students who are receiving funding by student loans must maintain full-time status throughout the term. Students are responsible for knowing their registration status. A change in status may affect eligibility for sponsorship, loans, athletics, scholarships, financial awards and some College services.</p>\r\n\r\n<p><strong><em>Note: Students registering in Credit programs must apply first then can register for the specific courses after being admitted.</em></strong></p>\r\n\r\n<h2><strong>Confidentiality</strong></h2>\r\n\r\n<p>The personal information a student provides upon admission is being collected under the authority of the Colleges Act and in accordance with the Freedom of Information and Protection of Privacy Act of the Province of Alberta. Data collected is used to determine your eligibility for admission to a program and if accepted and registered, is used to process the student&#39;s enrollment, to administer and evaluate College programs, and for statistical purposes. Personal data compiled may also be used by the College or disclosed to third parties for other operational purposes that are consistent with the mission of Keyano College or as required by the Statistics Act of Canada, or by the Alberta government. Keyano College will only release student information with the written consent of that student. Consent to Release forms are available from the Office of the Registrar. Public inquiries directly related to the collection and use of this information should be directed to the Keyano College FOIP Coordinator at 780-791-4853.</p>\r\n', 'frontend_assets/images/page_settings/1516701315Admission.jpg'),
(29, 'collagefee', 'collagefee', '', ''),
(30, 'examrtn', 'examrtn', '', ''),
(31, 'sportacademy', 'Sports Academy', '<p><span style=\"font-size:14px\">DJ State College Academy for Sport has been in existence since September 2003. Since this time we have developed Academies in football, rugby league, basketball, aquatics and golf. We now boast fantastic partnerships with professional and elite clubs including Leeds Rhinos, Leeds United FC, and Farsley Celtic FC.</span></p>\r\n\r\n<h3><strong><span style=\"font-size:14px\">Football Academy</span></strong></h3>\r\n\r\n<p><span style=\"font-size:14px\">The College Football Academy works closely with partners to provide exit routes onto USA Scholarships or preparing players for professional clubs and university. UEFA and Academy coaches will work with players three times a week, utilising the support of performance analysis, strength and conditioning and nutrition.</span></p>\r\n\r\n<h3><strong><span style=\"font-size:14px\">Rugby Academy with Leeds Rhinos</span></strong></h3>\r\n\r\n<p><span style=\"font-size:14px\">The partnership between Leeds City College and Leeds Rhinos has given young players a different pathway to progressing to a professional club. The college team is coached by Leeds Rhinos Academy coaches and training is based at the Leeds Rhinos Training Facilities, including Headingley Stadium.The Rhinos also help us by providing talks and careers advice to our young students through the medium of rugby league.</span></p>\r\n\r\n<h3><strong><span style=\"font-size:14px\">Golf Academy</span></strong></h3>\r\n\r\n<p><span style=\"font-size:14px\">DJ State College&nbsp;has set up a Golf Academy to help teach the next wave of the region&rsquo;s aspiring professional golfers after agreeing to introduce a new Elite Student Golf Programme. Students who elect to take part in the programme will take full advantage of state of the art facilities and receive coaching and tuition from fully qualified staff within the golfing industry.</span></p>\r\n', 'frontend_assets/images/page_settings/1516690739football-academy.jpg'),
(33, 'cookdriveacademy', 'Cooking And Driving Academy', '<p><strong>Driving Training</strong>: More emphasis on life-long driving skills. A positive attitude to safe driving underpinned by proven methodology. Highly qualified driving instructors. 50-minute lessons. Modern air-conditioned dual-controlled vehicles. Automatic and manual lessons available statewide. Multiple lesson packages at discounted rates. Government recognised L2 licence assessments. General driving assessments for licensed drivers looking for an assessment of their driving competence. Low-risk courses offering practical and theoretical sessions. Business Safety Essentials workshops for organisations that have staff that drive as an extension of the workplace. Keys2Drive information evenings and Keys2Drive FREE lesson. Below are the steps to gaining a licence in Tasmania: To hold an L1 licence, you must pass a theory test of the Tasmanian road rules at a Service Tasmania outlet. This can be practiced online on the transport website. &nbsp;L1s &ndash; You must hold your L1s for a minimum of three months before you are able to sit the L2 assessment. L2 assessments can be booked with your RACT instructor for your convenience and peace of mind. L2s &ndash; Once the L2 assessment has been passed, you have nine months to log 50 hours of driving. Once this has been achieved, a P1 assessment can be sat through the Department of State Growth. P-plate assessments can now be booked through RACT using the State Growth/Service Tasmania online portal &ndash; ask our friendly RACT Driver Training staff for details. P1 &ndash; You must hold your P1s for a minimum of 12 months without losing your licence or committing a P1 restart offence. P2 &ndash; The length of time you must hold your P2 licence for varies with your age: 18 to under 23 &ndash; two years. 23 to under 25 &ndash; 12 months or until aged 25 (whichever is longer). Over 25 &ndash; 12 months. If you undertake a practical driving test in an automatic vehicle you will be issued with a licence restricting you to driving only automatic vehicles for that particular class. If you hold an L1, L2, P1 or P2 licence without infringeme.</p>\r\n\r\n<p><strong>Cooking Training</strong>:<br />\r\nThe scent of spices and the taste of India&#39;s many flavours will offer you a unique gastronomic experience. In the north, the staple diet includes a range of flat breads such as porris, chappatis, nans and paranthas. In the south, a repertoire based on rice is an essential part of any meal fluffy white idlis, steamed cakes of ground rice and lentils and crisp dosas. Dals or split lentisls are common fare throughout India. Many of the culinary delights of India have originated from the great royal courts of earlier times. North Indian cuisine is most influenced by the great Mughals and savoured in the luscious curries, kormas and in the delicious tandoori recipes varieties of kababs and spicy roasted chicken. Other royal cuisines include the &#39;Wazwan&#39; from Kashmir, the &#39;Dum-Pukht&#39; cuisine of Oudh and Hyderabad&#39;s rich pulaos and biryanis.</p>\r\n', 'frontend_assets/images/page_settings/1516769723600x418.png'),
(34, 'scienceacademy', 'Science Academy', '<p>This program is a hands-on, interactive series of one-day experiential learning activities for Native American students in 9th-12th grade.&nbsp; The SSA curriculum focuses on Native American culture and the sciences, where students explore topics in fields of science, technology, engineering and math.&nbsp; Since the start of SSA in Feb. 2011, 268 students have engaged in 22 events with activities investigating topics such as Physics and Rocket Science, Wetland Restoration and the Salmon Life Cycle, Astronomy and Native Starlore, Ethnobotany and Plants of the Pacific Northwest, Geology of the Pacific Northwest,&nbsp; Intertidal Marine Biology and Service-learning in the Mt. Baker-Snoqualmie&nbsp; National Forest.&nbsp;&nbsp; Each event entails periods of lecture, field exploration, and cultural experience.&nbsp; For example, on the day youth explored the topic of ethno botany: students were taught traditional ways of using plants as medicine, food, and shelter by a native traditional plants expert, then given a guided hike through a local state park to examine these plants in the field, and finished with the making of a traditional herbal tea from collected and dried plants.<br />\r\n<br />\r\nThis is a two-week program for science-minded, Native American high school teens to experience intimate on-campus life, integrated science exploration and the development of cultural identity.&nbsp; This year will be the 3rd Annual Summer Science Camp and students will engage in activities such as DNA Disease Outbreak Investigation at the Children&rsquo;s Research Institute, Night Hike and Bonfire Storytelling in the North Cascades National Park, Marine Research &amp; Whale Cruise through the San Juan Islands, Tour of the Boeing Corp. Airplane Manufacturing Plant, Low Ropes Challenge Course, Lummi Canoe Journey Landing Ceremony, Listen to the Unungax Tribal Song &amp; Dance Group, Learn Archery with a former Olympic Coach, and so much more.&nbsp; Camp participants will join Native American youth from around the country to experience on-campus dormitory living, converse with current college students and faculty, and learn about college enrollment, admission and financial aid.&nbsp; Moreover, students will work in college lab facilities to conduct real-time research work.&nbsp; Students will develop, test and analyze their own working research project and present the outcomes of their work in a showcase at the end of camp.</p>\r\n', 'frontend_assets/images/page_settings/1516692376science-lab.jpg'),
(35, 'lifestyleacademy', 'Lifestyle Academy', '<p>At HEED SKILL DEVELOPMENT ACADEMY Nation Building by enhancing the employability of our youth is a passion and priority. We serve Corporate India by partnering with them to skill their employees and help them compete globally. We Bridge the Talent Gap; that is our mission.</p>\r\n\r\n<p>HEED SKILL DEVELOPMENT ACADEMY is committed to provide quality training for skill repair at an affordable cost and reach out to urban and semi urban India to meet the shortfall. We provide holistic training and placement services for the youth. The HEED SKILL DEVELOPMENT ACADEMY Vision &quot;Create a sustainable industry aligned ecosystem by promoting skill development, benefiting millions in India to get respectable employment opportunities for serving customers and other stakeholders.</p>\r\n\r\n<p>&quot;The HEED SKILL DEVELOPMENT ACADEMY Mission &quot;Develop over 10 million industry competent workforces by 2022 through establishment of National Occupational Standards, Labour Market Information System, Accreditation of training partners / vocational institutions, Certification of Trainers and facilitation of learner assessments and certifications.</p>\r\n', 'frontend_assets/images/page_settings/1516700259skill.jpg'),
(36, 'languageacademy', 'Language Academy', '<p>State College Institute of Languages, associated with DJ State College, is out in existence to help students with language proficiency. It offers various courses on foreign languages like English, French, Spanish, German, Arabic, Chinese, Japanese and Korean. It also offers Basic Bangla course for foreigners. It has both short and longer courses.&nbsp;<br />\r\n<br />\r\nEstablished in 2013, State College Institute of Languages is situated at the college&nbsp;campus in Dhanmondi, Dhaka, a convenient location to reach from all sides of the city. Well equipped with modern language learning facilities, it pays special attention to the needs of students. In a cordial and friendly atmosphere, classes are conducted by efficient and experienced teachers, trained at home and abroad.&nbsp;<br />\r\n<br />\r\nIf you are interested in learning English or other foreign languages for academic, professional or personal needs, we will be more than happy to assist you in fulfilling your language acquisition goals. Our mission is to guide you through the language learning process in a progressive way to satisfy your needs. Our curriculum emphasizes preparing individuals for highest academic and professional success.<br />\r\n<br />\r\nLearning a foreign language is always an advantage in today&rsquo;s multilingual global setting. It means good employment opportunities for those who are willing and prepared. Foreign employers prefer those who know their languages. It is also essential when one is traveling abroad and trying to know the culture of other races and communities. Through learning languages, the literature, arts, films,&nbsp;music etc of other countries can be better understood. Above all, learning a foreign is a great fun. State College Institute of Languages is careful to make your foreign language learning experience enjoyable.&nbsp;</p>\r\n', 'frontend_assets/images/page_settings/1516693282aaa.jpg'),
(37, 'informationcenter', 'Information Center', '<p style=\"text-align:justify\">The Information Center welcomes you to Hawaiʻi Community College. The Information Center exists to serve the public in providing general information about Hawaiʻi Community College. The Information Center is located in Building 378, Room 15A - it is located in the very front of the College campus and is situated in next to the Admissions &amp; Records Office and the Financial Aid Office. Together in the same location, the Admissions &amp; Records Office, the Information Center, and the Financial Aid Office are the Welcome Center.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>THE MISSION OF THE INFORMATION CENTER:</strong><br />\r\nPromote and provide accessibility to higher education by offering a convenient one-stop location which delivers current, accurate information and services to prospective individuals, groups, and private/public sector organizations via web, live chat, mail, telephone, tours, in person, printed material.<br />\r\n<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>SERVICES FOR EVERYONE</strong><br />\r\n1. Coordinate Program and Campus Visits from High School Groups and Individuals.&nbsp;<br />\r\n2. Represent College at High School Fairs&nbsp;<br />\r\n3. High School/Intermediate Outreach to Promote College Readiness&nbsp;<br />\r\n4. Walk-In Directory&nbsp;<br />\r\n5. Campus Phone Directory&nbsp;<br />\r\n6. Enrollment Checklist&nbsp;<br />\r\n7. Campus Maps/Directions&nbsp;<br />\r\n8. Hele On Bus Schedules&nbsp;<br />\r\n9. Urgent Services for Families (phone directory)&nbsp;</p>\r\n\r\n<p><strong>SERVICES FOR STUDENTS</strong>&nbsp;<br />\r\n1. Coordination of the Communication of College Resources via E-mail Broadcasts to the Student Body&nbsp;<br />\r\n2. Faculty/Staff /Program/Office Directory Assistance&nbsp;<br />\r\n3. Kauhale Newsletter &amp; Ka ʻIo News&nbsp;<br />\r\n4. Publicize the yearly Spring Commencement list of graduating students&nbsp;<br />\r\n5. Student Recognition via the Dean&#39;s List in the Tribune Herald&nbsp;<br />\r\n6. Updates Information on the 11 Information Boards on Manono Campus (Announcement Postings should be approved by the Information Center)</p>\r\n\r\n<p><strong>SERVICES FOR HAWAIʻI CC FACULTY &amp; STAFF</strong><br />\r\n1. Coordination of Program Information with all Programs at Hawaiʻi Community College<br />\r\n2. Coordination of Promotional Material with Media Graphics (Catalog Cover, Program Brochures, Commencement Ad)<br />\r\n3. Submit Public Service Announcements from the Hawaiʻi CC community to public media.<br />\r\n4. For Hawai&#39;i CC Guidelines and Procedures for event submissions click here. Please share with us how we are doing and how we can serve you better. Thank you for your interest in Hawaiʻi Community College. It is our hope that you find education more easily accessible and helpful in your educational goals and attainments.<br />\r\n<br />\r\n<strong>HOURS OF OPERATION</strong><br />\r\nWalk In Monday - Friday 8:00 am - 4:00 pm<br />\r\nMonday - Friday 8:00 am - 4:30 pm<br />\r\n<br />\r\n<strong>Contact Information Information Center Office:</strong><br />\r\nManono Campus, Bldg 378 Hawaiʻi Community College 1175 Manono Street Hilo, HI 96720<br />\r\nPhone: (808) 934-2500 Fax: (808) 934-2501 Email: hawccinf@hawaii.edu</p>\r\n', 'frontend_assets/images/page_settings/1516702728info.jpg'),
(38, 'admissionoffice', 'Admission Office', '<p>The Admissions Office at the University of Sk&ouml;vde can assist exchange students from partner universities abroad. Free-movers who want to apply to our programmes at master&#39;s and bachelor&#39;s level must be submit their application via www.universityadmissions.se, the national Swedish university application service.</p>\r\n\r\n<p>Admission to courses for exchange students is a different&nbsp;process than that&nbsp;of other international students. Only students from universities that the University of Sk&ouml;vde has an exchange student agreement with can be nominated and admitted to exchange studies.</p>\r\n\r\n<p>Students from all over the world, including non-nominated students from partner universities, can apply to our international master&#39;s programmes (second cycle), as well as single-subject&nbsp;courses at undergraduate (basic, first-cycle) or graduate (advanced, second-cycle) level.</p>\r\n\r\n<p>Basic requirements for masters&#39; programmes and graduate courses are a Bachelor&#39;s degree and proof of English proficiency. Basic requirements for undergraduate courses are a completed upper secondary education and proof of English proficiency. Beyond this, most programmes and courses have&nbsp;specific requirements,&nbsp;such as prior completed courses within a certain subject area.</p>\r\n\r\n<address><strong>Questions about admission?</strong>&nbsp;Phone: +46 (0)500-44 82 00&nbsp;<a href=\"mailto:admission@his.se\">admission@his.se</a></address>\r\n\r\n<address><strong>Questions about admission for exchange students?&nbsp;</strong>Phone: +46 (0)500-44 82 00&nbsp;<a href=\"mailto:exchangestudent@his.se\">exchangestudent@his.se</a></address>\r\n\r\n<address><strong>Contact a student and career counsellor?</strong><a href=\"http://www.his.se/contactacounsellor\">http://www.his.se/contactacounsellor</a></address>\r\n\r\n<address><strong>Contact the Student Accommodation Service?&nbsp;</strong><a href=\"mailto:bostadsformedling@his.se\">bostadsformedling@his.se</a></address>\r\n\r\n<address><strong>Contact International Office?</strong>&nbsp;<a href=\"mailto:international@his.se\">international@his.se</a>&nbsp;</address>\r\n', 'frontend_assets/images/page_settings/1516705072addmission_office.jpg'),
(39, 'digitalcampus', 'Digital Campus', '<p>Union College encourages students from all faith backgrounds to find their God-designed potential for leadership, scholarship and service. At Union, students develop spiritual and professional gifts; find professors who care for and respect the individual; and seize opportunities to create, learn and connect.</p>\r\n\r\n<p>An accredited, comprehensive college, Union offers active learning in a vibrant Christian atmosphere where students connect the classroom and their world. With a focus on undergraduate studies, Union&rsquo;s nurturing environment offers a traditional liberal arts education combined with practical experiences such as internships, academic and career counseling, study abroad and volunteer opportunities. An honors program challenges high academic achievers, and Union&rsquo;s Teaching Learning Center helps students of all academic abilities achieve their full potential.</p>\r\n\r\n<p>In the comfortable campus atmosphere, professors not graduate students teach classes. Students choose from more than 50 undergraduate majors, minors and emphases in 27 fields of study including a personalized degree and a Master of Physician Assistant Studies. Union&rsquo;s newest academic option, international rescue and relief, is the only bachelor&rsquo;s program of its kind in the United States. This high-energy major prepares students for careers of humanitarian service or disaster response around the world.</p>\r\n\r\n<p>Located in the capital city of Lincoln, Nebraska, Union&rsquo;s arboretum campus offers billowing trees in a sprawling park-like setting. Founded in 1891, Union&rsquo;s 50-acre setting blends the ivy and brick of collegiate tradition with modern facilities.</p>\r\n\r\n<p>Union College is owned and operated by the Seventh-day Adventist Church and exhibits a diverse international climate. Many countries and most states are represented, although students come primarily from a nine-state area known as the Mid-America Union Conference. The college is sensitive to individual, racial and ethnic differences and seeks students and personnel from diverse cultural backgrounds. Discover your strengths at Union College&mdash;academic, athletic, artistic, social and spiritual. Create your own experience at Union. Call 800.228.4600.</p>\r\n', 'frontend_assets/images/page_settings/1516706633kkk.jpg');
INSERT INTO `tbl_common_pages` (`id`, `name`, `title`, `body`, `attatched`) VALUES
(40, 'futurecampus', 'Future Campus', '<p>Collaboration, accessibility, and flexibility are topics that frequently come under consideration when investigating the future of education. Economic and technological forces increasingly enable virtual learning for a wider sector of the population. However optimistic these possibilities may seem, the future of education continues to be hotly debated. What if the digitisation of education leads to the replacement of educators with computers, and what does this mean for the future of the built environment?</p>\r\n\r\n<p>Such a change has posed a unique opportunity for non-traditional collaboration between the construction industry and other organizations. The&nbsp;<em>Campus of the Future</em>&nbsp;report documents an ongoing programme of research into diverse campus typologies.</p>\r\n\r\n<p>The report covers four thematic areas, which we believe will prove instrumental in shaping the future of higher education. These will redefine the notion of the campus in terms of its design, the services it enables and the way in which it operates. The four themes are:</p>\r\n\r\n<ul>\r\n	<li>Students of the future: their needs and expectations</li>\r\n	<li>Changing the delivery of higher education</li>\r\n	<li>Physical facilities and learning environments</li>\r\n	<li>Skills needed by future employers</li>\r\n</ul>\r\n\r\n<p>Each of these sections is supported by a number of case studies, Twitter thoughts and scenarios that illustrate the drivers shaping change and what the campus might look like decades into the future. The report was created with input from a wide range of Arup specialists, including Arup&rsquo;s Education business, as well as engineers, planners, educators, and technologists from collaborating organisations. This report was developed in collaboration with students from Central Saints Martins College of Art and Design MA Creative Practice for Narrative Environments.</p>\r\n', 'frontend_assets/images/page_settings/1516707673future-campus-1.jpg'),
(43, 'handwritingacademy', 'Hand Writing and Art Academy', '<p>Achieving excellence in handwriting Developing excellent handwriting and high quality presentation skills gives children a real sense of pride in themselves and their work, and increases their motivation and enthusiasm for writing. For teachers it promotes a unity of purpose and shared values. I can transform handwriting and presentation of pupils&#39; work in your school in no time at all.The number of days for a project varies according to the size of your school. I have delivered projects ranging from 1 day to 6 days, depending on the number of classes the school has. Remember though, the greater the input, the greater the long term impact on the quality of handwriting. I do 9 x 30 minute handwriting lessons in a day, in which teachers and support staff participate and observe the methods which I employ to teach handwriting effectively. The staff meetings focus on elements listed above for the staff Inset day. Some schools have opted for an Inset day, followed by demonstration lessons in school. Painting and Drawing Teaching art effectively, is much greater than creating a good picture. It promotes personal qualities which improve children&#39;s performance in all curriculum areas - care, concentration, perseverance and close observation. I have yet to meet a teacher who doesn&#39;t want their pupils to be able to draw and paint well, but I have met many teachers who have no training in simple, but effective ways of teaching children how to draw and paint with confidence and enjoyment. Teachers often have no faith in their own artistic ability and consequently this effects their confidence to teach art. My approach to teaching art is very much skill based, with great emphasis placed on correct and careful use of tools and materials, and setting children tasks appropriate to their skill levels when putting skills into practice.</p>\r\n\r\n<p>The drawing skills focus on mark making, creating a array of marks, lines, shapes and patterns using pencil or pens of various types. Because this develops good pencil control, it ties in very closely with my handwriting projects. When teaching watercolour or powder painting, with all age groups I start with the basics of how to colour mix, how to use water, how to use brushes and how to apply paint. I use the phrase, &#39;Keep in simple, do it well!&#39; Many is the time, after taking part in my workshops, that teachers have commented, &#39;Wasn&#39;t that simple! But no-one has shown me how to do that before!&#39; A very simple comment, but very profound and revealing.</p>\r\n', 'frontend_assets/images/page_settings/1516771799Handwriting.jpg'),
(44, 'teachingmethod', 'Teaching Method and Classes', '<p>Engaging, interactive and communicative teaching methods play an essential part&nbsp;on all our classes. We know from experience that this is the only way to prepare our students to communicate effectively in English in the world today.</p>\r\n\r\n<p>To help our students best achieve their learning goals, our teachers use a variety of proven teaching methods, in conjunction with the most up-to-date teaching materials and cutting-edge learning technologies.</p>\r\n\r\n<p>Our teachers work hard to ensure that all students get the individual attention they need. A sense of progress is also vital, so teachers provide regular personal feedback to students on how they are doing.<br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We believe that English classes should:</p>\r\n\r\n<ul>\r\n	<li>help you to achieve more in life</li>\r\n	<li>be motivating,</li>\r\n	<li>challenging and enjoyable</li>\r\n	<li>give you the chance to make friends with others.</li>\r\n</ul>\r\n\r\n<p><br />\r\nIn addition, we believe that teachers should:</p>\r\n\r\n<ul>\r\n	<li>be knowledgeable,&nbsp;</li>\r\n	<li>friendly and helpful</li>\r\n	<li>adapt the lesson content to&nbsp;student needs</li>\r\n	<li>teach you study skills,</li>\r\n</ul>\r\n\r\n<p>so you continue learning after class has finished.</p>\r\n\r\n<ul>\r\n</ul>\r\n', 'frontend_assets/images/page_settings/1516773918teaching.jpg'),
(45, 'academiccalender', 'Academic Calender', '<p>The youth wing has already announced a nationwide campaign to get members to sign up in order to influence the outcome of the membership vote, using the slogan &ldquo;a tenner against the grand coalition&rdquo;, referring to &euro;10 (&pound;8.80) being the cost of two months&rsquo; membership.</p>\r\n\r\n<p>Frederick Cordes, the Young Socialists leader in North-Rhine Westphalia, told the Rheinische Post newspaper: &ldquo;What matters now is that we bring as many GroKo opponents into the party as possible so we can blow up the result at the membership vote.&rdquo; SPD branches in a number of German states have reported significant rises in membership this week, with its North-Rhine Westphalia stronghold registering 520 applications on Monday alone. Johannes Kahrs, a spokesman for the SPD&rsquo;s liberal Seeheim circle of MPs, tweeted: &ldquo;Allowing people to join for only two months in order to vote no is indecent. Joining and then leaving is nasty trickery.&rdquo;</p>\r\n', 'frontend_assets/images/page_settings/1516793715pdf-sample.pdf'),
(46, 'examschedule', 'Exam Schedule', '<p>They were detained on 12 December after they had been invited to meet police officers over dinner in Yangon. The government has cited police as saying they were arrested for possessing secret documents relating to the security situation in Rakhine.</p>\r\n', 'frontend_assets/images/page_settings/1516854188FinalExamSchedule.pdf'),
(47, 'admissionform', 'Admission Form', '<p>Richardson said he declined to join the advisory board&rsquo;s tour of a new repatriation camp in Rakhine state on Wednesday, instead travelling to Yangon. Myanmar&rsquo;s military&nbsp;said earlier this month&nbsp;its soldiers had taken part in the killings of 10 captured Muslim &ldquo;terrorists&rdquo; at the beginning of September, after Buddhist villagers had forced the captured men into a grave the villagers had dug. It was a rare acknowledgment of wrongdoing during its operations in Rakhine by the Myanmar military, which said legal action would be taken against members of the security forces who violated their rules of engagement and the villagers involved. Richardson said he has asked the board to recommend that the Myanmar government set up an independent investigation into &ldquo;the mass grave issue, especially as it pertained to ... the involvement of the military&rdquo;. He did not say how the board had responded.&nbsp;</p>\r\n', 'frontend_assets/images/page_settings/1516854961admission-form.pdf'),
(48, 'faq', 'Frequently Asked Questions', '<p><span style=\"font-size:16px\"><strong>1. What are the admission requirements?</strong><br />\r\nOur Board of Admission completes a holistic review considering academic achievement, leadership development, community service, extracurricular activities, and personal essay among other factors. While we do not have set grade point average and standardized test score requirements, we do have an academic profile that is set for each year.<br />\r\n<br />\r\n<strong>2.&nbsp;What is the average class size?</strong><br />\r\nThe average class size is approximately 30 students, but many&nbsp;upper-level courses within a student&#39;s major are much smaller. The student-professor ratio is 11:1.<br />\r\n<br />\r\n<strong>3.&nbsp;Does DJ State College offer any summer programs?</strong><br />\r\nDJ State College offers the Prefreshman Summer Science Program to admitted students interested in a career in the health professions prior to their first year at DJ State College. The six-week long program is designed to strengthen study and performance in mathematics, library research, problem solving, biology, computer science, chemistry, and quantitative analysis.<br />\r\n<br />\r\n<strong>4.&nbsp;Are there opportunities for DJ State College students to study abroad?</strong><br />\r\nDJ State College provides students the opportunity to study at a variety of institutions around the world, including locations such as Ghana, England, Scotland and the Czech Republic.<br />\r\n<br />\r\n<strong>5.&nbsp;What medical services are provided for students?</strong><br />\r\nThe Health Services department is open to all&nbsp;DJ State College students from 9 a.m. to 5 p.m., Monday through Friday. A physician and nurse practitioner are available to see patients during office hours and nurses can be paged after hours. Should you require emergency medical attention, there are several hospitals in the area that have established relationships with DJ State College. All full-time students are required to participate in a health insurance program which assists with the cost if a student requires medical attention that the Health Services department is unable to provide.</span></p>\r\n', ''),
(49, 'vision', 'Our vision', 'Professional technology, precision manufacturing and satisfaction service.', ''),
(50, 'annual', 'Annual output of 2000', 'we are now able to assemble up to 2000 machines each year.', ''),
(51, 'origin', 'Originate in 1996', 'Years of development have allowed us to mature our design and production techniques.', ''),
(52, 'profile', 'Company Profile', '<p>Fabrics such as mesh, cotton, and Terry are selected for the fashioning of our clothing and accessories, but where do they come from? If you wish to achieve bulk production, then the answer is knitting machines. As an experienced Chinese manufacturer, HONGSHUO designs and assembles all kinds of circular knitting machine. Also available is the recently introduced seamless underwear knitting machine and tights knitting machine. Premium spare parts can be provided for all catalogued products. Founded in 1996, our company is now located within the picturesque Luojiang District of Quanzhou city. We have imported advanced production equipment from countries such as Japan and Germany. At our disposal is the CNC vertical lathe, high-speed milling machine, CNC machining center, and other types of sophisticated machinery. Supported by sophisticated software such as CAD and CAM, we are now able to assemble up to 2000 machines each year. Through computerized rendering, we can fully analyze each component of our machines. Years of development have allowed us to mature our design and production techniques. HONGSHUO has gradually risen to prominence as a trusted manufacturer in the domestic and abroad industry. Customers can rest assured, our dependable products come attached with excellent after-sales services. In the pursuit of excellence, we have placed a clear emphasis on quality and ingenuity. The rapid growth and expansion of HONGSHUO is assisted by the contributions of a dedicated staff. Especially noteworthy is our technical taskforce, whom have greatly advanced our manufacturing proficiency for circular knitting machines. These professionals accurately analyze global fabric trends in order to provide customers with relevant products.</p>\r\n\r\n<p>Key words:&nbsp;YouFeng(Hong Shuo) Precision Machinery</p>\r\n', 'assets/page_settings/BRdQxIqpShechLnqRLA_20190408142839.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_manage`
--

CREATE TABLE `tbl_contact_manage` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `subject` text COLLATE utf8_bin NOT NULL,
  `message_body` text COLLATE utf8_bin NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_contact_manage`
--

INSERT INTO `tbl_contact_manage` (`id`, `name`, `email`, `subject`, `message_body`, `insert_by`, `insert_time`) VALUES
(4, 'admin', 'anil#gmail.com', 'bangla', 'well done!', 0, '2019-04-09 05:13:20'),
(5, 'admin', 'anil#gmail.com', 'bangla', 'well done!', 0, '2019-04-09 05:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_page`
--

CREATE TABLE `tbl_contact_page` (
  `id` int(11) NOT NULL,
  `barnch_name` text COLLATE utf8_bin NOT NULL COMMENT 'main, agent',
  `phone` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `address` text COLLATE utf8_bin NOT NULL,
  `tel` text COLLATE utf8_bin,
  `contact_person` text COLLATE utf8_bin,
  `company_agent_name` text COLLATE utf8_bin,
  `insert_by` int(11) NOT NULL,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_contact_page`
--

INSERT INTO `tbl_contact_page` (`id`, `barnch_name`, `phone`, `email`, `address`, `tel`, `contact_person`, `company_agent_name`, `insert_by`, `insert_time`) VALUES
(2, 'Main', '01765432156', 'admin@rel-tex.com', 'Xipu Industrial Area, Heshi Town, Luojiang <br>\r\n                                 District, Quanzhou City, Fujian Province, China.', '+86-0595-28022527', '+86-0595-22031516', 'hrsoftbd', 1, '2019-04-07 16:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_divission`
--

CREATE TABLE `tbl_divission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_divission`
--

INSERT INTO `tbl_divission` (`id`, `name`) VALUES
(1, 'ঢাকা'),
(2, 'রাজশাহী'),
(3, 'চট্টগ্রাম'),
(4, 'সিলেট'),
(5, 'খুলনা'),
(6, 'বরিশাল'),
(7, 'রংপুর'),
(8, 'ময়মনসিংহ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_download_page`
--

CREATE TABLE `tbl_download_page` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `classification` varchar(255) COLLATE utf8_bin NOT NULL,
  `attachment` text COLLATE utf8_bin,
  `priority` int(11) NOT NULL DEFAULT '1' COMMENT 'max first',
  `download_times` int(11) NOT NULL DEFAULT '0',
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_download_page`
--

INSERT INTO `tbl_download_page` (`id`, `title`, `classification`, `attachment`, `priority`, `download_times`, `insert_time`, `insert_by`) VALUES
(1, 'SINGLE JERSEY TERRY JACQUARD KNITTING MACHINE', 'Advertising book', 'assets/downloadpagePhoto/chanpinx_20190408171325.png', 100, 0, '2019-04-08 17:13:25', 1),
(3, 'SILK STOCKING KNITTING MACHINE', 'Advertising book', NULL, 1, 0, '0000-00-00 00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general`
--

CREATE TABLE `tbl_general` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_machines`
--

CREATE TABLE `tbl_machines` (
  `id` int(11) NOT NULL,
  `machine_type_id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_machines`
--

INSERT INTO `tbl_machines` (`id`, `machine_type_id`, `name`, `description`, `insert_by`, `insert_time`) VALUES
(3, 1, 'Terry & Polar', '<p>Single jersey、3 thread fleece、Single jersey open width、terry/polar、Pattern wheel</p>\r\n', 1, '2019-04-07 14:28:50'),
(4, 2, 'Double Jersey I...', '<p>Double jersey/ Interlock/Rib、High loop pile、Double terry、Velour shearing、Double jersey pattern wheel、Double jersey/Rib open width.</p>\r\n', 1, '2019-04-07 14:29:36'),
(5, 2, 'Double Jersey P...', '<p>Single/double jersey jacqaurd、Auto stripper & jacquard、Terry jacquard、Seamless underwear、Transfer rib jacquard、Shoe upperl.</p>\r\n', 1, '2019-04-07 14:30:01'),
(6, 1, 'Single Jersey Open Width', '<p>Single/ double jersey jacquard leggings 、Single/ double jersey leggings</p>\r\n', 1, '2019-04-07 14:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_machine_photos`
--

CREATE TABLE `tbl_machine_photos` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL,
  `photo` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_machine_photos`
--

INSERT INTO `tbl_machine_photos` (`id`, `machine_id`, `photo`) VALUES
(8, 3, 'assets/machinePhoto/chanpinx_20190407142850.png'),
(9, 4, 'assets/machinePhoto/chanpinx_20190407142936.png'),
(10, 5, 'assets/machinePhoto/chanpinx_20190407143001.png'),
(11, 6, 'assets/machinePhoto/chanpinx_20190407143026.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_machine_type`
--

CREATE TABLE `tbl_machine_type` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_machine_type`
--

INSERT INTO `tbl_machine_type` (`id`, `name`) VALUES
(1, 'Single jersey series'),
(2, 'Double jersey series'),
(3, 'Electronic knitting machine'),
(4, 'Tights / pantyhose / leggings');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_photos`
--

CREATE TABLE `tbl_photos` (
  `id` int(11) NOT NULL,
  `photo_gallery_id` int(11) NOT NULL,
  `caption` text COLLATE utf8_bin NOT NULL,
  `photo` text COLLATE utf8_bin NOT NULL,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_photos`
--

INSERT INTO `tbl_photos` (`id`, `photo_gallery_id`, `caption`, `photo`, `insert_time`, `insert_by`) VALUES
(1, 1, 'Caption', 'assets/photo/cloudsdaylightforest_20190401183524.jpg', '2019-04-01 18:30:16', 1),
(2, 2, 'Caption', 'assets/photo/KBlackWallpaperUHD_20190402102431.jpg', '2019-04-02 10:24:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_photo_gallery`
--

CREATE TABLE `tbl_photo_gallery` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_photo_gallery`
--

INSERT INTO `tbl_photo_gallery` (`id`, `name`) VALUES
(1, 'Eid Gallery'),
(2, 'Pohela Boishakh'),
(3, '21 March');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `photo` text COLLATE utf8_bin COMMENT 'size 1240x380',
  `priority` int(11) NOT NULL COMMENT 'max upper',
  `insert_by` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `photo`, `priority`, `insert_by`, `insert_time`) VALUES
(1, 'assets/sliderPhoto/slider_20190407114618.jpg', 1, 1, '2019-04-07 05:46:18'),
(2, 'assets/sliderPhoto/slider_20190407114630.jpg', 2, 1, '2019-04-07 05:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_upozilla`
--

CREATE TABLE `tbl_upozilla` (
  `id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `zilla_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_upozilla`
--

INSERT INTO `tbl_upozilla` (`id`, `division_id`, `zilla_id`, `name`) VALUES
(1, 1, 1, 'সাভার'),
(2, 1, 1, 'ধামরাই'),
(3, 1, 1, 'কেরাণীগঞ্জ'),
(4, 1, 1, 'নবাবগঞ্জ'),
(5, 1, 1, 'দোহার'),
(6, 1, 1, 'তেজগাঁও উন্নয়ন সার্কেল'),
(7, 1, 2, 'কালীগঞ্জ'),
(8, 1, 2, 'কালিয়াকৈর'),
(9, 1, 2, 'কাপাসিয়া'),
(10, 1, 2, 'গাজীপুর সদর'),
(11, 1, 2, 'শ্রীপুর'),
(12, 1, 3, 'বাসাইল'),
(13, 1, 3, 'ভুয়াপুর'),
(14, 1, 3, 'ঘাটাইল'),
(15, 1, 3, 'দেলদুয়ার'),
(16, 1, 3, 'গোপালপুর'),
(17, 1, 3, 'মধুপুর'),
(18, 1, 3, 'মির্জাপুর'),
(19, 1, 3, 'নাগরপুর'),
(20, 1, 3, 'সখিপুর'),
(21, 1, 3, 'টাঙ্গাইল সদর'),
(22, 1, 3, 'কালিহাতী'),
(23, 1, 3, 'ধনবাড়ি'),
(24, 1, 4, 'আড়াইহাজার'),
(25, 1, 4, 'বন্দর'),
(26, 1, 4, 'নারায়ণগঞ্জ সদর'),
(27, 1, 4, 'রূপগঞ্জ'),
(28, 1, 4, 'সোনারগাঁ'),
(29, 1, 5, 'ইটনা'),
(30, 1, 5, 'কটিয়াদি'),
(31, 1, 5, 'ভৈরব'),
(32, 1, 5, 'হোসেনপুর'),
(33, 1, 5, 'তাড়াইল'),
(34, 1, 5, 'পাকুন্দিয়া'),
(35, 1, 5, 'কুলিয়ারচর'),
(36, 1, 5, 'কিশোরগঞ্জ সদর'),
(37, 1, 5, 'করিমগঞ্জ'),
(38, 1, 5, 'বাজিতপুর'),
(39, 1, 5, 'অষ্টগ্রাম'),
(40, 1, 5, 'মিঠামইন'),
(41, 1, 5, 'নিকলী'),
(42, 1, 6, 'বেলাবো'),
(43, 1, 6, 'মনোহরদী'),
(44, 1, 6, 'নরসিংদী সদর'),
(45, 1, 6, 'পলাশ'),
(46, 1, 6, 'রায়পুরা'),
(47, 1, 6, 'শিবপুর'),
(48, 1, 7, 'রাজবাড়ী সদর'),
(49, 1, 7, 'গোয়ালন্দ'),
(50, 1, 7, 'পাংশা'),
(51, 1, 7, 'বালিয়াকান্দি'),
(52, 1, 7, 'কালুখালী'),
(53, 1, 8, 'ফরিদপুর সদর'),
(54, 1, 8, 'আলফাডাঙ্গা'),
(55, 1, 8, 'বোয়ালমারী'),
(56, 1, 8, 'সদরপুর'),
(57, 1, 8, 'নগরকান্দা'),
(58, 1, 8, 'ভাঙ্গা'),
(59, 1, 8, 'চরভদ্রাসন'),
(60, 1, 8, 'মধুখালী'),
(61, 1, 8, 'সালথা'),
(62, 1, 9, 'মাদারীপুর সদর'),
(63, 1, 9, 'শিবচর'),
(64, 1, 9, 'কালকিনি'),
(65, 1, 9, 'রাজৈর'),
(66, 1, 10, 'গোপালগঞ্জ সদর'),
(67, 1, 10, 'কাশিয়ানী'),
(68, 1, 10, 'টুংগীপাড়া'),
(69, 1, 10, 'কোটালীপাড়া'),
(70, 1, 10, 'মুকসুদপুর'),
(71, 1, 11, 'মুন্সিগঞ্জ সদর'),
(72, 1, 11, 'শ্রীনগর'),
(73, 1, 11, 'সিরাজদিখান'),
(74, 1, 11, 'লৌহজং '),
(75, 1, 11, 'গজারিয়া'),
(76, 1, 11, 'টংগিবাড়ী'),
(77, 1, 12, 'হরিরামপুর'),
(78, 1, 12, 'সাটুরিয়া'),
(79, 1, 12, 'মানিকগঞ্জ সদর'),
(80, 1, 12, 'ঘিওর'),
(81, 1, 12, 'শিবালয়'),
(82, 1, 12, 'দৌলতপুর'),
(83, 1, 12, 'সিংগাইর'),
(84, 1, 13, 'শরিয়তপুর সদর'),
(85, 1, 13, 'নড়িয়া'),
(86, 1, 13, 'জাজিরা'),
(87, 1, 13, 'গোসাইরহাট'),
(88, 1, 13, 'ভেদরগঞ্জ'),
(89, 1, 13, 'ডামুড্যা'),
(90, 2, 14, 'পবা'),
(91, 2, 14, 'দুর্গাপুর'),
(92, 2, 14, 'মোহনপুর'),
(93, 2, 14, 'চারঘাট'),
(94, 2, 14, 'পুঠিয়া'),
(95, 2, 14, 'বাঘা'),
(96, 2, 14, 'গোদাগাড়ী'),
(97, 2, 14, 'তানোর'),
(98, 2, 14, 'বাঘমারা'),
(99, 2, 15, 'বেলকুচি'),
(100, 2, 15, 'চৌহালি'),
(101, 2, 15, 'কামারখন্দ'),
(102, 2, 15, 'কাজীপুর'),
(103, 2, 15, 'রায়গঞ্জ'),
(104, 2, 15, 'শাহজাদপুর'),
(105, 2, 15, 'সিরাজগঞ্জ সদর'),
(106, 2, 15, 'তাড়াশ'),
(107, 2, 15, 'উল্লাপাড়া'),
(108, 2, 16, 'সুজানগর'),
(109, 2, 16, 'ঈশ্বরদী'),
(110, 2, 16, 'ভাঙ্গুরা'),
(111, 2, 16, 'পাবনা সদর'),
(112, 2, 16, 'বেড়া'),
(113, 2, 16, 'আটঘরিয়া'),
(114, 2, 16, 'চাটমোহর'),
(115, 2, 16, 'সাঁথিয়া'),
(116, 2, 16, 'ফরিদপুর'),
(117, 2, 17, 'কাহালু'),
(118, 2, 17, 'বগুড়া সদর'),
(119, 2, 17, 'সারিয়াকান্দি'),
(120, 2, 17, 'শাজাহানপুর'),
(121, 2, 17, 'দুপচাঁচিয়া'),
(122, 2, 17, 'আদমদিঘি'),
(123, 2, 17, 'নন্দিগ্রাম'),
(124, 2, 17, 'সোনাতলা'),
(125, 2, 17, 'ধুনট'),
(126, 2, 17, 'গাবতলী'),
(127, 2, 17, 'শেরপুর'),
(128, 2, 17, 'শিবগঞ্জ'),
(129, 2, 18, 'চাঁপাইনবাবগঞ্জ সদর'),
(130, 2, 18, 'গোমস্তাপুর'),
(131, 2, 18, 'নাচোল'),
(132, 2, 18, 'ভোলাহাট'),
(133, 2, 18, 'শিবগঞ্জ'),
(134, 2, 19, 'আক্কেলপুর'),
(135, 2, 19, 'কালাই'),
(136, 2, 19, 'ক্ষেতলাল'),
(137, 2, 19, 'পাঁচবিবি'),
(138, 2, 19, 'জয়পুরহাট সদর'),
(139, 2, 20, 'মহাদেবপুর'),
(140, 2, 20, 'বদলগাছী'),
(141, 2, 20, 'পত্নিতলা'),
(142, 2, 20, 'ধামইরহাট'),
(143, 2, 20, 'নিয়ামতপুর'),
(144, 2, 20, 'মান্দা'),
(145, 2, 20, 'আত্রাই'),
(146, 2, 20, 'রাণীনগর'),
(147, 2, 20, 'নওগাঁ সদর'),
(148, 2, 20, 'সাপাহার'),
(149, 2, 20, 'পোরশা'),
(150, 2, 21, 'নাটোর সদর'),
(151, 2, 21, 'সিংড়া'),
(152, 2, 21, 'বড়াইগ্রাম'),
(153, 2, 21, 'বাগাতিপাড়া'),
(154, 2, 21, 'গুরুদাসপুর'),
(155, 2, 21, 'লালপুর'),
(156, 2, 21, 'নলডাঙ্গা'),
(157, 3, 22, 'রাঙ্গুনিয়া'),
(158, 3, 22, 'সীতাকুণ্ড'),
(159, 3, 22, 'মীরসরাই'),
(160, 3, 22, 'পটিয়া'),
(161, 3, 22, 'সন্দ্বীপ'),
(162, 3, 22, 'বাঁশখালী'),
(163, 3, 22, 'বোয়ালখালী'),
(164, 3, 22, 'আনোয়ারা'),
(165, 3, 22, 'সাতকানিয়া'),
(166, 3, 22, 'লোহাগাড়া'),
(167, 3, 22, 'হাটহাজারী'),
(168, 3, 22, 'ফটিকছড়ি'),
(169, 3, 22, 'রাঊজান'),
(170, 3, 22, 'চন্দনাইশ'),
(171, 3, 23, 'দেবিদ্বার'),
(172, 3, 23, 'বরুড়া'),
(173, 3, 23, 'ব্রাহ্মণপাড়া'),
(174, 3, 23, 'চান্দিনা'),
(175, 3, 23, 'চৌদ্দগ্রাম'),
(176, 3, 23, 'দাউদকান্দি'),
(177, 3, 23, 'হোমনা'),
(178, 3, 23, 'লাকসাম'),
(179, 3, 23, 'মুরাদনগর'),
(180, 3, 23, 'নাঙ্গলকোট'),
(181, 3, 23, 'কুমিল্লা সদর'),
(182, 3, 23, 'মেঘনা'),
(183, 3, 23, 'মনোহরগঞ্জ'),
(184, 3, 23, 'সদর দক্ষিণ'),
(185, 3, 23, 'তিতাস'),
(186, 3, 23, 'বুড়িচং'),
(187, 3, 24, 'ছাগলনাইয়া'),
(188, 3, 24, 'ফেনী সদর'),
(189, 3, 24, 'সোনাগাজী'),
(190, 3, 24, 'ফুলগাজী'),
(191, 3, 24, 'পরশুরাম'),
(192, 3, 24, 'দাগনভুঞা'),
(193, 3, 25, 'ব্রাহ্মণবাড়িয়া সদর'),
(194, 3, 25, 'কসবা'),
(195, 3, 25, 'নাসিরনগর'),
(196, 3, 25, 'সরাইল'),
(197, 3, 25, 'আশুগঞ্জ'),
(198, 3, 25, 'আখাউরা'),
(199, 3, 25, 'নবীনগর'),
(200, 3, 25, 'বাঞ্ছারামপুর'),
(201, 3, 25, 'বিজয়নগর'),
(202, 3, 26, 'রাঙ্গামাটি সদর'),
(203, 3, 26, 'কাপ্তাই'),
(204, 3, 26, 'কাউখালী'),
(205, 3, 26, 'বাঘাইছড়ি'),
(206, 3, 26, 'বরকল'),
(207, 3, 26, 'লংগদু'),
(208, 3, 26, 'রাজস্থলী'),
(209, 3, 26, 'বিলাইছড়ি'),
(210, 3, 26, 'জুরাছড়ি'),
(211, 3, 26, 'নানিয়ারচর'),
(212, 3, 27, 'হাইমচর'),
(213, 3, 27, 'কচুয়া'),
(214, 3, 27, 'শহরাস্তি'),
(215, 3, 27, 'চাঁদপুর সদর'),
(216, 3, 27, 'মতলব উত্তর'),
(217, 3, 27, 'ফরিদ্গঞ্জ'),
(218, 3, 27, 'মতলব দক্ষিণ'),
(219, 3, 27, 'হাজীগঞ্জ'),
(220, 3, 28, 'নোয়াখালী সদর'),
(221, 3, 28, 'কোম্পানীগঞ্জ'),
(222, 3, 28, 'বেগমগঞ্জ'),
(223, 3, 28, 'হাতিয়া'),
(224, 3, 28, 'সুবর্ণচর'),
(225, 3, 28, 'কবিরহাট'),
(226, 3, 28, 'সেনবাগ'),
(227, 3, 28, 'চাটখিল'),
(228, 3, 28, 'সোনাইমুড়ী'),
(229, 3, 29, 'লক্ষ্মীপুর সদর'),
(230, 3, 29, 'কমলনগর'),
(231, 3, 29, 'রায়পুর'),
(232, 3, 29, 'রামগতি'),
(233, 3, 29, 'রামগঞ্জ'),
(234, 3, 30, 'কক্সবাজার সদর'),
(235, 3, 30, 'চকরিয়া'),
(236, 3, 30, 'কুতুবদিয়া'),
(237, 3, 30, 'উখিয়া'),
(238, 3, 30, 'মহেশখালী'),
(239, 3, 30, 'পেকুয়া'),
(240, 3, 30, 'রামু'),
(241, 3, 30, 'টেকনাফ'),
(242, 3, 31, 'খাগড়াছড়ি সদর'),
(243, 3, 31, 'দিঘীনালা'),
(244, 3, 31, 'পানছড়ি'),
(245, 3, 31, 'লক্ষীছড়ি'),
(246, 3, 31, 'মহালছড়ি'),
(247, 3, 31, 'মানিকছড়ি'),
(248, 3, 31, 'রামগড়'),
(249, 3, 31, 'মাটিরাঙ্গা'),
(250, 3, 31, 'গুইমারা'),
(251, 3, 32, 'বান্দরবান সদর'),
(252, 3, 32, 'আলীকদম'),
(253, 3, 32, 'নাইক্ষ্যংছড়ি'),
(254, 3, 32, 'রোয়াংছড়ি'),
(255, 3, 32, 'লামা'),
(256, 3, 32, 'রুমা'),
(257, 3, 32, 'থানচি'),
(258, 4, 33, 'বালাগঞ্জ'),
(259, 4, 33, 'বিয়ানীবাজার'),
(260, 4, 33, 'বিশ্বনাথ'),
(261, 4, 33, 'কোম্পানীগঞ্জ'),
(262, 4, 33, 'ফেঞ্চুগঞ্জ'),
(263, 4, 33, 'গোলাপগঞ্জ'),
(264, 4, 33, 'গোয়াইনঘাট'),
(265, 4, 33, 'জৈন্তাপুর'),
(266, 4, 33, 'কানাইঘাট'),
(267, 4, 33, 'সিলেট সদর'),
(268, 4, 33, 'জকিগঞ্জ'),
(269, 4, 33, 'দক্ষিণ সুরমা'),
(270, 4, 33, 'ওসমানী নগর'),
(271, 4, 34, 'বড়লেখা'),
(272, 4, 34, 'কমলগঞ্জ'),
(273, 4, 34, 'কুলাউরা'),
(274, 4, 34, 'মৌলভীবাজার সদর '),
(275, 4, 34, 'রাজনগর'),
(276, 4, 34, 'শ্রীমঙ্গল'),
(277, 4, 34, 'জুড়ী'),
(278, 4, 35, 'নবীগঞ্জ'),
(279, 4, 35, 'বাহুবল'),
(280, 4, 35, 'আজমিরীগঞ্জ'),
(281, 4, 35, 'বানিয়াচং'),
(282, 4, 35, 'লাখাই'),
(283, 4, 35, 'চুনারুঘাট'),
(284, 4, 35, 'হবিগঞ্জ সদর'),
(285, 4, 35, 'মাধবপুর'),
(286, 4, 36, 'সুনামগঞ্জ সদর'),
(287, 4, 36, 'দক্ষিণ সুনামগঞ্জ'),
(288, 4, 36, 'বিশ্বম্ভরপুর'),
(289, 4, 36, 'ছাতক'),
(290, 4, 36, 'জগন্নাথপুর'),
(291, 4, 36, 'তাহিরপুর'),
(292, 4, 36, 'ধর্মপাশা'),
(293, 4, 36, 'জামালগঞ্জ'),
(294, 4, 36, 'শাল্লা'),
(295, 4, 36, 'দিরাই'),
(296, 4, 36, 'দোয়ারাবাজার'),
(297, 5, 37, 'পাইকগাছা'),
(298, 5, 37, 'ফুলতলা'),
(299, 5, 37, 'দিঘলিয়া'),
(300, 5, 37, 'রূপসা'),
(301, 5, 37, 'তেরখাদা'),
(302, 5, 37, 'ডুমুরিয়া'),
(303, 5, 37, 'বটিয়াঘাটা'),
(304, 5, 37, 'দাকোপ'),
(305, 5, 37, 'কয়রা'),
(306, 5, 38, 'মণিরামপুর'),
(307, 5, 38, 'অভয়নগর'),
(308, 5, 38, 'বাঘারপাড়া'),
(309, 5, 38, 'চৌগাছা'),
(310, 5, 38, 'ঝিকরগাছা'),
(311, 5, 38, 'কেশবপুর'),
(312, 5, 38, 'যশোর সদর'),
(313, 5, 38, 'শার্শা'),
(314, 5, 39, 'আশাশুনি'),
(315, 5, 39, 'দেবহাটা'),
(316, 5, 39, 'কলারোয়া'),
(317, 5, 39, 'সাতক্ষীরা সদর'),
(318, 5, 39, 'শ্যামনগর'),
(319, 5, 39, 'তালা'),
(320, 5, 39, 'কালিগঞ্জ'),
(321, 5, 40, 'মুজিবনগর'),
(322, 5, 40, 'মেহেরপুর সদর'),
(323, 5, 40, 'গাংনী'),
(324, 5, 41, 'নড়াইল সদর'),
(325, 5, 41, 'লোহাগড়া'),
(326, 5, 41, 'কালিয়া'),
(327, 5, 42, 'চুয়াডাঙ্গা সদর'),
(328, 5, 42, 'আলমডাঙ্গা'),
(329, 5, 42, 'দামুড়হুদা'),
(330, 5, 42, 'জীবননগর'),
(331, 5, 43, 'শালিখা'),
(332, 5, 43, 'শ্রীপুর'),
(333, 5, 43, 'মাগুরা সদর'),
(334, 5, 43, 'মহম্মদপুর'),
(335, 5, 44, 'ফকিরহাট'),
(336, 5, 44, 'বাগেরহাট সদর'),
(337, 5, 44, 'মোল্লাহাট'),
(338, 5, 44, 'শরণখোলা'),
(339, 5, 44, 'রামপাল'),
(340, 5, 44, 'মোড়েলগঞ্জ'),
(341, 5, 44, 'কচুয়া'),
(342, 5, 44, 'মোংলা'),
(343, 5, 44, 'চিতলমারী'),
(344, 5, 45, 'ঝিনাইদহ সদর'),
(345, 5, 45, 'শৈলকুপা'),
(346, 5, 45, 'হরিণাকুণ্ডু '),
(347, 5, 45, 'কালীগঞ্জ'),
(348, 5, 45, 'কোটচাঁদপুর'),
(349, 5, 45, 'মহেশপুর'),
(350, 5, 46, 'কুষ্টিয়া সদর'),
(351, 5, 46, 'কুমারখালী'),
(352, 5, 46, 'খোকসা'),
(353, 5, 46, 'মিরপুর'),
(354, 5, 46, 'দৌলতপুর'),
(355, 5, 46, 'ভেড়ামারা'),
(356, 6, 47, 'বরিশাল সদর'),
(357, 6, 47, 'বাকেরগঞ্জ'),
(358, 6, 47, 'বাবুগঞ্জ'),
(359, 6, 47, 'উজিরপুর'),
(360, 6, 47, 'বানারীপাড়া'),
(361, 6, 47, 'গৌরনদী'),
(362, 6, 47, 'আগৈলঝাড়া'),
(363, 6, 47, 'মেহেন্দিগঞ্জ'),
(364, 6, 47, 'মুলাদী'),
(365, 6, 47, 'হিজলা'),
(366, 6, 48, 'ঝালকাঠি সদর'),
(367, 6, 48, 'কাঠালিয়া'),
(368, 6, 48, 'নলছিটি'),
(369, 6, 48, 'রাজাপুর'),
(370, 6, 49, 'বাউফল'),
(371, 6, 49, 'পটুয়াখালী সদর'),
(372, 6, 49, 'দুমকি'),
(373, 6, 49, 'দশমিনা'),
(374, 6, 49, 'কলাপাড়া'),
(375, 6, 49, 'মির্জাগঞ্জ'),
(376, 6, 49, 'গলাচিপা'),
(377, 6, 49, 'রাঙ্গাবালী'),
(378, 6, 50, 'পিরোজপুর সদর'),
(379, 6, 50, 'নাজিরপুর'),
(380, 6, 50, 'কাউখালী'),
(381, 6, 50, 'জিয়ানগর'),
(382, 6, 50, 'ভান্ডারিয়া'),
(383, 6, 50, 'মঠবাড়ীয়া'),
(384, 6, 50, 'নেছারাবাদ'),
(385, 6, 51, 'ভোলা সদর'),
(386, 6, 51, 'বোরহানউদ্দিন'),
(387, 6, 51, 'চরফ্যাশন'),
(388, 6, 51, 'দৌলতখান'),
(389, 6, 51, 'মনপুরা'),
(390, 6, 51, 'তজুমদ্দিন'),
(391, 6, 51, 'লালমোহন'),
(392, 6, 52, 'আমতলী'),
(393, 6, 52, 'বরগুনা সদর'),
(394, 6, 52, 'বেতাগী'),
(395, 6, 52, 'বামনা'),
(396, 6, 52, 'পাথরঘাটা'),
(397, 6, 52, 'তালতলি'),
(398, 7, 53, 'রংপুর সদর'),
(399, 7, 53, 'গঙ্গাচড়া'),
(400, 7, 53, 'তারাগঞ্জ'),
(401, 7, 53, 'বদরগঞ্জ'),
(402, 7, 53, 'মিঠাপুকুর'),
(403, 7, 53, 'কাউনিয়া'),
(404, 7, 53, 'পীরগঞ্জ'),
(405, 7, 53, 'পীরগাছা'),
(406, 7, 54, 'লালমনিরহাট সদর'),
(407, 7, 54, 'আদিতমারী'),
(408, 7, 54, 'কালীগঞ্জ'),
(409, 7, 54, 'হাতীবান্ধা'),
(410, 7, 54, 'পাটগ্রাম'),
(411, 7, 55, 'পঞ্চগড় সদর'),
(412, 7, 55, 'দেবীগঞ্জ'),
(413, 7, 55, 'বোদা'),
(414, 7, 55, 'আটোয়ারী'),
(415, 7, 55, 'তেতুলিয়া'),
(416, 7, 56, 'কুড়িগ্রাম সদর'),
(417, 7, 56, 'নাগেশ্বরী'),
(418, 7, 56, 'ভুরুঙ্গামারী'),
(419, 7, 56, 'ফুলবাড়ী'),
(420, 7, 56, 'রাজারহাট'),
(421, 7, 56, 'উলিপুর'),
(422, 7, 56, 'চিলমারী'),
(423, 7, 56, 'রৌমারী'),
(424, 7, 56, 'চর রাজিবপুর'),
(425, 7, 57, 'নবাবগঞ্জ'),
(426, 7, 57, 'বীরগঞ্জ'),
(427, 7, 57, 'ঘোড়াঘাট'),
(428, 7, 57, 'বিরামপুর'),
(429, 7, 57, 'পার্বতীপুর'),
(430, 7, 57, 'বোচাগঞ্জ'),
(431, 7, 57, 'কাহারোল'),
(432, 7, 57, 'ফুলবাড়ী'),
(433, 7, 57, 'দিনাজপুর সদর'),
(434, 7, 57, 'হাকিমপুর'),
(435, 7, 57, 'খানসামা'),
(436, 7, 57, 'বিরল'),
(437, 7, 57, 'চিরিরবন্দর'),
(438, 7, 58, 'ঠাকুরগাঁও সদর'),
(439, 7, 58, 'পীরগঞ্জ'),
(440, 7, 58, 'রাণীশংকৈল'),
(441, 7, 58, 'হরিপুর'),
(442, 7, 58, 'বালিয়াডাঙ্গী'),
(443, 7, 59, 'সাদুল্লাপুর'),
(444, 7, 59, 'গাইবান্ধা সদর'),
(445, 7, 59, 'পলাশবাড়ী'),
(446, 7, 59, 'সাঘাটা'),
(447, 7, 59, 'গোবিন্দগঞ্জ'),
(448, 7, 59, 'সুন্দরগঞ্জ'),
(449, 7, 59, 'ফুলছড়ি'),
(450, 7, 60, 'সৈয়দপুর'),
(451, 7, 60, 'ডোমার'),
(452, 7, 60, 'ডিমলা'),
(453, 7, 60, 'জলঢাকা'),
(454, 7, 60, 'কিশোরগঞ্জ'),
(455, 7, 60, 'নীলফামারী সদর'),
(456, 8, 61, 'ফুলবাড়ীয়া '),
(457, 8, 61, 'ত্রিশাল'),
(458, 8, 61, 'ভালুকা'),
(459, 8, 61, 'মুক্তাগাছা'),
(460, 8, 61, 'ময়মনসিংহ সদর'),
(461, 8, 61, 'ধোবাউরা'),
(462, 8, 61, 'ফুলপুর'),
(463, 8, 61, 'হালুয়াঘাট'),
(464, 8, 61, 'গৌরীপুর'),
(465, 8, 61, 'গফরগাঁও'),
(466, 8, 61, 'ঈশ্বরগঞ্জ'),
(467, 8, 61, 'নান্দাইল'),
(468, 8, 61, 'তারাকান্দা'),
(469, 8, 62, 'জামালপুর সদর'),
(470, 8, 62, 'মেলান্দহ'),
(471, 8, 62, 'ইসলামপুর'),
(472, 8, 62, 'দেওয়ানগঞ্জ'),
(473, 8, 62, 'সরিষাবাড়ী'),
(474, 8, 62, 'মাদারগঞ্জ'),
(475, 8, 62, 'বকশীগঞ্জ'),
(476, 8, 63, 'বারহাট্টা'),
(477, 8, 63, 'দুর্গাপুর'),
(478, 8, 63, 'কেন্দুয়া'),
(479, 8, 63, 'আটপাড়া'),
(480, 8, 63, 'মদন'),
(481, 8, 63, 'খালিয়াজুরী'),
(482, 8, 63, 'কলমাকান্দা'),
(483, 8, 63, 'মোহনগঞ্জ'),
(484, 8, 63, 'পূর্বধলা'),
(485, 8, 63, 'নেত্রকোনা সদর'),
(486, 8, 64, 'শেরপুর সদর'),
(487, 8, 64, 'নালিতাবাড়ী'),
(488, 8, 64, 'শ্রীবরদী'),
(489, 8, 64, 'নকলা'),
(490, 8, 64, 'ঝিনাইগাতী'),
(491, 1, 1, 'ঢাকা মহানগর');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_videos`
--

CREATE TABLE `tbl_videos` (
  `id` int(11) NOT NULL,
  `video_link` text COLLATE utf8_bin NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `insert_time` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '0000-00-00 00:00',
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_videos`
--

INSERT INTO `tbl_videos` (`id`, `video_link`, `title`, `insert_time`, `insert_by`) VALUES
(1, 'https://www.youtube.com/embed/nE_vVkiYltc', 'PANDA BABIES', '2019-04-01 16:20:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_zilla`
--

CREATE TABLE `tbl_zilla` (
  `id` int(11) NOT NULL,
  `divission_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_zilla`
--

INSERT INTO `tbl_zilla` (`id`, `divission_id`, `name`) VALUES
(1, 1, 'ঢাকা'),
(2, 1, 'গাজীপুর'),
(3, 1, 'টাঙ্গাইল'),
(4, 1, 'নারায়ণগঞ্জ'),
(5, 1, 'কিশোরগঞ্জ'),
(6, 1, 'নরসিংদী'),
(7, 1, 'রাজবাড়ী'),
(8, 1, 'ফরিদপুর'),
(9, 1, 'মাদারীপুর'),
(10, 1, 'গোপালগঞ্জ'),
(11, 1, 'মুন্সিগঞ্জ'),
(12, 1, 'মানিকগঞ্জ'),
(13, 1, 'শরীয়তপুর'),
(14, 2, 'রাজশাহী'),
(15, 2, 'সিরাজগঞ্জ'),
(16, 2, 'পাবনা'),
(17, 2, 'বগুড়া'),
(18, 2, 'চাঁপাইনবাবগঞ্জ'),
(19, 2, 'জয়পুরহাট'),
(20, 2, 'নওগাঁ'),
(21, 2, 'নাটোর'),
(22, 3, 'চট্টগ্রাম'),
(23, 3, 'কুমিল্লা'),
(24, 3, 'ফেনী'),
(25, 3, 'ব্রাহ্মণবাড়িয়া'),
(26, 3, 'রাঙ্গামাটি'),
(27, 3, 'চাঁদপুর'),
(28, 3, 'নোয়াখালী'),
(29, 3, 'লক্ষ্মীপুর'),
(30, 3, 'কক্সবাজার'),
(31, 3, 'খাগড়াছড়ি'),
(32, 3, 'বান্দরবান'),
(33, 4, 'সিলেট'),
(34, 4, 'মৌলভীবাজার'),
(35, 4, 'হবিগঞ্জ'),
(36, 4, 'সুনামগঞ্জ'),
(37, 5, 'খুলনা'),
(38, 5, 'যশোর'),
(39, 5, 'সাতক্ষীরা'),
(40, 5, 'মেহেরপুর'),
(41, 5, 'নড়াইল'),
(42, 5, 'চুয়াডাঙ্গা'),
(43, 5, 'মাগুড়া'),
(44, 5, 'বাগেরহাট'),
(45, 5, 'ঝিনাইদহ'),
(46, 5, 'কুষ্টিয়া'),
(47, 6, 'বরিশাল'),
(48, 6, 'ঝালকাঠি'),
(49, 6, 'পটুয়াখালী'),
(50, 6, 'পিরোজপুর'),
(51, 6, 'ভোলা'),
(52, 6, 'বরগুনা'),
(53, 7, 'রংপুর'),
(54, 7, 'লালমনিরহাট'),
(55, 7, 'পঞ্চগড়'),
(56, 7, 'কুড়িগ্রাম'),
(57, 7, 'দিনাজপুর'),
(58, 7, 'ঠাকুরগাঁও'),
(59, 7, 'গাইবান্ধা'),
(60, 7, 'নীলফামারী'),
(61, 8, 'ময়মনসিংহ'),
(62, 8, 'জামালপুর'),
(63, 8, 'নেত্রকোনা'),
(64, 8, 'শেরপুর');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `username` varchar(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `address` int(11) NOT NULL COMMENT 'thana id',
  `roadHouse` text NOT NULL,
  `phone` text NOT NULL,
  `userType` text NOT NULL,
  `photo` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active user, 0 for not active user',
  `emailVerified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for verify, 0 for not verify',
  `mobileVerified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for verify, 0 for not verify'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `email`, `password`, `address`, `roadHouse`, `phone`, `userType`, `photo`, `status`, `emailVerified`, `mobileVerified`) VALUES
(1, 'rayhan', 'roky', 'roky', 'roky@tm.hrsoftbd.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, '12/6 solimullah road', '01709372481', 'admin', 'assets/userPhoto/IMG_20171229_212919_561.jpg', 1, 0, 0),
(2, 'mamun', 'khan', 'mamun214', 'mamun214@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 16, '23/67, black road.', '01716545454', 'user', 'assets/userPhoto/al_mamun.jpg', 1, 0, 0),
(3, 'Foishaal', 'Ahmed', 'Foishaal', 'Foishaal@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 97, 'Madaripur', '01738244627', 'user', 'assets/userPhoto/defaultUser.jpg', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='types of user, each type has single controller';

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `value`, `name`) VALUES
(2, 'user', 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_status`
--
ALTER TABLE `log_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_recovery`
--
ALTER TABLE `password_recovery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_applications`
--
ALTER TABLE `tbl_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_application_photos`
--
ALTER TABLE `tbl_application_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_application_type`
--
ALTER TABLE `tbl_application_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_common_pages`
--
ALTER TABLE `tbl_common_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_manage`
--
ALTER TABLE `tbl_contact_manage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_page`
--
ALTER TABLE `tbl_contact_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_divission`
--
ALTER TABLE `tbl_divission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_download_page`
--
ALTER TABLE `tbl_download_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_general`
--
ALTER TABLE `tbl_general`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tbl_machines`
--
ALTER TABLE `tbl_machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_machine_photos`
--
ALTER TABLE `tbl_machine_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_machine_type`
--
ALTER TABLE `tbl_machine_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_photo_gallery`
--
ALTER TABLE `tbl_photo_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_upozilla`
--
ALTER TABLE `tbl_upozilla`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_zilla`
--
ALTER TABLE `tbl_zilla`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`,`email`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_status`
--
ALTER TABLE `log_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_recovery`
--
ALTER TABLE `password_recovery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_applications`
--
ALTER TABLE `tbl_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_application_photos`
--
ALTER TABLE `tbl_application_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_application_type`
--
ALTER TABLE `tbl_application_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_common_pages`
--
ALTER TABLE `tbl_common_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `tbl_contact_manage`
--
ALTER TABLE `tbl_contact_manage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_contact_page`
--
ALTER TABLE `tbl_contact_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_divission`
--
ALTER TABLE `tbl_divission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_download_page`
--
ALTER TABLE `tbl_download_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_general`
--
ALTER TABLE `tbl_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_machines`
--
ALTER TABLE `tbl_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_machine_photos`
--
ALTER TABLE `tbl_machine_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_machine_type`
--
ALTER TABLE `tbl_machine_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_photo_gallery`
--
ALTER TABLE `tbl_photo_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_upozilla`
--
ALTER TABLE `tbl_upozilla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=492;

--
-- AUTO_INCREMENT for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_zilla`
--
ALTER TABLE `tbl_zilla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
